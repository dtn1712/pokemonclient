﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class MessageNetwork
{
    public byte Key;
    public Dictionary<byte, object> Parameters;

    public MessageNetwork()
    {
        Parameters = new Dictionary<byte, object>();
    }

    public MessageNetwork(byte key)
    {
        Key = key;
        Parameters = new Dictionary<byte, object>();
    }
}
