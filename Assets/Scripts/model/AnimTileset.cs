﻿using UnityEngine;
using System.Collections;

public class AnimTileset : UnitMap {

    int IndexAnim = 0;
    public Texture2D Texture;
    public byte Type_Index;
	void Update()
    {
        if(transform.childCount > 0 && Time.frameCount % 15 == 0)
        {
            transform.GetChild(IndexAnim).gameObject.SetActive(false);
            IndexAnim = (IndexAnim + 1) % transform.childCount;
            transform.GetChild(IndexAnim).gameObject.SetActive(true);
        }
    }
    protected override void OnDrawGizmosSelected()
    {
        base.OnDrawGizmosSelected();
        if(Texture != null)
        {
            if (Type_Index > 32) Type_Index = 32;
            for (int x = 0; x < 3; x++)
            {
                Transform Anim = transform.GetChild(x);
                Vector[] V = ToRect(Type_Index);
                for (int i = 0; i < 4; i++)
                {
                    SpriteRenderer RS = Anim.GetChild(i).GetComponent<SpriteRenderer>();
                    RS.sprite = UnityEngine.Sprite.Create(Texture, new Rect((V[i].X + (x * 4)) * 24, V[i].Y * 24, 24, 24), new Vector2(0, 1), 48);
                }
            }
        }
    }
    
}
