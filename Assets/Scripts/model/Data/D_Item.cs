﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[System.Serializable]
public class D_Item
{
    public string Icon;
    public string Name;
    public enumCategoryItem Category;
    public string Effect;
    public Rect Rect
    {
        get { return rect.Rect; }
        set { rect.Rect = value; }
    }

    private Rectangle rect;

    public Sprite Sprite
    {
        get
        {
            Texture2D t2D = Resources.Load<Texture2D>("2D/icon/" + Icon);
            Sprite sprite;
            if (t2D != null)
            {
                sprite = Sprite.Create(t2D, Rect, new Vector2(0.5f, 0.5f));
                sprite.name = Icon;
                return sprite;
            }
            else
                return null;
        }
    }

    public Texture2D TextureIcon
    {
        get
        {
            Sprite sprite = Sprite;
            if (sprite != null)
            {
                Rect r = sprite.rect;
                Texture2D icon = new Texture2D((int)r.width, (int)r.height);
                Debug.Log("Rect:" + r);
                icon.SetPixels(sprite.texture.GetPixels((int)r.x, (int)r.y, (int)r.width, (int)r.height));
                icon.Apply();
                return icon;
            }
            return null;
        }
    }

    public D_Item()
    {
        Icon = "Null";
        Name = "No name";
        Effect = "";
        rect = new Rectangle();
    }
}

[System.Serializable]
public class Rectangle
{
    public float X, Y, Width, Height;
    public Rectangle()
    {
        X = Y = Width = Height = 0;
    }

    public Rect Rect
    {
        get { return new Rect(X, Y, Width, Height); }
        set
        {
            X = value.x;
            Y = value.y;
            Width = value.width;
            Height = value.height;
        }
    }
}