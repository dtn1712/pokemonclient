﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class D_Pokemon {

    public string Id;
    public string Name;
    public string Avatar;
    public D_PokemonDefault Default;
    public D_PokemonProfile Profile;
    public List<D_DamageWhenAttacked> Damages;
    public List<D_PokemonEvolutions> Evolutions;
    public List<D_PokemonNaturalMove> NaturalMoves;

    public Sprite SpriteAvatar
    {
        get
        {
            Texture2D t2D = Resources.Load<Texture2D>("2D/pokemon/" + Avatar);
            if (t2D == null) return null;
            else
            return Sprite.Create(t2D, new Rect(0, 0, t2D.width, t2D.height), new Vector2(0.5f,0.5f));
        }
    }

    public void Awake()
    {
        //transform.GetChild(0).GetComponent<Text>().text = Id + " - " + Name;
        //gameObject.name = Id + " - " + Name;
    }

    public void Selected()
    {
        //if (V_PokedexDialog.eSelectPokedex != null)
        //    V_PokedexDialog.eSelectPokedex(gameObject);
    }

    public void Init()
    {
        Id = "N/A";
        Name = "None";
        Default = new D_PokemonDefault();
        Default.Type = new List<enumType>();
        Default.Type.Add(enumType.Bug);
        Profile = new D_PokemonProfile();
        Damages = new List<D_DamageWhenAttacked>();
        Evolutions = new List<D_PokemonEvolutions>();
        NaturalMoves = new List<D_PokemonNaturalMove>();
    }
}

[System.Serializable]
public class D_PokemonDefault : System.Object
{
    public int HP, Attack, Defense, Speed, SpATK, SpDef;
    public List<enumType> Type;
    public string SeedPokemon;
}

[System.Serializable]
public class D_PokemonEvolutions : System.Object
{
    public string IdFrom, IdTo, Details; 
}

[System.Serializable]
public class D_PokemonNaturalMove : System.Object
{
    public int Lv;
    public string SkillName;
}