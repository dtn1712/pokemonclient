﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class Database {

    public static SkillDatabase MovesData;
    public static Dictionary<string, int> Moves = new Dictionary<string, int>();

    public static Pokedex PokedexData;
    public static Dictionary<string, int> Pokedex = new Dictionary<string, int>();

    public static ItemData ItemData;
    public static Dictionary<string, int> Items = new Dictionary<string, int>();

    public static void LoadMovesData()
    {
        if (Database.MovesData == null)
        {
            Database.MovesData = (SkillDatabase)FileBinary.ReadTextAssets("Database/Skills");
            if (Database.MovesData == null)
            {
                Database.MovesData = new SkillDatabase();
            }
        }
        Database.Moves = new Dictionary<string, int>();
        for (int i = 0; i < Database.MovesData.Count; i++)
            Database.Moves[Database.MovesData[i].Name.ToLower()] = i;
        Debug.Log("<color=yellow>Loading Moves Data is complete.</color>");
    }

    public static void LoadPokedexData()
    {
        if (Database.PokedexData == null)
        {
            Database.PokedexData = (Pokedex)FileBinary.ReadTextAssets("Database/Pokedex");
            if (Database.PokedexData == null)
            {
                Database.PokedexData = new global::Pokedex();
            }
        }
        Database.Pokedex = new Dictionary<string, int>();
        for (int i = 0; i < Database.PokedexData.Count; i++)
            Database.Pokedex[Database.PokedexData[i].Id.ToLower()] = i;
        Debug.Log("<color=yellow>Loading Pokedex Data is complete.</color>");
    }

    public static void LoadItemData()
    {
        if (Database.ItemData == null)
        {
            Database.ItemData = (ItemData)FileBinary.ReadTextAssets("Database/Items");
            if (Database.ItemData == null)
            {
                Database.ItemData = new global::ItemData();
            }
        }
        Database.Items = new Dictionary<string, int>();
        for (int i = 0; i < Database.ItemData.Count; i++)
            Database.Items[Database.ItemData[i].Name.ToLower()] = i;
        Debug.Log("<color=yellow>Loading Items Data is complete.</color>");
    }

    public static void LoadDatabase()
    {
        LoadMovesData();
        LoadPokedexData();
        LoadItemData();
    }
}

//Skilldatase
[System.Serializable]
public class SkillDatabase : List<SkillData>
{
    public void AddItem(SkillData skill)
    {
        this.Add(skill);
    }
}

[System.Serializable]
public class Pokedex : List<D_Pokemon>
{
    public void AddItem(D_Pokemon pokemon)
    {
        this.Add(pokemon);
    }
}

[System.Serializable]
public class ItemData: List<D_Item>
{

}