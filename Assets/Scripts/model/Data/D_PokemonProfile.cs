﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class D_PokemonProfile : System.Object {
    public string Height, Weight, CatchRate, GenderRatio, 
        EggGroups, HatchSteps, Abilities, Evs;
}
