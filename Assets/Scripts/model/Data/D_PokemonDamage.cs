﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class D_DamageWhenAttacked : System.Object
{
    public enumType Class;
    public float Value;
}