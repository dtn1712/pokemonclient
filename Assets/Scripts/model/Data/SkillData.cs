﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SkillData {

    public string Name;
    public enumType Type;
    public enumCategorySkill Category;
    public int Power, Accuracy, PP;
    public string Effect;
	public SkillData(string name, enumType type, enumCategorySkill category, int power, int accuracy, int pp, string effect)
    {
        Name = name;
        Type = type;
        Category = category;
        Power = power;
        Accuracy = accuracy;
        PP = pp;
        Effect = effect;
    }
}
