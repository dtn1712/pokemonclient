﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[System.Serializable]
public enum enumType
{
    Bug,
    Dark,
    Dragon,
    Electric,
    Fairy,
    Fighting,
    Fire,
    Flying,
    Ghost,
    Ground,
    Grass,
    Ice,
    Normal,
    Poison,
    Psychic,
    Rock,
    Steel,
    Water,
}

[System.Serializable]
public enum enumCategorySkill
{
    None,
    Physical,
    Status,
    Special,
}

public enum enumCategoryItem
{
    Hold, Battle, General, Berries, Medicine, Pokeballs, Machines
}
