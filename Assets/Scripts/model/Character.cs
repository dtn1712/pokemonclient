﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using Game60;

public class Character : UnitMap {

    public static Character Player;

    //**** public properties
    public Texture2D Texture;
    public bool IsCharacter;
    public bool IsMainPlayer;
    public Sprite[] arrMoveDown, arrMoveLeft, arrMoveRight, arrMoveUp;

    public static bool IsChatMessage;

    //**** protected properties
    
    bool isMoving = false, IsLeftMouseDown = false;
    enumDirection direction;

    #region Getter, Setter

    public bool IsMoving
    {
        get { return isMoving; }
    }

    public enumDirection Direction
    {
        get { return direction; }
        set
        {
            switch (value)
            {
                case enumDirection.Down:
                    ChangeSprite(arrMoveDown, 1);
                    break;
                case enumDirection.Up:
                    ChangeSprite(arrMoveUp, 1);
                    break;
                case enumDirection.Left:
                    ChangeSprite(arrMoveLeft, 1);
                    break;
                case enumDirection.Right:
                    ChangeSprite(arrMoveRight, 1);
                    break;
            }
            direction = value;
        }
    }
    #endregion

    #region SpriteRender Child
    SpriteRenderer A
    {
        get { return transform.GetChild(0).GetComponent<SpriteRenderer>(); }
    }
    SpriteRenderer B
    {
        get { return transform.GetChild(1).GetComponent<SpriteRenderer>(); }
    }
    SpriteRenderer C
    {
        get { return transform.GetChild(2).GetComponent<SpriteRenderer>(); }
    }
    SpriteRenderer D
    {
        get { return transform.GetChild(3).GetComponent<SpriteRenderer>(); }
    }
    #endregion

    #region pivot
    Vector2[] pivots = new Vector2[4];
    #endregion
    
    public float speed = 1;

    int[] temp;
    void OnGUI()
    {
        
    }

    protected override void OnDrawGizmosSelected()
    {
        base.OnDrawGizmosSelected();
        CreateTexture();
    }

    #region Getter, Setter Properties

    public Vector Location
    {
        get { return new Vector(transform.position); }
        set
        {
            transform.position = new Vector3(value.X, value.Y);
        }
    }

    public float Speed
    {
        get { return speed; }
        set { speed = value; }
    }

    #endregion

    #region System

    private void CreateTexture()
    {
        if (Texture != null)
        {
            int w = Texture.width / 3;
            int h = Texture.height / 4;
            Vector2 pivot;
            if (IsCharacter)
                pivot = new Vector2(0, 1.75f);
            else
                pivot = new Vector2(0, 2f);

            int pixel_format = 48;

            pivots[0] = new Vector2(1, 1) + pivot;
            pivots[1] = new Vector2(0, 1) + pivot;
            pivots[2] = new Vector2(1, 0) + pivot;
            pivots[3] = new Vector2(0, 0) + pivot;

            CreateSpriteForArray(ref arrMoveLeft, w, h, pivot, pixel_format, 0, "Left");

            CreateSpriteForArray(ref arrMoveDown, w, h, pivot, pixel_format, 1, "Down");

            CreateSpriteForArray(ref arrMoveRight, w, h, pivot, pixel_format, 2, "Right");

            CreateSpriteForArray(ref arrMoveUp, w, h, pivot, pixel_format, 3, "Up");

            Direction = cMap.DirectionPlayer;
        }
    }

    void CreateSpriteForArray(ref Sprite[] array, int width, int height, Vector2 pivot, int pixel_unit, int row, string nameArray)
    {
        array = new UnityEngine.Sprite[12];
        int w = width / 2;
        int h = height / 2;

        for (int i = 0; i < 3; i++)
        {
            for(int j = 0; j < 4; j++)
            {
                int x = width * i + (j % 2) * w;
                int y = height * row + (j / 2) * h;
                Rect rect = new Rect(x,y,w,h);
                array[i * 4 + j] = UnityEngine.Sprite.Create(Texture, rect, pivots[j], pixel_unit);
                array[i * 4 + j].name = string.Format("{0} {1}-{2}", nameArray, i, j);
            }
        }
    }
    
    void ChangeSprite(Sprite[] array ,int sprite_id)
    {
        SpriteRenderer A = transform.GetChild(0).GetComponent<SpriteRenderer>();
        SpriteRenderer B = transform.GetChild(1).GetComponent<SpriteRenderer>();
        SpriteRenderer C = transform.GetChild(2).GetComponent<SpriteRenderer>();
        SpriteRenderer D = transform.GetChild(3).GetComponent<SpriteRenderer>();
        A.sprite = array[sprite_id * 4 + 0];
        B.sprite = array[sprite_id * 4 + 1];
        C.sprite = array[sprite_id * 4 + 2];
        D.sprite = array[sprite_id * 4 + 3];
    }

    protected override void Awake()
    {
        base.Awake();
        Direction = enumDirection.Down;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        if (IsMainPlayer)
        {
            cKeyPad.Key_MoveDown -= CKeyPad_Key_MoveDown;
            cKeyPad.Key_MoveLeft -= CKeyPad_Key_MoveLeft;
            cKeyPad.Key_MoveRight -= CKeyPad_Key_MoveRight;
            cKeyPad.Key_MoveUp -= CKeyPad_Key_MoveUp;
            cKeyPad.Key_RunAction -= CKeyPad_Key_RunAction;
        }
    }

    void Start()
    {
        CreateTexture();
        if (IsMainPlayer)
        {
            cKeyPad.Key_MoveDown += CKeyPad_Key_MoveDown;
            cKeyPad.Key_MoveLeft += CKeyPad_Key_MoveLeft;
            cKeyPad.Key_MoveRight += CKeyPad_Key_MoveRight;
            cKeyPad.Key_MoveUp += CKeyPad_Key_MoveUp;
            cKeyPad.Key_RunAction += CKeyPad_Key_RunAction;
        }
    }
    
    void Update()
    {
        #region Move Character
        if (IsMainPlayer)
        {
            #region Move by Keyboard
            if (Input.GetKey(KeyCode.S))
            {
                Request_MoveDown();
            }
            else if (Input.GetKey(KeyCode.W))
            {
                Request_MoveUp();
            }
            else if (Input.GetKey(KeyCode.A))
            {
                Request_MoveLeft();
            }
            else if (Input.GetKey(KeyCode.D))
            {
                Request_MoveRight();
            }
            else if(Input.GetKeyDown(KeyCode.Space))
            {
                Action_Event();
            }
            #endregion
            //-----------
        }

        #endregion

        #region Update Mouse hoặc Touch
        if (!IsChatMessage)
        {
            if (Input.GetMouseButtonDown(0))
                IsLeftMouseDown = true;
            if (Input.GetMouseButtonUp(0))
                IsLeftMouseDown = false;
        }
        #endregion
    }

    #endregion

    #region Event KeyPad
    private void CKeyPad_Key_MoveUp()
    {
        Request_MoveUp();
    }

    private void CKeyPad_Key_MoveRight()
    {
        Request_MoveRight();
    }

    private void CKeyPad_Key_MoveLeft()
    {
        Request_MoveLeft();
    }

    private void CKeyPad_Key_MoveDown()
    {
        Request_MoveDown();
    }

    private void CKeyPad_Key_RunAction()
    {
        Action_Event();
    }
    #endregion

    #region Kiểm tra xem có thể di chuyển hay không

    bool IsCollision(Vector vec)
    {
        
        if (Sprite.sortingLayerName.IndexOf("Sky") >= 0) return true;

        enumDirection direction;
        Vector v_direction = vec - Location;
        if(v_direction.X != 0)
        {
            if (v_direction.X > 0)
                direction = enumDirection.Right;
            else direction = enumDirection.Left;
        }
        else
        {
            if (v_direction.Y > 0)
                direction = enumDirection.Up;
            else direction = enumDirection.Down;
        }

        DirectionAble target_tileset = new DirectionAble();
        DirectionAble next_tileset = new DirectionAble();

        List<UnitMap> Target = Map.Collision.Get(new Vector(Location));
        List<UnitMap> list = Map.Collision.Get(vec);

        //Get Check collision of target tileset
        if (Target.Count == 0)
        {
            Vector loc = new Vector(Location);
            target_tileset.Check(Map.Collision.GetStaticCollision(loc));
        }
        else
        {
            target_tileset.Check(Target[0].Type_Collision);
        }

        //Get Check collision of next tileset
        if (list.Count == 0)
        {
            next_tileset.Check(Map.Collision.GetStaticCollision(vec));
        }
        else
        {
            UnitMap unit = list[list.Count - 1];
            if (unit.LayerID <= LayerID)
            {
                next_tileset.Check(unit.Type_Collision);
            }
            else
            {
                next_tileset.CheckAble();
            }
        }

        return target_tileset.Linked(direction, next_tileset);
    }

    bool CanMove(enumDirection dir)
    {
        if (IsChatMessage) return false;
        switch(dir)
        {
            case enumDirection.Down:
                return IsCollision(Location + new Vector(0, -1));
                break;
            case enumDirection.Up:
                return IsCollision(Location + new Vector(0, 1));
                break;
            case enumDirection.Left:
                return IsCollision(Location + new Vector(-1, 0));
                break;
            case enumDirection.Right:
                return IsCollision(Location + new Vector(1, 0));
                break;
            default: return false;
        }
    }

    bool CanMove()
    {
        return CanMove(direction);
    }
    #endregion

    #region Check và hiệu ứng bụi cỏ
    void Check_On_Grass()
    {
        Vector2 vec = transform.position;
        if(Map.GrassTileset.Get(Map.Key(vec)) != null)
        {
            OnGrass();
        }
        else
        {
            OutGrass();
        }
    }

    void OnGrass()
    {
        if (A.color.a != 0.25f)
        {
            A.color = new Color(1, 1, 1, 0.25f);
            B.color = new Color(1, 1, 1, 0.25f);
        }
    }

    void OutGrass()
    {
        if (A.color.a != 1)
        {
            A.color = new Color(1, 1, 1, 1);
            B.color = new Color(1, 1, 1, 1);
        }
    }

    #endregion

    #region Check tỉ lệ gặp pokemon trên map

    void Check_OnBattleMapper()
    {
        if(IsMainPlayer)
        {
            Tileset tileset = Map.BattleMapper.Get(transform.position);
            if (tileset != null)
            {
                float ratio = Random.value;
                if(ratio <= tileset.BattleRatio)
                {
                    Debug.LogFormat("Goto Battle");
                    cUserMap.Request_GoTo_Battle_OnMap();
                }
            }
        }
    }

    #endregion

    #region Di chuyển 4 hướng
    
    public void MoveDown()
    {
        if (!isMoving)
        {
            Direction = enumDirection.Down;
            if (CanMove())
                StartCoroutine(IE_Move(new Vector3(0, -0.25f, 0), arrMoveDown));
        }
    }

    public void MoveUp()
    {
        if (!isMoving)
        {
            Direction = enumDirection.Up;
            if (CanMove())
                StartCoroutine(IE_Move(new Vector3(0, 0.25f, 0), arrMoveUp));
        }
    }

    public void MoveLeft()
    {
        if (!isMoving)
        {
            Direction = enumDirection.Left;
            if (CanMove())
                StartCoroutine(IE_Move(new Vector3(-0.25f,0 , 0), arrMoveLeft));
        }
    }

    public void MoveRight()
    {
        if (!isMoving)
        {
            Direction = enumDirection.Right;
            if (CanMove())
                StartCoroutine(IE_Move(new Vector3(0.25f, 0, 0), arrMoveRight));
        }
    }

    IEnumerator IE_Move(Vector3 Lengh, Sprite[] anim)
    {
        if (Speed > 0 && !isMoving)
        {
            isMoving = true;
            int count = 8;
            float time = (0.25f / Speed) / count;

            while (time < 0.02f && count > 1)
            {
                time *= 2;
                count /= 2;
                yield return null;
            }
                

            for (int i = 0; i < count; i++)
            {
                yield return new WaitForSeconds(time);
                ChangeSprite(anim, 0);
                transform.position += (Lengh / count);
            }
            for (int i = 0; i < count; i++)
            {
                yield return new WaitForSeconds(time);
                ChangeSprite(anim, 1);
                transform.position += (Lengh / count);
            }

            for (int i = 0; i < count; i++)
            {
                yield return new WaitForSeconds(time);
                ChangeSprite(anim, 2);
                transform.position += (Lengh / count);
            }
            for (int i = 0; i < count; i++)
            {
                yield return new WaitForSeconds(time);
                ChangeSprite(anim, 1);
                transform.position += (Lengh / count);
            }

            if (transform.position.x != (int)transform.position.x || transform.position.y != (int)transform.position.y)
                transform.position = new Vector3((int)transform.position.x, (int)transform.position.y, 0);

            isMoving = false;
        }
        SetupOderLayer();
        Check_On_Grass();
        Check_OnBattleMapper();
        Action_Event_AtTarget();
        yield return null;
    }

    IEnumerator IE_MoveDown()
    {
        isMoving = true;
        float step = 1;
        while(step > 0)
        {
            transform.position += new Vector3(0, -0.0625f * speed, 0);
            step -= 0.0625f * speed;
            int i = (int)((1 - step) / 0.25);
            if (i >= 3) i = 1;
            ChangeSprite(arrMoveDown, i);
            yield return new WaitForSeconds(0.02f);
        }
        isMoving = false;
        SetupOderLayer();
        Check_On_Grass();
        Check_OnBattleMapper();
        Action_Event_AtTarget();
    }

    IEnumerator IE_MoveUp()
    {
        isMoving = true;
        float step = 1;
        while (step > 0)
        {
            transform.position += new Vector3(0, 0.0625f * speed, 0);
            step -= 0.0625f * speed;
            int i = (int)((1 - step) / 0.25);
            if (i >= 3) i = 1;
            ChangeSprite(arrMoveUp, i);
            yield return new WaitForSeconds(0.02f);
        }
        isMoving = false;
        SetupOderLayer();
        Check_On_Grass();
        Check_OnBattleMapper();
        Action_Event_AtTarget();
    }

    IEnumerator IE_MoveLeft()
    {
        isMoving = true;
        float step = 1;
        while (step > 0)
        {
            transform.position += new Vector3(-0.0625f * speed,0 , 0);
            step -= 0.0625f * speed;
            int i = (int)((1 - step) / 0.25);
            if (i >= 3) i = 1;
            ChangeSprite(arrMoveLeft, i);
            yield return new WaitForSeconds(0.02f);
        }
        isMoving = false;
        Check_On_Grass();
        Check_OnBattleMapper();
        Action_Event_AtTarget();
    }

    IEnumerator IE_MoveRight()
    {
        isMoving = true;
        float step = 1;
        while (step > 0)
        {
            transform.position += new Vector3(0.0625f * speed, 0, 0);
            step -= 0.0625f * speed;
            int i = (int)((1 - step) / 0.25);
            if (i >= 3) i = 1;
            ChangeSprite(arrMoveRight, i);
            yield return new WaitForSeconds(0.02f);
        }
        isMoving = false;
        Check_On_Grass();
        Check_OnBattleMapper();
        Action_Event_AtTarget();
    }

    #endregion

    //-----
    #region Request Network
    public void Request_MoveDown()
    {
        if (!isMoving)
        {
            Direction = enumDirection.Down;
            if (CanMove(enumDirection.Down))
            {
                CheckLocationOnServer(Location.X, Location.Y - 1);
            }
        }
        MoveDown();
    }

    public void Request_MoveUp()
    {
        if (!isMoving)
        {
            Direction = enumDirection.Up;
            if (CanMove(enumDirection.Up))
                CheckLocationOnServer(Location.X, Location.Y + 1);
        }
        MoveUp();
    }

    public void Request_MoveLeft()
    {
        if (!isMoving)
        {
            Direction = enumDirection.Left;
            if (CanMove(enumDirection.Left))
                CheckLocationOnServer(Location.X - 1, Location.Y);
        }
        MoveLeft();
    }

    public void Request_MoveRight()
    {
        if (!isMoving)
        {
            Direction = enumDirection.Right;
            if (CanMove(enumDirection.Right))
                CheckLocationOnServer(Location.X + 1, Location.Y);
        }
        MoveRight();
    }

    public void CheckLocationOnServer(int X, int Y)
    {
        if(!isMoving)
            cUserMap.Request_Move(X, Y);
    }

    #endregion

    #region Action Event
    public void Action_Event()
    {
        int X = Location.X;
        int Y = Location.Y;
        switch(Direction)
        {
            case enumDirection.Down:
                Y -= 1; break;
            case enumDirection.Left:
                X -= 1; break;
            case enumDirection.Right:
                X += 1; break;
            case enumDirection.Up:
                Y += 1; break;
        }
        U_EventNPC npc = Map.EventNPC.Get(new Vector2(X, Y));
        if(npc != null)
        {
            npc.Process();
        }
    }

    public void Action_Event_AtTarget()
    {
        if (IsMainPlayer)
        {
            int X = Location.X;
            int Y = Location.Y;
            U_EventNPC npc = Map.EventNPC.Get(new Vector2(X, Y));
            if (npc != null && npc.TypeRun == EnumRunNPC.OnTouchCollison)
            {
                npc.Process();
            }
        }
    }
    #endregion
}

public class DirectionAble
{
    public bool Up, Right, Left, Down;
    public DirectionAble()
    {
        Up = Right = Left = Down = false;
    }

    public bool Linked(enumDirection direction ,DirectionAble dir)
    {
        switch(direction)
        {
            case enumDirection.Down: return Down && dir.Up;
            case enumDirection.Up: return Up && dir.Down;
            case enumDirection.Left: return Left && dir.Right;
            case enumDirection.Right: return Right && dir.Left;
        }
        return false;
    }

    public void CheckAble()
    {
        Up = Right = Left = Down = true;
    }

    public void CheckDisable()
    {
        Up = Right = Left = Down = false;
    }

    public void Check(enumTypeCollision type)
    {
        CheckDisable();
        switch (type)
        {
            case enumTypeCollision.None:
                CheckAble();
                break;
            case enumTypeCollision.Hard:
                CheckDisable();
                break;

            case enumTypeCollision.Down:
                Down = true;
                break;
            case enumTypeCollision.Left:
                Left = true;
                break;
            case enumTypeCollision.Up:
                Up = true;
                break;
            case enumTypeCollision.Right:
                Right = true;
                break;

            case enumTypeCollision.Down_Left:
                Down = true;
                Left = true;
                break;
            case enumTypeCollision.Down_Right:
                Down = true;
                Right = true;
                break;
            case enumTypeCollision.Down_Up:
                Down = true;
                Up = true;
                break;
            case enumTypeCollision.Left_Right:
                Left = true;
                Right = true;
                break;
            case enumTypeCollision.Left_Up:
                Left = true;
                Up = true;
                break;
            case enumTypeCollision.Right_Up:
                Right = true;
                Up = true;
                break;

            case enumTypeCollision.Down_Right_Left:
                Down = true;
                Right = true;
                Left = true;
                break;
            case enumTypeCollision.Down_Up_Left:
                Down = true;
                Up = true;
                Left = true;
                break;
            case enumTypeCollision.Down_Up_Right:
                Down = Up = Right = true;
                break;
            case enumTypeCollision.Up_Right_Left:
                Up = Right = Left = true;
                break;
        }
    }

    public override string ToString()
    {
        if (Up && Right && Left && Down)
            return "Can Able";
        if (!Up && !Right && !Left && !Down)
            return "Hard";
        string mess = "";
        if (Up) mess += "Up - ";
        if (Right) mess += "Right - ";
        if (Down) mess += "Down - ";
        if (Left) mess += "Left";
        return mess;

    }
}

public class Log
{
    public static bool CanLog = false;

    public static void Write(object obj)
    {
        if(CanLog)
            Debug.Log(obj);
    }

    public static void Write(string c,object obj)
    {
        Debug.Log(string.Format("<color={0}>{1}</color>",c,obj));
    }
}
