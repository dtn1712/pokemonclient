﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Map
{
    public static CollisionManager Collision;
    public static GrassTilesetManager GrassTileset;
    public static BattleMapperManager BattleMapper;
    public static EventNPCManager EventNPC;

    public static void Init()
    {
        Collision = new CollisionManager();
        GrassTileset = new GrassTilesetManager();
        BattleMapper = new BattleMapperManager();
        EventNPC = new EventNPCManager();
    }

    public static void Refesh()
    {
        if (Collision == null)
            Init();
        else
        {
            Collision.Units.Clear();
            Collision.StaticCollision.Clear();
            GrassTileset.Tilesets.Clear();
            BattleMapper.Tilesets.Clear();
            EventNPC.Events.Clear();
        }
    }

    public static string Key(Vector2 vector)
    {
        return string.Format("{0},{1}", (int)vector.x, (int)vector.y);
    }

    public static string Key(Vector vector)
    {
        return string.Format("{0},{1}", (int)vector.X, (int)vector.Y);
    }
}

public class CollisionManager
{
    public Dictionary<int, UnitMap> Units = new Dictionary<int, UnitMap>();
    public Dictionary<string, enumTypeCollision> StaticCollision = new Dictionary<string, enumTypeCollision>();
    
    int Count = 0;

    public List<UnitMap> Get(Vector vec, int layer = -1)
    {
        List <UnitMap> list = new List<UnitMap>();
        UnitMap u;
        if (layer <= -1)
        {
            for (int i = 0; i < Units.Count; i++)
            {
                u = Units.ElementAt(i).Value;
                if (u.Location == vec)
                    list.Add(Units.ElementAt(i).Value);
            }
        }
        else
        {
            for (int i = 0; i < Units.Count; i++)
            {
                u = Units.ElementAt(i).Value;
                if (u.Location == vec && u.LayerID <= layer)
                    list.Add(Units.ElementAt(i).Value);
            }
        }

        for (int i = 0; i < list.Count - 1; i++)
            for (int j = i + 1; j < list.Count; j++)
                if (list[i].LayerID < list[j].LayerID)
                {
                    UnitMap temp = list[i];
                    list[i] = list[j];
                    list[j] = temp;
                }
        return list;
    }

    public enumTypeCollision GetStaticCollision(Vector vec)
    {
        string key = Map.Key(vec);
        if (StaticCollision.ContainsKey(key))
            return StaticCollision[key];
        else
        {
            return enumTypeCollision.None;
        }
    }

    public enumTypeCollision GetStaticCollision(Vector2 vec)
    {
        string key = Map.Key(vec);
        if (StaticCollision.ContainsKey(key))
            return StaticCollision[key];
        else
        {
            return enumTypeCollision.None;
        }
    }

    public void Register(UnitMap unit)
    {
        if (unit.IsStaticCollision)
        {
            StaticCollision[Map.Key(unit.transform.position)] = unit.Type_Collision;
            UnityEngine.Object.Destroy(unit.gameObject);
        }
        else
        {
            for (int i = 0; i < Units.Count; i++)
            {
                UnitMap u = Units.ElementAt(i).Value;
                if (u.transform.position.x == unit.transform.position.x &&
                   u.transform.position.y == unit.transform.position.y)
                    Debug.Log("<color=yellow>Over: " + unit.gameObject.name + "</color>");
            }
            Units[Count] = unit;
            unit.ID_Collison = Count;
            Count++;
        }
        //Debug.Log("<color=yellow> Register Collision: " + unit.gameObject.name + "</color>");
    }
}

public class GrassTilesetManager
{
    public Dictionary<string, Tileset> Tilesets = new Dictionary<string, Tileset>();

    public void Register(Tileset tileset)
    {
        Vector2 vec = tileset.transform.position;
        string id_groundBattle = Map.Key(vec);

        for (int i = 0; i < Tilesets.Count; i++)
        {
            Tileset u = Tilesets.ElementAt(i).Value;
            if (u.transform.position.x == tileset.transform.position.x &&
               u.transform.position.y == tileset.transform.position.y)
                Debug.Log("<color=yellow>Over: " + tileset.gameObject.name + "</color>");
        }

        if (Tilesets.ContainsKey(id_groundBattle))
        {
            if (tileset.LayerID >= Tilesets[id_groundBattle].LayerID)
                Tilesets[id_groundBattle] = tileset;
        }
        else
        {
            Tilesets[id_groundBattle] = tileset;
        }
    }

    public Tileset Get(string key)
    {
        if (Tilesets.ContainsKey(key))
            return Tilesets[key];
        else
            return null;
    }
}

public class BattleMapperManager
{
    public Dictionary<string, Tileset> Tilesets = new Dictionary<string, Tileset>();

    public void Register(Tileset tileset)
    {
        Vector2 vec = tileset.transform.position;
        string id_groundBattle = Map.Key(vec);

        if (Tilesets.ContainsKey(id_groundBattle))
        {
            if (tileset.LayerID >= Tilesets[id_groundBattle].LayerID)
                Tilesets[id_groundBattle] = tileset;
        }
        else
        {
            Tilesets[id_groundBattle] = tileset;
        }
    }

    public Tileset Get(string key)
    {
        if (Tilesets.ContainsKey(key))
            return Tilesets[key];
        else
            return null;
    }

    public Tileset Get(Vector3 vec)
    {
        string key = Map.Key(vec);
        return Get(key);
    }

}

public class EventNPCManager
{
    public Dictionary<int, U_EventNPC> Events = new Dictionary<int, U_EventNPC>();

    int Count = 0;

    public void Register(U_EventNPC enpc)
    {
        Events[Count] = enpc;
        enpc.Id_NPC = Count;
        Count++;
    }

    public U_EventNPC Get(Vector2 vec)
    {
        U_EventNPC npc = null;
        for(int i = 0; i < Events.Count; i++)
        {
            U_EventNPC temp = Events.ElementAt(i).Value;
            int X = (int)temp.transform.position.x;
            int Y = (int)temp.transform.position.y;
            if(X == (int) vec.x && Y == (int) vec.y)
            {
                npc = temp;
                break;
            }
        }
        if (npc != null) return npc;
        else return null;
    }
}