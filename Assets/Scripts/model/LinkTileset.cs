﻿using UnityEngine;
using System.Collections;

public class LinkTileset : UnitMap {

    public Texture2D Texture;
    public byte Type_Index;

    protected override void OnDrawGizmosSelected()
    {
        base.OnDrawGizmosSelected();
        if(Texture != null)
        {
            if (Type_Index > 32) Type_Index = 32;
            Vector[] V = ToRect(Type_Index);
            for (int i = 0; i < 4; i++)
            {
                SpriteRenderer RS = transform.GetChild(i).GetComponent<SpriteRenderer>();
                RS.sprite = UnityEngine.Sprite.Create(Texture, new Rect((V[i].X) * 24, V[i].Y * 24, 24, 24), new Vector2(0, 1), 48);
            }
        }
    }
}
