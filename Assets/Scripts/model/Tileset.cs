﻿using UnityEngine;
using System.Collections;

public class Tileset : UnitMap {

    //properties
    public bool GroundBattles;
    public bool GroundGrass;
    public float BattleRatio;

    protected override void Awake()
    {
        base.Awake();
        OnRegisterGroundBattle += Regis_GroundBattle;
        OnRegisterGroundGrass += Regis_GroundGrass;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        OnRegisterGroundBattle -= Regis_GroundBattle;
        OnRegisterGroundGrass -= Regis_GroundGrass;
    }

    protected override void OnDrawGizmos()
    {
        base.OnDrawGizmos();
        if(GroundGrass)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireCube(transform.position + new Vector3(0.5f, -0.5f, 0), new Vector3(0.5f, 0.5f, 0));
        }
    }

    void Regis_GroundBattle(params object[] obj)
    {
        if (GroundBattles)
            Map.BattleMapper.Register(this);
    }

    void Regis_GroundGrass(params object[] obj)
    {
        if (GroundGrass)
        {
            Map.GrassTileset.Register(this);
        }
    }
}
