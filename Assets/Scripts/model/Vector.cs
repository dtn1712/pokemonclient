﻿using UnityEngine;
using System.Collections;

public class Vector {

    int x, y;

    public int X { get { return x; } set { x = value; } }
    public int Y { get { return y; } set { y = value; } }

    public Vector(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public Vector()
    {
        x = y = 0;
    }

    public Vector(Vector v)
    {
        x = v.x; y = v.y;
    }

    public Vector(Vector2 v)
    {
        x = (int)v.x; y = (int)v.y;
    }

    public Vector(Vector3 v)
    {
        x = (int)v.x; y = (int)v.y;
    }

    public override string ToString()
    {
        return "[" + x + "," + y + "]";
    }

    #region Operator
    public static Vector operator +(Vector a, Vector b)
    {
        return new Vector(a.x + b.x, a.y + b.y);
    }
    public static Vector operator -(Vector a, Vector b)
    {
        return new Vector(a.x - b.x, a.y - b.y);
    }

    public static Vector operator +(Vector a, Vector2 b)
    {
        return new Vector(a.x + (int)b.x, a.y + (int)b.y);
    }
    public static Vector operator -(Vector a, Vector2 b)
    {
        return new Vector(a.x - (int)b.x, a.y - (int)b.y);
    }

    public static Vector operator +(Vector a, Vector3 b)
    {
        return new Vector(a.x + (int)b.x, a.y + (int)b.y);
    }
    public static Vector operator -(Vector a, Vector3 b)
    {
        return new Vector(a.x - (int)b.x, a.y - (int)b.y);
    }

    public static Vector2 operator +(Vector2 a, Vector b)
    {
        return new Vector2(a.x + b.x, a.y + b.y);
    }
    public static Vector2 operator -(Vector2 a, Vector b)
    {
        return new Vector2(a.x - b.x, a.y - b.y);
    }

    public static Vector3 operator +(Vector3 a, Vector b)
    {
        return new Vector3(a.x + b.x, a.y + b.y, a.z);
    }
    public static Vector3 operator -(Vector3 a, Vector b)
    {
        return new Vector3(a.x - b.x, a.y - b.y, a.z);
    }
    
    public static bool operator ==(Vector a, Vector b)
    {
        if (a.x == b.x && a.y == b.y)
            return true;
        else
            return false;
    }
    public static bool operator !=(Vector a, Vector b)
    {
        if (a.x != b.x && a.y != b.y)
            return true;
        else
            return false;
    }
    #endregion
}
