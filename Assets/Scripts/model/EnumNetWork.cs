﻿using UnityEngine;
using System.Collections;

public enum EnumNetWork {
    
    Login,
    Regis,

    BackToGame,
    ChangeMap,
    MoveOnMap,
    UpdateLocation,
    Goto_LocalMap,
    Goto_Battle,

    Load_Battle,
    UpdatePokemon_User,
    UpdatePokemon_Enemy,
    Load_List_Pokemon,
    ChangedPokemon,
    Player_Atack,
    Enemy_Atack,
    Player_be_actacked,
    Enemy_be_atacked,
    Action_Complete,
    Arrest_Pokemon,
    Run_GoOut,
    Turn_Action,
    ContinuteBattle,
    Win_Battle,
    Lose_Battle,
    Arrest_Success,

    BackTo_Map,

    RecoverHP,
}
