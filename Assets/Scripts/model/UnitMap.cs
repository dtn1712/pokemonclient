﻿using UnityEngine;
using System.Collections;

public class UnitMap : MonoBehaviour {

    public delegate void ActionEvent(params object[] obj);
    public static ActionEvent OnRegisterCollision;
    public static ActionEvent OnRegisterGroundBattle;
    public static ActionEvent OnRegisterGroundGrass;

    protected SpriteRenderer Sprite;
    protected int CollisionValue = 0;

    public int LayerLevel
    {
        get { return Sprite.sortingOrder; }
        set { Sprite.sortingOrder = value; }
    }
    public int LayerID
    {
        get { return SortingLayer.GetLayerValueFromName( Sprite.sortingLayerName); }
        set { Sprite.sortingLayerName = SortingLayer.layers[value].name; }
    }
    public Vector Location
    {
        get { return new Vector(transform.position); }
        set { transform.position = new Vector3(value.X, value.Y);}
    }

    public bool IS_Collision = false;
    public int ID_Collison;
    public bool IsStaticCollision;
    public bool IsOnGizmos;
    public bool AutoPosition;
    public bool WriteLog;

    public enumTypeCollision Type_Collision = enumTypeCollision.None;
    

    protected virtual void Awake()
    {
        Sprite = transform.GetComponent<SpriteRenderer>();
        SetupOderLayer();
        OnRegisterCollision += RegisterCollision;
    }

    protected virtual void OnDestroy()
    {
        OnRegisterCollision -= RegisterCollision;
    }

    /// <summary>
    /// Giúp định hình position của Character trên Editor Unity
    /// </summary>
    //**************
    Texture2D iconGizmos;
    //**************
    protected virtual void OnDrawGizmosSelected()
    {
        if (IsOnGizmos)
        {
            if (AutoPosition)
            {
                Vector2 v = transform.position;
                transform.position = new Vector3((int)v.x, (int)v.y, 0);
            }
        }
    }

    protected virtual void OnDrawGizmos()
    {
        if (Type_Collision == enumTypeCollision.Hard)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position + new Vector3(0.5f, -0.5f, 0), 0.25f);
        }
        else if(Type_Collision == enumTypeCollision.None)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawWireCube(transform.position + new Vector3(0.5f, -0.5f, 0), new Vector3(0.6f,0.6f,0));
        }
        else
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(transform.position + new Vector3(0.5f, -0.5f, 0), new Vector3(0.5f, 0.5f, 0));
        }
    }

    /// <summary>
    /// Sắp xếp các đối tượng theo tung độ.
    /// Đối tượng có tung độ lớn sẽ được vẽ trước.
    /// </summary>
    protected void SetupOderLayer()
    {
        LayerLevel = (Location.Y * -1);
    }

    /// <summary>
    /// Đăng ký Collision
    /// </summary>
    protected void RegisterCollision(params object[] obj)
    {
        if (WriteLog)
            Debug.Log("<color=yellow>" + gameObject.name + "</color>");
        if(Type_Collision != enumTypeCollision.None || IS_Collision)
            Map.Collision.Register(this);
    }

    /// <summary>
    /// Ghép 1 Sprite tileset từ 4 sprite 24 pixel nhỏ hơn.
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    protected Vector[] ToRect(int type)
    {
        Vector[] V = new Vector[4];
        switch (type)
        {
            case 0:
                #region
                {
                    V[0] = new Vector(0, 5);
                    V[1] = new Vector(1, 5);
                    V[2] = new Vector(0, 4);
                    V[3] = new Vector(1, 4);
                }
                break;
            #endregion
            case 1:
                #region
                {
                    V[0] = new Vector(2, 3);
                    V[1] = new Vector(1, 3);
                    V[2] = new Vector(2, 0);
                    V[3] = new Vector(1, 0);
                }
                break;
            #endregion
            case 2:
                #region
                {
                    V[0] = new Vector(0, 1);
                    V[1] = new Vector(3, 1);
                    V[2] = new Vector(0, 2);
                    V[3] = new Vector(3, 2);
                }
                break;
            #endregion
            case 3:
                #region
                {
                    V[0] = new Vector(0, 3);
                    V[1] = new Vector(3, 3);
                    V[2] = new Vector(0, 2);
                    V[3] = new Vector(3, 2);
                }
                break;
            #endregion
            case 4:
                #region
                {
                    V[0] = new Vector(0, 1);
                    V[1] = new Vector(3, 1);
                    V[2] = new Vector(0, 0);
                    V[3] = new Vector(3, 0);
                }
                break;
            #endregion
            case 5:
                #region
                {
                    V[0] = new Vector(0, 3);
                    V[1] = new Vector(1, 3);
                    V[2] = new Vector(0, 0);
                    V[3] = new Vector(1, 0);
                }
                break;
            #endregion
            case 6:
                #region
                {
                    V[0] = new Vector(2, 3);
                    V[1] = new Vector(3, 3);
                    V[2] = new Vector(2, 0);
                    V[3] = new Vector(3, 0);
                }
                break;
            #endregion
            case 7:
                #region
                {
                    V[0] = new Vector(0, 3);
                    V[1] = new Vector(1, 3);
                    V[2] = new Vector(0, 2);
                    V[3] = new Vector(1, 2);
                }
                break;
            #endregion
            case 8:
                #region
                {
                    V[0] = new Vector(2, 3);
                    V[1] = new Vector(1, 3);
                    V[2] = new Vector(2, 2);
                    V[3] = new Vector(1, 2);
                }
                break;
            #endregion
            case 9:
                #region
                {
                    V[0] = new Vector(2, 3);
                    V[1] = new Vector(3, 3);
                    V[2] = new Vector(2, 2);
                    V[3] = new Vector(3, 2);
                }
                break;
            #endregion
            case 10:
                #region
                {
                    V[0] = new Vector(2, 1);
                    V[1] = new Vector(3, 1);
                    V[2] = new Vector(2, 2);
                    V[3] = new Vector(3, 2);
                }
                break;
            #endregion
            case 11:
                #region
                {
                    V[0] = new Vector(2, 1);
                    V[1] = new Vector(3, 1);
                    V[2] = new Vector(2, 0);
                    V[3] = new Vector(3, 0);
                }
                break;
            #endregion
            case 12:
                #region
                {
                    V[0] = new Vector(2, 1);
                    V[1] = new Vector(1, 1);
                    V[2] = new Vector(2, 0);
                    V[3] = new Vector(1, 0);
                }
                break;
            #endregion
            case 13:
                #region
                {
                    V[0] = new Vector(0, 1);
                    V[1] = new Vector(1, 1);
                    V[2] = new Vector(0, 0);
                    V[3] = new Vector(1, 0);
                }
                break;
            #endregion
            case 14:
                #region
                {
                    V[0] = new Vector(0, 1);
                    V[1] = new Vector(1, 1);
                    V[2] = new Vector(0, 2);
                    V[3] = new Vector(1, 2);
                }
                break;
            #endregion
            case 15:
                #region
                {
                    V[0] = new Vector(1, 2);
                    V[1] = new Vector(2, 2);
                    V[2] = new Vector(1, 1);
                    V[3] = new Vector(2, 1);
                }
                break;
            #endregion
            case 16:
                #region
                {
                    V[0] = new Vector(2, 5);
                    V[1] = new Vector(3, 5);
                    V[2] = new Vector(2, 4);
                    V[3] = new Vector(3, 4);
                }
                break;
            #endregion
            case 17:
                #region
                {
                    V[0] = new Vector(0, 1);
                    V[1] = new Vector(3, 5);
                    V[2] = new Vector(0, 2);
                    V[3] = new Vector(3, 4);
                }
                break;
            #endregion
            case 18:
                #region
                {
                    V[0] = new Vector(2, 3);
                    V[1] = new Vector(1, 3);
                    V[2] = new Vector(2, 4);
                    V[3] = new Vector(3, 4);
                }
                break;
            #endregion
            case 19:
                #region
                {
                    V[0] = new Vector(2, 5);
                    V[1] = new Vector(3, 5);
                    V[2] = new Vector(2, 0);
                    V[3] = new Vector(1, 0);
                }
                break;
            #endregion
            case 20:
                #region
                {
                    V[0] = new Vector(2, 5);
                    V[1] = new Vector(3, 1);
                    V[2] = new Vector(2, 4);
                    V[3] = new Vector(3, 2);
                }
                break;
            #endregion
            case 21:
                #region
                {
                    V[0] = new Vector(0, 3);
                    V[1] = new Vector(1, 3);
                    V[2] = new Vector(0, 2);
                    V[3] = new Vector(3, 4);
                }
                break;
            #endregion
            case 22:
                #region
                {
                    V[0] = new Vector(2, 3);
                    V[1] = new Vector(3, 3);
                    V[2] = new Vector(2, 4);
                    V[3] = new Vector(3, 2);
                }
                break;
            #endregion
            case 23:
                #region
                {
                    V[0] = new Vector(2, 5);
                    V[1] = new Vector(3, 1);
                    V[2] = new Vector(2, 0);
                    V[3] = new Vector(3, 0);
                }
                break;
            #endregion
            case 24:
                #region
                {
                    V[0] = new Vector(0, 1);
                    V[1] = new Vector(3, 5);
                    V[2] = new Vector(0, 0);
                    V[3] = new Vector(1, 0);
                }
                break;
            #endregion
            case 25:
                #region
                {
                    V[0] = new Vector(1, 2);
                    V[1] = new Vector(2, 2);
                    V[2] = new Vector(2, 4);
                    V[3] = new Vector(3, 4);
                }
                break;
            #endregion
            case 26:
                #region
                {
                    V[0] = new Vector(2, 5);
                    V[1] = new Vector(2, 2);
                    V[2] = new Vector(2, 4);
                    V[3] = new Vector(2, 1);
                }
                break;
            #endregion
            case 27:
                #region
                {
                    V[0] = new Vector(2, 5);
                    V[1] = new Vector(3, 5);
                    V[2] = new Vector(1, 1);
                    V[3] = new Vector(2, 1);
                }
                break;
            #endregion
            case 28:
                #region
                {
                    V[0] = new Vector(1, 2);
                    V[1] = new Vector(3, 5);
                    V[2] = new Vector(1, 1);
                    V[3] = new Vector(3, 4);
                }
                break;
            #endregion
            case 29:
                #region
                {
                    V[0] = new Vector(2, 5);
                    V[1] = new Vector(2, 2);
                    V[2] = new Vector(1, 1);
                    V[3] = new Vector(2, 1);
                }
                break;
            #endregion
            case 30:
                #region
                {
                    V[0] = new Vector(1, 2);
                    V[1] = new Vector(3, 5);
                    V[2] = new Vector(1, 1);
                    V[3] = new Vector(2, 1);
                }
                break;
            #endregion
            case 31:
                #region
                {
                    V[0] = new Vector(1, 2);
                    V[1] = new Vector(2, 2);
                    V[2] = new Vector(2, 4);
                    V[3] = new Vector(2, 1);
                }
                break;
            #endregion
            case 32:
                #region
                {
                    V[0] = new Vector(1, 2);
                    V[1] = new Vector(2, 2);
                    V[2] = new Vector(1, 1);
                    V[3] = new Vector(3, 4);
                }
                break;
                #endregion
        }

        return V;
    }
}

public struct SoInt
{
    public int num1, num2, num3;
    public string str1;
    public float f1;

    public SoInt(int n1, int n2, int n3, string s1, float f)
    {
        num1 = n1; num2 = n2; num3 = 3;
        str1 = s1; f1 = f;
    }
}
