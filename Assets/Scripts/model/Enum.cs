﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum enumDirection
{
    Up, Down, Left, Right
}

public enum enumTypeCollision
{
    None,
    Hard,
    //** 1 hướng
    Down,
    Left,
    Right,
    Up,

    //** 2 hướng
    Down_Up,
    Down_Right,
    Down_Left,

    Left_Up,
    Left_Right,

    Right_Up,

    //** 3 hướng
    Down_Up_Right,
    Down_Up_Left,
    Down_Right_Left,
    Up_Right_Left,
}
