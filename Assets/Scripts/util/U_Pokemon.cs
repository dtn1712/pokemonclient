﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class U_Pokemon : MonoBehaviour {

    public GameObject HPBar;
    public Text TxtName;

    public Sprite[] Sprites
    {
        get { return transform.GetChild(0).GetComponent<U_PokemonSprite>().Sprites; }
    }

    public Image Image
    {
        get { return GetComponent<Image>(); }
    }

	// Use this for initialization
    Vector3 Scale
    {
        get { return transform.localScale; }
        set { transform.localScale = value; }
    }
    int Index;

    //IEnumerator
    IEnumerator Breathing;

    void Awake()
    {
        Index = 0;
        if(Sprites.Length > 0)
            Image.sprite = Sprites[Index];
    }

    void Start () {
        Breathing = IE_Breathing();

        StartCoroutine(Breathing);
    }
	
	// Update is called once per frame
	void Update () {

	}

    void OnDestroy()
    {
        StopAllCoroutines();
    }

    IEnumerator IE_Breathing()
    {
        bool isbreathing = true;
        while(true)
        {
            if (Sprites.Length > 0)
            {
                Index = (Index + 1) % Sprites.Length;
                Image.sprite = Sprites[Index];
            }
            yield return new WaitForSeconds(0.2f);
        }
        yield return null;
    }

    public void GetDamge(float damge, int effect = 0)
    {
        if(HPBar.transform.localScale.x - damge <= 0)
            HPBar.transform.localScale = new Vector3(0, 1, 1);
        else
            HPBar.transform.localScale -= new Vector3(damge, 0, 0);

        StartCoroutine(Be_Atacked());
    }

    public void Atack(int skill)
    {
        cUserBattle.Player_Attack(skill);
    }

    public void SetName(string name)
    {
        TxtName.text = name;
        TxtName.transform.GetChild(0).GetComponent<Text>().text = name;
    }

    IEnumerator Be_Atacked()
    {
        Image img = GetComponent<Image>();
        img.color = new Color(1, 1, 1, 0);
        yield return new WaitForSeconds(0.2f);
        img.color = new Color(1, 1, 1, 1);
        if(HPBar.transform.localScale.x <= 0)
            img.color = new Color(1, 1, 1, 0);
       
    }
}
