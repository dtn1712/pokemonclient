﻿using UnityEngine;
using System.Collections;

public class U_Sound : MonoBehaviour {

    public AudioSource Sound
    {
        get { return GetComponent<AudioSource>(); }
    }
    
    public void Play()
    {
        Sound.Play();
        StartCoroutine(IE_PlayComplete(Sound.clip.length));
    }

    IEnumerator IE_PlayComplete(float time)
    {
        yield return new WaitForSeconds(time);
        Object.Destroy(gameObject);
    }
	

}
