﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class U_Effect : MonoBehaviour {

    public Sprite[] Sprites;
    Action<string> ActionComplete;
    Image Image
    {
        get { return GetComponent<Image>(); }
    }

	public void Show(Action<string> action)
    {
        StartCoroutine(IE_Show());
        ActionComplete = action;
    }

    IEnumerator IE_Show()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            if (i - 1 >= 0)
            {
                transform.GetChild(i - 1).gameObject.SetActive(false);
            }
            transform.GetChild(i).gameObject.SetActive(true);
            if (transform.GetChild(i).GetComponent<AudioSource>() != null)
            {
                GameObject sound = Instantiate(Resources.Load<GameObject>("prefabs/sound"));
                U_Sound uS = sound.GetComponent<U_Sound>();
                uS.Sound.clip = transform.GetChild(i).GetComponent<AudioSource>().clip;
                uS.Play();
            }
            yield return new WaitForSeconds(0.05f);
        }
        if (ActionComplete != null) ActionComplete("attack");
        Destroy();
    }

    public void Destroy()
    {
        UnityEngine.Object.Destroy(gameObject);
    }

}
