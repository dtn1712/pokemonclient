﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class U_MapExample : MonoBehaviour {

    public SpriteRenderer SpriteRender
    {
        get { return GetComponent<SpriteRenderer>(); }
    }
    public float Opaticy;

    private void OnDrawGizmosSelected()
    {
        Color c = SpriteRender.color;
        SpriteRender.color = new Color(c.r, c.g, c.b, Opaticy);
    }
}
