﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class U_Led : MonoBehaviour {

    public Sprite[] Leds;
    public bool IsTurnOn;
    int index;

    public int Index
    {
        get { return index; }
        set { index = value;
            GetComponent<Image>().sprite = Leds[index];
        }
    }

    void Awake()
    {
        if (Leds.Length > 0)
        {
            if (!IsTurnOn)
            {
                GetComponent<Image>().sprite = Leds[0];
                index = 0;
            }
            else
            {
                GetComponent<Image>().sprite = Leds[1];
                index = 1;
            }
        }
        else
        {
            index = -1;
        }
    }

    /// <summary>
    /// Mặc định Index 1 là ON
    /// </summary>
    public void TURN_ON()
    {
        if (Leds.Length > 1)
            GetComponent<Image>().sprite = Leds[1];
    }

    /// <summary>
    /// Mặc định Index 0 là OFF
    /// </summary>
    public void TURN_OFF()
    {
        if (Leds.Length > 0)
            GetComponent<Image>().sprite = Leds[0];
    }
}
