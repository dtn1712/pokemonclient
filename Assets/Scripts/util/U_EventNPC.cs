﻿using UnityEngine;
using System.Collections;

public class U_EventNPC : MonoBehaviour {

    public delegate void ActionEvent(params object[] obj);
    public static ActionEvent OnRegisEventNPC;

    public int Id_NPC;
    public int X, Y;
    public EnumRunNPC TypeRun = EnumRunNPC.OnAction;
    public EnumEventNPC EventCode = EnumEventNPC.Log;
    public string[] parameters;

    private Texture2D icon_event;

    void Awake()
    {
        OnRegisEventNPC += Regis;
    }

    private void OnDestroy()
    {
        OnRegisEventNPC -= Regis;
    }

    public virtual void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position + new Vector3(0.5f, -0.5f), 0.25f);
        X = (int)transform.position.x;
        Y = (int)transform.position.y;
    }

    public void Process()
    {
        cEventNPC.Process(gameObject, EventCode, parameters);
    }

    void Regis(params object[] obj)
    {
        Map.EventNPC.Register(this);
    }
}

public enum EnumRunNPC
{
    OnAction,
    OnTouchCollison,
}
