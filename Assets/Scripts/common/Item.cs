﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Item
{
    public int ID { get; set; }
    public string Name { get; set; }
    public string Describe { get; set; }

    public Item(int id)
    {
        ID = id;
    }

    public Item(int id, string name)
    {
        ID = id;
        Name = name;
    }
}
