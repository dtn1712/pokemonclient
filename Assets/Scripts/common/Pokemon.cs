﻿using UnityEngine;
using System.Collections;

public class Pokemon {

	public string Name { set; get; }

    public float PAtk { set; get; }
    public float PDef { set; get; }
    public float MAtk { set; get; }
    public float MDef { set; get; }
    public float Agi { set; get; }
    public float Ini { set; get; }

    public int HP { set; get; }
    public int MaxHP { set; get; }

    public Skill[] Skills;

    public Pokemon()
    {
        PAtk = 0;
        PDef = 0;
        MAtk = 0;
        MDef = 0;
        Agi = 0;
        Ini = 0;

        Name = "???";
        Skills = new Skill[4];
    }

    public Pokemon(string name)
    {
        Name = name;
        Skills = new Skill[4];
    }

    /// <summary>
    /// Thiết lập thông số thuộc tính
    /// </summary>
    /// <param name="patk">Physic Atk</param>
    /// <param name="pdef">Physic Defence</param>
    /// <param name="matk">Magic Atk</param>
    /// <param name="mdef">Magic Defence</param>
    /// <param name="agi">AGI</param>
    /// <param name="ini">INI</param>
    public void SetProperties(float patk,float pdef, float matk, float mdef, float agi, float ini)
    {
        PAtk = patk;
        PDef = pdef;
        MAtk = matk;
        MDef = mdef;
        Agi = agi;
        Ini = ini;
    }

    public void SetHP(int hp, int max_hp)
    {
        HP = hp;
        MaxHP = max_hp;
    }

    public float Ini_Pow
    {
        get { return Mathf.Pow(Ini, 0.2f); }
    }

    public float Ini_Random
    {
        get { return Mathf.Pow(Ini, (float)1 / 11) - 1; }
    }

    public int Skill_Can_Use
    {
        get
        {
            int count = 0;
            for (int i = 0; i < Skills.Length; i++)
                if (Skills[i] != null && Skills[i].Name.Length > 0)
                    count++;
            return count;
        }
    }

    public bool CanFighter
    {
        get { return HP > 0; }
    }

}
