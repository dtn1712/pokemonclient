﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Skill
{
    public string Name { get ; set; }

    public float Power { get; set; }

    public Skill (string name, float power)
    {
        Name = name;
        Power = power;
    }
}
