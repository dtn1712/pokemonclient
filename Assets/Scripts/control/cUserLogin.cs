﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using IClient;
using System;

public class cUserLogin : MonoBehaviour {

    //public static IServerPeer Peer;
    public static Account Player;
    public static int Id;
    void Start()
    {
        //Peer = new IServerPeer();
        //Peer.Connect("192.168.1.10", 11111);

        //===
        Player = new Account();
    }
    
    void Update()
    {
        //Peer.Service();
    }

    public static void Login(string user, string pass)
    {
		SocketConnection accountServer = SocketManager.getInstance ().getServerConnection (ServerType.ACCOUNT);
		Message msg = new Message ();
		msg.SetCommandId (CommandType.COMMAND_LOGIN);
		msg.SetProtocolVersion(1);

		LoginDTO loginDTO = new LoginDTO (AccountLoginType.USERNAME.ToString(), user, pass);
		msg.PutString(Message.DataKey, JsonUtility.ToJson (loginDTO));

		accountServer.sendMessage (msg);
//		MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Login);
//        mess.Parameters[0] = user;
//        mess.Parameters[1] = pass;
//        SendMessage(mess);
    }

    public static void Register(string user, string pass)
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Regis);
        mess.Parameters[0] = user;
        mess.Parameters[1] = pass;
        SendMessage(mess);
    }

    public static void GoToGame()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.BackToGame);
        SendMessage(mess);
    }

    public static void Load_Info_Account()
    {
        //Packed pack = new Packed((int)PokemonClientCode.LoadInfoAccount);
        //Peer.SendPacked(pack);
    }

    public static void OnDisconnect()
    {

    }

    public static void SendMessage(MessageNetwork mess)
    {
        VirtualServer.ReceiveMessage(mess);
    }

	[Serializable]
	public class LoginDTO {
		public string loginType;
		public string loginKey;
		public string loginValue;

		public LoginDTO(string _loginType, string _loginKey, string _loginValue) {
			this.loginType = _loginType;
			this.loginKey = _loginKey;
			this.loginValue = _loginValue;
		}
	}
}
