﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class cUserMap
{
    public static void SendMessage(MessageNetwork mess)
    {
        VirtualServer.ReceiveMessage(mess);
    }

    public static void Request_Move(int x, int y)
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.MoveOnMap);
        mess.Parameters[0] = x;
        mess.Parameters[1] = y;
        SendMessage(mess);
    }
    
    public static void Request_ChangeMap(string Map, int X, int Y, enumDirection Direction)
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.ChangeMap);
        mess.Parameters[0] = Map;
        mess.Parameters[1] = X;
        mess.Parameters[2] = Y;
        mess.Parameters[3] = (int) Direction;
        SendMessage(mess);
    }

    public static void Request_GoTo_Battle_OnMap()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Goto_Battle);
        SendMessage(mess);
    }

    public static void Request_GoTo_Local_Map(int X, int Y)
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Goto_LocalMap);
        mess.Parameters[0] = X;
        mess.Parameters[1] = Y;
        SendMessage(mess);
    }
}
