﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class cUserBattle {

    public delegate void BattleAction(Dictionary<byte, object> dic);
    public static BattleAction Enemy_Be_Atacked;
    public static BattleAction Player_Be_Atacked;
    public static BattleAction Load_Battle_Info;
    public static BattleAction Arrest_Pokemon;
    public static BattleAction Run_GoOut;
    public static BattleAction Load_ListPokemon;
    public static BattleAction ChangedPokemon;
    public static BattleAction Turn_Action;
    public static BattleAction ContinuteBattle;
    public static BattleAction Win_Battle;
    public static BattleAction Lose_Battle;
    public static BattleAction Arrest_Success;
    public static void SendMessage(MessageNetwork mess)
    {
        VirtualServer.ReceiveMessage(mess);
    }


    public static void LoadBattle()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Load_Battle);
        cUserBattle.SendMessage(mess);
    }
    public static void Player_Attack(int skill)
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Player_Atack);
        mess.Parameters[0] = skill;
        cUserBattle.SendMessage(mess);
    }


    //Hàm ảo mô phỏng sự lựa chọn hành động của enemy
    public static void Enemy_Attack()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Enemy_Atack);
        mess.Parameters[0] = -1;
        cUserBattle.SendMessage(mess);
    }

    public static void Arrest()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Arrest_Pokemon);
        SendMessage(mess);
    }

    public static void CompleteAction(string action)
    {
        Debug.Log("Complete Action");
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Action_Complete);
        mess.Parameters[0] = action;
        cUserBattle.SendMessage(mess);
    }

    public static void BackTo_Map()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.BackTo_Map);
        SendMessage(mess);
    }

    public static void Run_Out()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Run_GoOut);
        SendMessage(mess);
    }

    public static void Load_PokemonList()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Load_List_Pokemon);
        SendMessage(mess);
    }

    public static void ChangePokemon(int Index)
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.ChangedPokemon);
        mess.Parameters[0] = Index;
        mess.Parameters[1] = false;
        SendMessage(mess);
    }

    public static void ChoosePokemonFighter(int Index)
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.ChangedPokemon);
        mess.Parameters[0] = Index;
        mess.Parameters[1] = true;
        SendMessage(mess);
    }
}
