﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class c_battle : MonoBehaviour {

    public GameObject Canvas_UI, Canvas_Action;
    public GameObject Team, Enemy, FromPokeball, ToPokeball, Point_In, Point_Out,
                      List_Ball_Team, List_Ball_Enemy;
    public GameObject UI_PokemonUser, UI_PokemonEnemy;
    public GameObject
        Action_Fight,
        Action_Atack,
        Action_Skill,
        Panel_Result,
        Panel_ChoosePokemon,
        Panel_Continute_Battle;

    public Text[] Skills;

    public Image Win_Texture, Lose_Texture;
    public AudioClip Win_1, Win_2, Lose;

    bool IsContinuteBattle;

    void Awake()
    {
        cUserBattle.Enemy_Be_Atacked += Enemey_be_atacked;
        cUserBattle.Player_Be_Atacked += Player_be_atacked;
        cUserBattle.Load_Battle_Info += Load_Battle_Info;
        cUserBattle.Turn_Action += Turn_Action;
        cUserBattle.Arrest_Pokemon += Arrest_Pokemon;
        cUserBattle.Win_Battle += Win_Battle;
        cUserBattle.Lose_Battle += Lose_Battle;
        cUserBattle.Arrest_Success += Arrest_Success;
        cUserBattle.Run_GoOut += Run_GoOut;
        cUserBattle.ChangedPokemon += ChangePokemon;
        cUserBattle.ContinuteBattle += ContinuteBattle;

        VirtualClient.OnChangeMap += OnChangeMap;

        IsContinuteBattle = false;
    }

	// Use this for initialization
	void Start () {
        Hide_All_Ball();
        cUserBattle.LoadBattle();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnDestroy()
    {
        cUserBattle.Enemy_Be_Atacked -= Enemey_be_atacked;
        cUserBattle.Player_Be_Atacked -= Player_be_atacked;
        cUserBattle.Load_Battle_Info -= Load_Battle_Info;
        cUserBattle.Turn_Action -= Turn_Action;
        cUserBattle.Arrest_Pokemon -= Arrest_Pokemon;
        cUserBattle.Win_Battle -= Win_Battle;
        cUserBattle.Lose_Battle -= Lose_Battle;
        cUserBattle.Arrest_Success -= Arrest_Success;
        cUserBattle.Run_GoOut -= Run_GoOut;
        cUserBattle.ChangedPokemon -= ChangePokemon;
        cUserBattle.ContinuteBattle -= ContinuteBattle;

        VirtualClient.OnChangeMap -= OnChangeMap;
    }

    #region Event
    
    void OnChangeMap(Dictionary<byte, object> dic)
    {
        string name = (string)dic[0];
        int x = (int)dic[1];
        int y = (int)dic[2];
        enumDirection direction = enumDirection.Down;
        if(dic.Count > 3)
            direction = (enumDirection)dic[3];
        cMap.TransportMap(name, x, y, direction);
    }

    /// <summary>
    /// Load tin về battle room
    /// </summary>
    /// <param name="dic"></param>
    void Load_Battle_Info(Dictionary<byte, object> dic)
    {
        //Load User Spriter
        Team.GetComponent<U_Pokemon>().SetName((string)dic[0]);
        Load_Sprite_PokemonUser((string)dic[0]);
        Team.GetComponent<U_Pokemon>().HPBar.transform.localScale = new Vector3((float)dic[1], 1, 1);
        Show_Balls_User((int)dic[2]);

        //Load Enemy Spriter
        Enemy.GetComponent<U_Pokemon>().SetName((string)dic[3]);
        Load_Sprite_PokemonEnemy((string)dic[3]);
        Enemy.GetComponent<U_Pokemon>().HPBar.transform.localScale = new Vector3((float)dic[4], 1, 1);
        Show_Balls_Enemy((int)dic[5]);

        for (int i = 0; i < Skills.Length; i++)
        {
            string skill = (string)dic[(byte)(i + 6)];
            if (skill != null && skill.Length > 0)
            {
                Skills[i].text = skill;
                Skills[i].transform.parent.gameObject.SetActive(true);
            }
            else
            {
                Skills[i].transform.parent.gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// Hiện một hoạt cảnh kẻ thù bị tấn công.
    /// </summary>
    /// <param name="dic"></param>
    void Enemey_be_atacked(Dictionary<byte, object> dic)
    {
        float damge = (float) dic[0];
        Add_Skill(dic[1].ToString(), Enemy);
        Enemy.GetComponent<U_Pokemon>().GetDamge(damge);
    }

    /// <summary>
    /// Hiện một hoạt cảnh player bị tấn công
    /// </summary>
    /// <param name="dic"></param>
    void Player_be_atacked(Dictionary<byte, object> dic)
    {
        float damge = (float)dic[0];
        Add_Skill(dic[1].ToString(), Team);
        Team.GetComponent<U_Pokemon>().GetDamge(damge);
    }
    IEnumerator Enemy_selection_random()
    {
        yield return new WaitForSeconds(3);
        cUserBattle.Enemy_Attack();
    }

    void Add_Skill(string name, GameObject go)
    {
        Debug.LogWarning(name);
        GameObject skill = (GameObject) Instantiate(Resources.Load<GameObject>("prefabs/effect/" + name), Canvas_UI.transform);
        skill.transform.position = go.transform.position;
        skill.GetComponent<U_Effect>().Show(CompleteAction);
    }

    public void CompleteAction(string code)
    {
        cUserBattle.CompleteAction(code);
    }

    public void Turn_Action(Dictionary<byte, object> dic)
    {
        string actionof = dic[0].ToString();
        if(actionof == "enemy")
        {
            StartCoroutine(Enemy_selection_random());
        }
        else
        {
            Action_Atack.SetActive(true);
        }
    }

    public void Arrest_Pokemon(Dictionary<byte, object> dic)
    {
        GameObject g = (GameObject) Instantiate(Resources.Load<GameObject>("prefabs/Poke ball"), Canvas_UI.transform);
        V_PokeBall v_poke = g.GetComponent<V_PokeBall>();
        v_poke.Pokemon = Enemy;
        v_poke.To_Point = ToPokeball.transform;
        v_poke.KetQua = (bool) dic[0];
        v_poke.transform.position = FromPokeball.transform.position;
        v_poke.Run();
    }

    public void Run_GoOut(Dictionary<byte, object> dic)
    {
        CompleteAction("go_out");
    }

    public void Win_Battle(Dictionary<byte, object> dic)
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.clip = Win_1;
        audio.loop = false;
        audio.Play();
        Win_Texture.transform.localScale = new Vector3(0, 1, 1);
        Panel_Result.SetActive(true);
        StartCoroutine(IE_WinBattle());
    }

    public void ContinuteBattle(Dictionary<byte, object> dic)
    {
        Panel_Continute_Battle.SetActive(true);
        IsContinuteBattle = true;
    }

    IEnumerator IE_WinBattle()
    {
        Win_Texture.gameObject.SetActive(true);
        for (int i = 0; i < 20; i++)
        {
            Win_Texture.transform.localScale += new Vector3(0.05f, 0, 0);
            yield return new WaitForSeconds(0.02f);
        }
    }

    public void Lose_Battle(Dictionary<byte, object> dic)
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.clip = Lose;
        audio.loop = false;
        audio.Play();
        Lose_Texture.transform.localScale = new Vector3(0, 1, 1);
        Panel_Result.SetActive(true);
        StartCoroutine(IE_LoseBattle());
    }
    IEnumerator IE_LoseBattle()
    {
        Lose_Texture.gameObject.SetActive(true);
        for (int i = 0; i < 20; i++)
        {
            Lose_Texture.transform.localScale += new Vector3(0.05f, 0, 0);
            yield return new WaitForSeconds(0.05f);
        }
    }

    public void ChangePokemon(Dictionary<byte, object> dic)
    {
        bool success = (bool)dic[0];
        if(success)
        {
            Panel_ChoosePokemon.GetComponent<V_ChoosePokemonPanel>().IsStart = false;
            StartCoroutine(IE_ChangePokemon(dic));
        }
        else
        {
            Debug.LogWarning(dic[1]);
        }
    }

    IEnumerator IE_ChangePokemon(Dictionary<byte, object> dic)
    {
        UI_PokemonUser.GetComponent<Image>().color = new Color(1, 1, 1, 1);
        Panel_ChoosePokemon.SetActive(false);
        Team.GetComponent<U_Pokemon>().SetName("");
        Team.GetComponent<U_Pokemon>().HPBar.SetActive(false);
        Vector3 vec = new Vector3(UI_PokemonUser.transform.position.x - Point_In.transform.position.x,
            UI_PokemonUser.transform.position.y - Point_In.transform.position.y,0 );
        vec /= 20;
        for(int i = 0; i < 20; i++)
        {
            UI_PokemonUser.transform.position -= vec;
            UI_PokemonUser.transform.localScale -= new Vector3(0.05f, 0.05f, 0.05f);
            yield return new WaitForSeconds(0.02f);
        }

        Load_Sprite_PokemonUser((string)dic[1]);

        Transform parent = Team.transform.parent;
        parent.GetChild(0).gameObject.SetActive(true);
        parent.GetChild(1).gameObject.SetActive(true);
        parent.GetChild(2).gameObject.SetActive(true);
        for (int i = 0; i < 20; i++)
        {
            UI_PokemonUser.transform.position += vec;
            UI_PokemonUser.transform.localScale += new Vector3(0.05f, 0.05f, 0.05f);
            yield return new WaitForSeconds(0.02f);
        }
        Team.GetComponent<U_Pokemon>().SetName((string)dic[1]);
        Team.GetComponent<U_Pokemon>().HPBar.SetActive(true);
        Team.GetComponent<U_Pokemon>().HPBar.transform.localScale = new Vector3((float)dic[2], 1, 1);
        for (int i = 0; i < Skills.Length; i++)
        {
            string skill = (string)dic[(byte)(i + 3)];
            if (skill != null && skill.Length > 0)
            {
                Skills[i].text = skill;
                Skills[i].transform.parent.gameObject.SetActive(true);
            }
            else
            {
                Skills[i].transform.parent.gameObject.SetActive(false);
            }
        }
        cUserBattle.CompleteAction("attack");
        yield return null;
    }

    public void Arrest_Success(Dictionary<byte, object> dic)
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.clip = Win_2;
        audio.loop = false;
        audio.Play();
        Win_Texture.transform.localScale = new Vector3(0, 1, 1);
        Panel_Result.SetActive(true);
        StartCoroutine(IE_WinBattle());
    }
    #endregion

    #region List Ball of Pokeball

    public void Hide_All_Ball()
    {
        for (int i = 0; i < List_Ball_Team.transform.childCount; i++)
            List_Ball_Team.transform.GetChild(i).gameObject.SetActive(false);
        for (int i = 0; i < List_Ball_Enemy.transform.childCount; i++)
            List_Ball_Enemy.transform.GetChild(i).gameObject.SetActive(false);
    }

    public void Show_Balls_User(int count)
    {
        for (int i = 0; i < count; i++)
            List_Ball_Team.transform.GetChild(i).gameObject.SetActive(true);
    }

    public void Show_Balls_Enemy(int count)
    {
        for (int i = 0; i < count; i++)
            List_Ball_Enemy.transform.GetChild(i).gameObject.SetActive(true);
    }

    #endregion

    #region Method
    public void Load_Sprite_PokemonUser(string name)
    {
        for (int i = 0; i < Team.transform.childCount; i++)
            Object.Destroy(Team.transform.GetChild(i).gameObject);
        GameObject sprite = (GameObject)Instantiate(Resources.Load<GameObject>("prefabs/battle/player/" + name),Team.transform);
        
    }
    public void Load_Sprite_PokemonEnemy(string name)
    {
        for (int i = 0; i < Enemy.transform.childCount; i++)
            Object.Destroy(Enemy.transform.GetChild(i).gameObject);
        GameObject sprite = (GameObject)Instantiate(Resources.Load<GameObject>("prefabs/battle/enemy/" + name), Enemy.transform);
    }
    #endregion

    #region Fighter
    public void btn_fight()
    {
        Action_Fight.SetActive(false);
        Action_Atack.SetActive(true);
    }

    #endregion

    #region Atack
    public void btn_atack()
    {
        Action_Atack.SetActive(false);
        Action_Skill.SetActive(true);
    }

    public void btn_use_item()
    {

    }

    public void btn_arrest()
    {
        cUserBattle.Arrest();
        Action_Atack.SetActive(false);
    }

    public void btn_ChangePokemon()
    {
        Action_Atack.SetActive(false);
        Panel_Continute_Battle.SetActive(false);
        Panel_ChoosePokemon.SetActive(true);
    }

    public void btn_CancelChangePokemon()
    {
        if (IsContinuteBattle)
            Panel_Continute_Battle.SetActive(true);
        else
            Action_Atack.SetActive(true);
        Panel_ChoosePokemon.SetActive(false);
    }

    public void btn_RunGoOut()
    {
        cUserBattle.Run_Out();
        Action_Atack.SetActive(false);
    }

    #endregion

    #region Skill

    public void skill(int index)
    {
        Team.GetComponent<U_Pokemon>().Atack(index);
        Action_Skill.SetActive(false);
    }

    public void back()
    {
        Action_Skill.SetActive(false);
        Action_Atack.SetActive(true);
    }

    #endregion

    #region Result
    public void Result_OK()
    {
        cUserBattle.BackTo_Map();
    }
    #endregion
}
