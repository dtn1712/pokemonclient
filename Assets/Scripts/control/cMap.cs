﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class cMap : MonoBehaviour {

    public GameObject Camera;
    public GameObject CanvasNPC;
    public GameObject CanvasUI;
    public GameObject CanvasDialog;
    public GameObject Player;

    #region *** Start Player
    public static Vector2 PositionPlayer;
    public static enumDirection DirectionPlayer;
    #endregion

    public static void TransportMap(string name, int x, int y, enumDirection direction)
    {
        PositionPlayer = new Vector2(x, y);
        DirectionPlayer = direction;
        Application.LoadLevel(name);
    }

    void Awake()
    {

        //***Check login
        if (!cLogin.LoginSuccesful)
            Application.LoadLevel("SceneLogin");

        CanvasNPC = GameObject.FindGameObjectWithTag("CanvasNPC");
        if (CanvasNPC != null)
            Log.Write("<color=blue>" + CanvasNPC.name + "</color>");

        //*** Check Gameobject CanvasUI
        if(GameObject.FindGameObjectWithTag("Canvas UI") == null)
        {
            CanvasUI = Instantiate(Resources.Load<GameObject>("Prefabs/Map/Canvas UI"));
            CanvasUI.name = "Canvas UI";
        }
        else
        {
            CanvasUI = GameObject.FindGameObjectWithTag("Canvas UI");
        }

        //*** Check GameObject Canvas Dialog
        if (GameObject.FindGameObjectWithTag("Canvas Dialog") == null)
        {
            CanvasDialog = Instantiate(Resources.Load<GameObject>("Prefabs/Map/Canvas Dialog"));
            CanvasDialog.name = "Canvas Dialog";
        }
        else
            CanvasDialog = GameObject.FindGameObjectWithTag("Canvas Dialog");

        Map.Refesh();
        Instance_Main_Player();
        
        VirtualClient.OnUpdateLocation += OnUpdateLocation;
        VirtualClient.OnChangeMap += OnChangeMap;
        VirtualClient.OnGoToBattle += OnGoToBattle;
        VirtualClient.OnGotoLocalMap += OnGotoLocalMap;
    }

    private void OnGotoLocalMap(Dictionary<byte, object> dic)
    {
        int X = (int) dic[0]; int Y = (int)dic[1];
        Player.transform.position = new Vector3(X, Y, 0); 
    }

    private void Start()
    {
        if (UnitMap.OnRegisterCollision != null) UnitMap.OnRegisterCollision();
        if (UnitMap.OnRegisterGroundBattle != null) UnitMap.OnRegisterGroundBattle();
        if (UnitMap.OnRegisterGroundGrass != null) UnitMap.OnRegisterGroundGrass();
        if (U_EventNPC.OnRegisEventNPC != null) U_EventNPC.OnRegisEventNPC();
        
    }

    private void OnGoToBattle(Dictionary<byte, object> dic)
    {
        Debug.Log("Id Room Battle: " + dic[0]);
        Application.LoadLevel("battle");
    }

    void OnDestroy()
    {
        VirtualClient.OnUpdateLocation -= OnUpdateLocation;
        VirtualClient.OnChangeMap -= OnChangeMap;
        VirtualClient.OnGoToBattle -= OnGoToBattle;
        VirtualClient.OnGotoLocalMap -= OnGotoLocalMap;
    }

    private void OnUpdateLocation(Dictionary<byte, object> dic)
    {
        int X = (int) dic[0];
        int Y = (int) dic[1];
        StartCoroutine(IE_OnUpdateLocation(X, Y));
    }

    IEnumerator IE_OnUpdateLocation(int X, int Y)
    {
        Character player = Player.GetComponent<Character>();
        yield return new WaitForSeconds(0.25f);
        while(player.IsMoving)
        {
            yield return null;
        }
        player.Location = new Vector(X, Y);
    }

    void Instance_Main_Player()
    {
        Player = (GameObject)Instantiate(Resources.Load("prefabs/Character/Player 2"));
        Player.GetComponent<Character>().IsMainPlayer = true;
        Player.GetComponent<Character>().Direction = DirectionPlayer;
        Player.transform.position = new Vector3(PositionPlayer.x, PositionPlayer.y, 0);
        Camera.transform.parent = Player.transform;
        Camera.transform.localPosition = new Vector3(0, 0, -10);
        Player.transform.parent = CanvasNPC.transform;

        Character.Player = Player.GetComponent<Character>();
    }

    private void OnChangeMap(Dictionary<byte, object> dic)
    {
        string name = (string)dic[0];
        int x = (int)dic[1];
        int y = (int)dic[2];
        enumDirection direction = (enumDirection)dic[3];
        cMap.TransportMap(name, x, y, direction);
    }
}
