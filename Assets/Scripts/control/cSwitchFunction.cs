﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class cSwitchFunction
{
    public static void Function(GameObject sender, SwitchFunction SFObject)
    {
        switch(SFObject.FunctinoName.ToLower())
        {
            case "recover_hp":
                RecoverHPAllPokemon();
                break;
            case "change_map":
                Change_Map(SFObject.Parameters);
                break;
            case "about_demo":
                About_Demo(sender);
                break;
        }
    }

    //------
    public static void RecoverHPAllPokemon()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.RecoverHP);
        VirtualClient.SendMessage(mess);
    }

    public static void Change_Map(List<string> param)
    {
        string MapName = param[0];
        int posX = int.Parse(param[1]);
        int posY = int.Parse(param[2]);
        enumDirection direction = Character.Player.Direction;
        if (param.Count > 3)
        {
            switch (param[3].ToLower())
            {
                case "left": direction = enumDirection.Left; break;
                case "right": direction = enumDirection.Right; break;
                case "up": direction = enumDirection.Up; break;
                case "down": direction = enumDirection.Down; break;
            }
        }
        cUserMap.Request_ChangeMap(MapName, posX, posY, direction);
    }

    public static void About_Demo(GameObject sender)
    {
        string[] list = new string[3];
        list[0] = "Demo này mô tả những chức năng có thể có khi kích hoạt Event.";
        list[1] = "right";
        list[2] = "System";
        cEventNPC.Message(sender, list);
    }
}

public class SwitchFunction
{
    public string FunctinoName { get; set; }
    public List<string> Parameters { get; set; }

    public SwitchFunction(string function_name)
    {
        FunctinoName = function_name;
        Parameters = new List<string>();
    }
}
