﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class cKeyPad
{
    public delegate void KeyAction();
    public static KeyAction Key_MoveDown;
    public static KeyAction Key_MoveUp;
    public static KeyAction Key_MoveLeft;
    public static KeyAction Key_MoveRight;
    public static KeyAction Key_RunAction;
}
