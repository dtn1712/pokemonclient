﻿using UnityEngine;
using System.Collections;

public class cEventNPC
{

    public delegate void EventNPC(GameObject gameObj, string[] parameters);
    public static event EventNPC eShowMessage;
    public static event EventNPC eSwitchMessage;

    public static void Process(GameObject gameObj, EnumEventNPC code, string[] parameters)
    {
        switch (code)
        {
            case EnumEventNPC.Log:
                Log(gameObj, parameters);
                break;
            case EnumEventNPC.ChangeMap:
                ChangeMap(gameObj, parameters);
                break;
            case EnumEventNPC.ChangeMap_With_Condition:
                ChangeMap_With_Condition(gameObj, parameters);
                break;
            case EnumEventNPC.Message:
                Message(gameObj, parameters);
                break;
            case EnumEventNPC.SwitchMessage:
                SwitchMessage(gameObj, parameters);
                break;
            case EnumEventNPC.AutoMove:
                AutoMove(gameObj, parameters);
                break;
            case EnumEventNPC.Goto_House:
                Goto_House(gameObj, parameters);
                break;
            case EnumEventNPC.Goto_Position:
                Goto_Position(gameObj, parameters);
                break;
        }
    }

    public static void Log(GameObject gameObj, string[] parameters)
    {
        Debug.Log("cEventNPC Log: " + parameters[0]);
    }

    /// <summary>
    /// Gồm 3 tham số:
    /// 1: Tên Map/Scene
    /// 2: Vị trí X
    /// 3: Vị trí Y
    /// </summary>
    /// <param name="gameObj"></param>
    /// <param name="parameters"></param>
    public static void ChangeMap(GameObject gameObj, string[] parameters)
    {
        string MapName = parameters[0];
        int posX = int.Parse(parameters[1]);
        int posY = int.Parse(parameters[2]);
        enumDirection direction = Character.Player.Direction;
        if (parameters.Length > 3)
        {
            switch (parameters[3].ToLower())
            {
                case "left": direction = enumDirection.Left; break;
                case "right": direction = enumDirection.Right; break;
                case "up": direction = enumDirection.Up; break;
                case "down": direction = enumDirection.Down; break;
            }
        }
        cUserMap.Request_ChangeMap(MapName, posX, posY, direction);
    }

    /// <summary>
    /// Show một chat message.
    /// 
    /// </summary>
    /// <param name="gameObj">Đối tượng kích hoạt Event</param>
    /// <param name="parameters">
    /// Tham số nhận. Gồm:
    /// 1. Nội dung, 2. Canh lề [left, right], 3. Tiêu đề
    /// </param>
    public static void Message(GameObject gameObj, string[] parameters)
    {
        if (eShowMessage != null)
        {
            eShowMessage(gameObj, parameters);
        }
    }
    /// <summary>
    /// Một box lựa chọn. Gồm 11 tham số.
    /// 1: Lời thoại.
    /// 2: Canh lề [left,right]
    /// 3: Tên Npc
    /// 4..7: Tên các lựa chọn
    /// 8..11: Cấu trúc hành động gồm : tên action,param 1, param 2,...
    /// Ví dụ: change_map,Map01,2,3
    /// </summary>
    /// <param name="gameObj"></param>
    /// <param name="parameters"></param>
    public static void SwitchMessage(GameObject gameObj, string[] parameters)
    {
        if (eSwitchMessage != null)
        {
            eSwitchMessage(gameObj, parameters);
        }
    }

    public static void AutoMove(GameObject gameObj, string[] parameters)
    {
        if (parameters.Length > 0)
        {
            switch (parameters[0].ToLower())
            {
                case "left": Character.Player.Request_MoveLeft(); break;
                case "right": Character.Player.Request_MoveRight(); break;
                case "up": Character.Player.Request_MoveUp(); break;
                case "down":
                    {
                        Character.Player.Request_MoveDown();
                        break;
                    }
            }
        }
    }

    public static void ChangeMap_With_Condition(GameObject gameObj, string[] parameters)
    {
        string MapName = parameters[0];
        int posX = (int)gameObj.transform.position.x;
        int posY = (int)gameObj.transform.position.y;
        enumDirection direction = Character.Player.Direction;

        bool condition = direction.ToString().ToLower() == parameters[1];

        if (parameters.Length > 2)
        {
            switch (parameters[2].ToLower())
            {
                case "left": direction = enumDirection.Left; break;
                case "right": direction = enumDirection.Right; break;
                case "up": direction = enumDirection.Up; break;
                case "down": direction = enumDirection.Down; break;
            }
        }
        if (condition)
            cUserMap.Request_ChangeMap(MapName, posX, posY, direction);
    }

    public static void Goto_House(GameObject gameObj, string[] parameters)
    {
        string MapName = parameters[0];
        int posX = int.Parse(parameters[1]);
        int posY = int.Parse(parameters[2]);
        enumDirection direction = Character.Player.Direction;

        bool condition = direction.ToString().ToLower() == parameters[3];

        if (parameters.Length > 3)
        {
            switch (parameters[3].ToLower())
            {
                case "left": direction = enumDirection.Left; break;
                case "right": direction = enumDirection.Right; break;
                case "up": direction = enumDirection.Up; break;
                case "down": direction = enumDirection.Down; break;
            }
        }
        if (condition)
        {
            gameObj.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            cUserMap.Request_ChangeMap(MapName, posX, posY, direction);
        }
    }

    public static void Goto_Position(GameObject gameObj, string[] parameters)
    {
        int x = int.Parse(parameters[0]);
        int y = int.Parse(parameters[1]);
        cUserMap.Request_GoTo_Local_Map(x, y);
        if (parameters.Length > 2)
        {
            switch (parameters[2].ToLower())
            {
                case "left": Character.Player.Direction = enumDirection.Left; break;
                case "right": Character.Player.Direction = enumDirection.Right; break;
                case "up": Character.Player.Direction = enumDirection.Up; break;
                case "down": Character.Player.Direction = enumDirection.Down; break;
            }
        }
    }
}

public enum EnumEventNPC
{
    None,
    Log,
    ChangeMap,
    ChangeMap_With_Condition,
    Message,
    SwitchMessage,
    AutoMove,
    Goto_House,
    Goto_Position
}