﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

public class cLogin : MonoBehaviour {

    public GameObject FormLogin, FormRegister;
    public InputField Login_User, Login_Password, Register_User, Register_Password;
    public Text AlertText;
    public GameObject Network;

    public static bool LoginSuccesful = false;

    void Awake()
    {
        VirtualClient.OnLogin += OnLogin;
        VirtualClient.OnRegis += OnRegister;
        VirtualClient.OnChangeMap += OnChangeMap;
        Map.Init();
        VirtualServer.Init();
        LoginSuccesful = false;
        DontDestroyOnLoad(Network);
    }
    
    void OnDestroy()
    {
        VirtualClient.OnLogin -= OnLogin;
        VirtualClient.OnRegis -= OnRegister;
        VirtualClient.OnChangeMap -= OnChangeMap;
    }

    #region Event

    private void OnConnectToServer(Dictionary<byte, object> dic)
    {
        cUserLogin.Id = (int)dic[1];
        Debug.Log("Get Id: " + cUserLogin.Id);
    }

    private void OnLogin(Dictionary<byte, object> dic)
    {
        bool result = (bool) dic[0];
        if(result)
        {
            AlertText.text = "Login successful.";
            Database.LoadDatabase();
            StartCoroutine(IE_RequestBackToGame());
            LoginSuccesful = true;
        }
        else
        {
            AlertText.text = "Login failed.";
        }
    }

    private void OnRegister(Dictionary<byte, object> dic)
    {
        if ((bool)dic[0])
        {
            AlertText.text = "Account created.";
        }
    }

    private void OnChangeMap(Dictionary<byte, object> dic)
    {
        string name = (string) dic[0];
        int x = (int)dic[1];
        int y = (int)dic[2];
        cMap.PositionPlayer = new Vector2(x, y);
        Application.LoadLevel(name);
    }

    private void OnLoadInfoAccount(Dictionary<byte, object> dic)
    {
        //cUserNetwork.Player.User = (string)dic[0];
        //cUserNetwork.Player.MapName = (string)dic[1];
        //cUserNetwork.Player.PosX = (int)dic[2];
        //cUserNetwork.Player.PosY = (int)dic[3];
        //cUserNetwork.Player.Direction = (int)dic[4];
        //Application.LoadLevel(cUserNetwork.Player.MapName);
    }

    public void OnStatusNetwork()
    {
        
    }

    public void OnAlert(Dictionary<byte, object> dic)
    {
        AlertText.text = dic[0].ToString();
    }
    #endregion

    #region Button OnClick
    public void btn_Login()
    {
        cUserLogin.Login(Login_User.text, Login_Password.text);
    }

    public void btn_Register()
    {
        FormLogin.SetActive(false);
        FormRegister.SetActive(true);
    }

    public void btn_OKRegister()
    {
        string user = Register_User.text;
        string pass = Register_Password.text;
        if(user == "" || pass == "")
        {
            AlertText.text = "User Name or Password is not null. Try Again!";
        }
        else
        {
            cUserLogin.Register(user, pass);
        }
    }

    public void btn_BackRegister()
    {
        FormLogin.SetActive(true);
        FormRegister.SetActive(false);
    }
    #endregion

    #region IE
    IEnumerator IE_RequestBackToGame()
    {
        yield return new WaitForSeconds(1.5f);
        cUserLogin.GoToGame();
    }
    #endregion
}
