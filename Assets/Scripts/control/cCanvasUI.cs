﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class cCanvasUI : MonoBehaviour {

    public Image KeyPad_Movement;
    public GameObject NumPad_Movement;

    bool IsMouseDown;
    float Radius
    {
        get
        {
            return (Input.mousePosition - KeyPad_Movement.transform.position).magnitude;
        }
    }

    private void Awake()
    {
        IsMouseDown = false;
    }

    void Start () {
		
	}
	
	void Update () {
		if(!IsMouseDown && Input.GetMouseButtonDown(0))
        {
            IsMouseDown = true;
        }

        if (IsMouseDown && Input.GetMouseButtonUp(0))
        {
            IsMouseDown = false;
            NumPad_Movement.transform.position = KeyPad_Movement.transform.position;
        }

        if(IsMouseDown)
        {
            if (Radius <= KeyPad_Movement.rectTransform.rect.width / 2)
            {
                NumPad_Movement.transform.position = Input.mousePosition;
                KeyPadMovement_Action();
            }
            else
                NumPad_Movement.transform.position = KeyPad_Movement.transform.position;
        }
    }

    void KeyPadMovement_Action()
    {
        Vector3 vec = Input.mousePosition - KeyPad_Movement.transform.position;
        vec.Normalize();
        float Alpha = (180 / Mathf.PI) * Mathf.Acos(vec.x);
        if(vec.y > 0)
        {
            //Move Right
            if(Alpha < 45)
            {
                if (cKeyPad.Key_MoveRight != null)
                    cKeyPad.Key_MoveRight();
            }
            //Move Up
            else if(Alpha >= 45 && Alpha < 135)
            {
                if (cKeyPad.Key_MoveUp != null)
                {
                    cKeyPad.Key_MoveUp();
                }
            }
            //Move Left
            else if(Alpha >= 135)
            {
                if (cKeyPad.Key_MoveLeft != null)
                    cKeyPad.Key_MoveLeft();
            }
        }
        else
        {
            //Move Right
            if (Alpha < 45)
            {
                if (cKeyPad.Key_MoveRight != null)
                    cKeyPad.Key_MoveRight();
            }
            //Move Down
            else if (Alpha >= 45 && Alpha < 135)
            {
                if (cKeyPad.Key_MoveUp != null)
                {
                    cKeyPad.Key_MoveDown();
                }
            }
            //Move Left
            else if (Alpha >= 135)
            {
                if (cKeyPad.Key_MoveLeft != null)
                    cKeyPad.Key_MoveLeft();
            }
        }
    }
    
    public void OnClick_NumPadAction()
    {
        if (cKeyPad.Key_RunAction != null) cKeyPad.Key_RunAction();
    }

    public void OpenItemBox()
    {
        if (V_ItemBox.ActionOpenItemBox != null) V_ItemBox.ActionOpenItemBox(null);
    }

    public void OpenPokedex()
    {
        if (V_PokedexDialog.eShowDialog != null) V_PokedexDialog.eShowDialog(null);
    }
}
