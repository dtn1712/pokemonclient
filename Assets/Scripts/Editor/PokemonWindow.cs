﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PokemonWindow : EditorWindow
{
    [MenuItem("Database/Pokemons")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        PokemonWindow window = (PokemonWindow)EditorWindow.GetWindow(typeof(PokemonWindow));
        window.title = "Pokemon Data";
        window.Show();
        
    }

    D_Pokemon p;
    Sprite sprite;
    bool F_Default, F_Profile, F_Damages, F_Evolution, F_Natural;
    bool SecondType ;

    int type1, type2, size_damage, size_evo, size_natural;
    int Index;

    string ExpandSeedPokemon;

    Vector2 vecvalue, vecInfo;

    void OnEnable()
    {
        p = new D_Pokemon();

        p.Init();

        vecvalue = new Vector2();
        vecInfo = new Vector2();
        Index = -1;
        ExpandSeedPokemon = "+";
        if (Database.PokedexData == null)
            Database.LoadPokedexData();
    }

    public void SaveData()
    {
        string path = System.IO.Path.Combine(FileBinary.Resources, "Database/Pokedex.bytes");
        FileBinary.Write(path, Database.PokedexData);
    }

    void OnGUI()
    {
        if (Index >= 0 && Index < Database.PokedexData.Count)
        {
            p = Database.PokedexData[Index];
            sprite = p.SpriteAvatar;
        }
        EditorGUILayout.BeginHorizontal();

        #region Init & Edit
        vecInfo = EditorGUILayout.BeginScrollView(vecInfo);
            EditorGUILayout.BeginVertical();
        if (Index >= 0 && Index < Database.PokedexData.Count)
        {
            EditorGUILayout.LabelField("Info", EditorStyles.boldLabel);
            #region Info based
            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
            {
                EditorGUILayout.LabelField("Id", EditorStyles.label, GUILayout.Width(24));
                p.Id = EditorGUILayout.TextField(p.Id, EditorStyles.textField, GUILayout.MaxWidth(40));
                EditorGUILayout.LabelField("Name", EditorStyles.label, GUILayout.Width(48));
                p.Name = EditorGUILayout.TextField(p.Name, EditorStyles.textField, GUILayout.MaxWidth(160));
                EditorGUILayout.LabelField("Avatar", EditorStyles.label, GUILayout.Width(48));
                sprite = (Sprite)EditorGUILayout.ObjectField(sprite, typeof(Sprite), GUILayout.MaxWidth(160));
                if (sprite != null)
                {
                    p.Avatar = sprite.texture.name;
                }
            }
            EditorGUILayout.EndHorizontal();
            #endregion

            #region Info Default
            F_Default = EditorGUILayout.Foldout(F_Default, "Default Info");
            if (F_Default)
            {
                EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
                {
                    EditorGUILayout.LabelField("HP", EditorStyles.label, GUILayout.Width(24));
                    p.Default.HP = EditorGUILayout.IntField(p.Default.HP, EditorStyles.textField, GUILayout.MaxWidth(40));
                    EditorGUILayout.LabelField("Atk", EditorStyles.label, GUILayout.Width(24));
                    p.Default.Attack = EditorGUILayout.IntField(p.Default.Attack, EditorStyles.textField, GUILayout.MaxWidth(40));
                    EditorGUILayout.LabelField("Def", EditorStyles.label, GUILayout.Width(24));
                    p.Default.Defense = EditorGUILayout.IntField(p.Default.Defense, EditorStyles.textField, GUILayout.MaxWidth(40));
                    EditorGUILayout.LabelField("SAtk", EditorStyles.label, GUILayout.Width(32));
                    p.Default.SpATK = EditorGUILayout.IntField(p.Default.SpATK, EditorStyles.textField, GUILayout.MaxWidth(40));
                    EditorGUILayout.LabelField("SDef", EditorStyles.label, GUILayout.Width(32));
                    p.Default.SpDef = EditorGUILayout.IntField(p.Default.SpDef, EditorStyles.textField, GUILayout.MaxWidth(40));
                    EditorGUILayout.LabelField("Speed", EditorStyles.label, GUILayout.Width(48));
                    p.Default.Speed = EditorGUILayout.IntField(p.Default.Speed, EditorStyles.textField, GUILayout.MaxWidth(40));
                }
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
                {
                    EditorGUILayout.LabelField("Seed Pokemon", EditorStyles.label, GUILayout.Width(96));
                    if (ExpandSeedPokemon == "+")
                        p.Default.SeedPokemon = EditorGUILayout.TextField(p.Default.SeedPokemon, EditorStyles.textField);
                    else
                        p.Default.SeedPokemon = EditorGUILayout.TextArea(p.Default.SeedPokemon, EditorStyles.textField);
                    if (GUILayout.Button(ExpandSeedPokemon, GUILayout.Width(20)))
                    {
                        if (ExpandSeedPokemon == "+") ExpandSeedPokemon = "-";
                        else ExpandSeedPokemon = "+";
                    }

                    EditorGUILayout.LabelField("Type", EditorStyles.label, GUILayout.Width(40));
                    p.Default.Type[0] = (enumType)EditorGUILayout.Popup((int)p.Default.Type[0], Enum.GetNames(typeof(enumType)), GUILayout.Width(96));

                    if (p.Default.Type.Count == 1) SecondType = false; else SecondType = true;

                    if (GUILayout.Button(SecondType ? "-" : "+",GUILayout.Width(24)))
                    {
                        SecondType = !SecondType;
                        if (SecondType)
                            p.Default.Type.Add(enumType.Bug);
                        else
                            p.Default.Type.RemoveAt(1);
                    }
                    if (p.Default.Type.Count >= 2)
                        p.Default.Type[1] = (enumType)EditorGUILayout.Popup((int)p.Default.Type[1], Enum.GetNames(typeof(enumType)), GUILayout.Width(96));
                    
                }
                EditorGUILayout.EndHorizontal();
            }
            #endregion

            #region Profile Info
            F_Profile = EditorGUILayout.Foldout(F_Profile, "Profile Info");
            if (F_Profile)
            {
                EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
                {
                    EditorGUILayout.LabelField("Height", EditorStyles.label, GUILayout.Width(80));
                    p.Profile.Height = EditorGUILayout.TextField(p.Profile.Height, EditorStyles.textField, GUILayout.MaxWidth(96));
                    EditorGUILayout.LabelField("Weight", EditorStyles.label, GUILayout.Width(80));
                    p.Profile.Weight = EditorGUILayout.TextField(p.Profile.Weight, EditorStyles.textField, GUILayout.MaxWidth(96));
                }
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
                {
                    EditorGUILayout.LabelField("Catch Rate", EditorStyles.label, GUILayout.Width(80));
                    p.Profile.CatchRate = EditorGUILayout.TextField(p.Profile.CatchRate, EditorStyles.textField, GUILayout.MaxWidth(96));
                    EditorGUILayout.LabelField("Gender Ratio", EditorStyles.label, GUILayout.Width(80));
                    p.Profile.GenderRatio = EditorGUILayout.TextField(p.Profile.GenderRatio, EditorStyles.textField, GUILayout.MaxWidth(96));
                }
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
                {
                    EditorGUILayout.LabelField("Egg Groups", EditorStyles.label, GUILayout.Width(80));
                    p.Profile.EggGroups = EditorGUILayout.TextField(p.Profile.EggGroups, EditorStyles.textField, GUILayout.MaxWidth(96));
                    EditorGUILayout.LabelField("Hatch Steps", EditorStyles.label, GUILayout.Width(80));
                    p.Profile.HatchSteps = EditorGUILayout.TextField(p.Profile.HatchSteps, EditorStyles.textField, GUILayout.MaxWidth(96));
                }
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
                {
                    EditorGUILayout.LabelField("Abilities", EditorStyles.label, GUILayout.Width(80));
                    p.Profile.Abilities = EditorGUILayout.TextField(p.Profile.Abilities, EditorStyles.textField, GUILayout.MaxWidth(96));
                    EditorGUILayout.LabelField("Evs", EditorStyles.label, GUILayout.Width(80));
                    p.Profile.Evs = EditorGUILayout.TextField(p.Profile.Evs, EditorStyles.textField, GUILayout.MaxWidth(96));
                }
                EditorGUILayout.EndHorizontal();

            }
            #endregion

            #region Damages Info
            F_Damages = EditorGUILayout.Foldout(F_Damages, "Damages Info");
            if (F_Damages)
            {
                EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
                {
                    EditorGUILayout.LabelField("Size", EditorStyles.label, GUILayout.Width(80));
                    size_damage = EditorGUILayout.IntField(size_damage, EditorStyles.textField, GUILayout.MaxWidth(96));
                    EditorGUILayout.LabelField("", EditorStyles.label, GUILayout.Width(40));
                    if (GUILayout.Button("OK", EditorStyles.miniButton, GUILayout.MaxWidth(48)))
                    {
                        p.Damages = new List<D_DamageWhenAttacked>();
                        for (int i = 0; i < size_damage; i++)
                        {
                            p.Damages.Add(new D_DamageWhenAttacked());
                            p.Damages[i].Value = 2;
                        }
                    }
                }
                EditorGUILayout.EndHorizontal();
                if (p.Damages != null)
                {
                    for (int i = 0; i < p.Damages.Count; i++)
                    {
                        EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
                        {
                            D_DamageWhenAttacked damage = p.Damages[i];
                            EditorGUILayout.LabelField((i + 1) + ".", EditorStyles.label, GUILayout.Width(24));
                            damage.Class = (enumType)EditorGUILayout.Popup((int)damage.Class, Enum.GetNames(typeof(enumType)), GUILayout.Width(80));
                            damage.Value = EditorGUILayout.FloatField(damage.Value, EditorStyles.numberField, GUILayout.Width(48));
                            if (GUILayout.Button("2x", EditorStyles.miniButton, GUILayout.MaxWidth(48)))
                                damage.Value = 2;
                            if (GUILayout.Button("0.5x", EditorStyles.miniButton, GUILayout.MaxWidth(48)))
                                damage.Value = 0.5f;
                            if (GUILayout.Button("0.25x", EditorStyles.miniButton, GUILayout.MaxWidth(48)))
                                damage.Value = 0.25f;
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                }
            }
            #endregion

            #region Evolutions Info
            F_Evolution = EditorGUILayout.Foldout(F_Evolution, "Evolutions");
            if (F_Evolution)
            {
                EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
                {
                    EditorGUILayout.LabelField("Size", EditorStyles.label, GUILayout.Width(80));
                    size_evo = EditorGUILayout.IntField(size_evo, EditorStyles.textField, GUILayout.MaxWidth(96));
                    EditorGUILayout.LabelField("", EditorStyles.label, GUILayout.Width(40));
                    if (GUILayout.Button("OK", EditorStyles.miniButton, GUILayout.MaxWidth(48)))
                    {
                        p.Evolutions = new List<D_PokemonEvolutions>();
                        for (int i = 0; i < size_evo; i++)
                        {
                            p.Evolutions.Add(new D_PokemonEvolutions());
                        }
                    }
                }
                EditorGUILayout.EndHorizontal();
                if (p.Evolutions != null)
                {
                    for (int i = 0; i < p.Evolutions.Count; i++)
                    {
                        EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
                        {
                            D_PokemonEvolutions evo = p.Evolutions[i];
                            EditorGUILayout.LabelField((i + 1) + ".", EditorStyles.label, GUILayout.Width(24));
                            EditorGUILayout.LabelField("Id From: ", GUILayout.Width(54));
                            evo.IdFrom = EditorGUILayout.TextField(evo.IdFrom, EditorStyles.textField, GUILayout.Width(48));
                            EditorGUILayout.LabelField("To: ", GUILayout.Width(32));
                            evo.IdTo = EditorGUILayout.TextField(evo.IdTo, EditorStyles.textField, GUILayout.Width(48));
                            EditorGUILayout.LabelField("Details", GUILayout.Width(48));
                            evo.Details = EditorGUILayout.TextArea(evo.Details, EditorStyles.textArea);
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                }
            }
            #endregion

            #region Natural Move Info
            F_Natural = EditorGUILayout.Foldout(F_Natural, "Natural Moves");
            if (F_Natural)
            {
                EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
                {
                    EditorGUILayout.LabelField("Size", EditorStyles.label, GUILayout.Width(80));
                    size_natural = EditorGUILayout.IntField(size_natural, EditorStyles.textField, GUILayout.MaxWidth(96));
                    EditorGUILayout.LabelField("", EditorStyles.label, GUILayout.Width(40));
                    if (GUILayout.Button("OK", EditorStyles.miniButton, GUILayout.MaxWidth(48)))
                    {
                        p.NaturalMoves = new List<D_PokemonNaturalMove>();
                        for (int i = 0; i < size_natural; i++)
                        {
                            p.NaturalMoves.Add(new D_PokemonNaturalMove());
                        }
                    }
                }
                EditorGUILayout.EndHorizontal();
                if (p.NaturalMoves != null)
                {
                    EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
                    {
                        GUILayout.Space(30);
                        EditorGUILayout.LabelField("Lv: ", GUILayout.Width(24));
                        GUILayout.Space(24);
                        EditorGUILayout.LabelField("Skill Name: ", GUILayout.Width(64));
                    }
                    EditorGUILayout.EndHorizontal();

                    for (int i = 0; i < p.NaturalMoves.Count; i++)
                    {
                        EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
                        {
                            D_PokemonNaturalMove move = p.NaturalMoves[i];
                            EditorGUILayout.LabelField((i + 1) + ".", EditorStyles.label, GUILayout.Width(24));
                            //EditorGUILayout.LabelField("Lv: ", GUILayout.Width(24));
                            move.Lv = EditorGUILayout.IntField(move.Lv, EditorStyles.textField, GUILayout.Width(32));
                            //EditorGUILayout.LabelField("Skill Name: ", GUILayout.Width(64));
                            move.SkillName = EditorGUILayout.TextField(move.SkillName, EditorStyles.textField, GUILayout.Width(96));
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                }
            }
            #endregion
        }
            EditorGUILayout.EndVertical();
        EditorGUILayout.EndScrollView();
            #endregion
        #region Show List
        EditorGUILayout.BeginVertical("box",GUILayout.Width(300));
        {
            EditorGUILayout.LabelField("List Pokedex", EditorStyles.boldLabel);
            EditorGUILayout.BeginHorizontal("box");
            {
                if (GUILayout.Button("New Item"))
                {
                    D_Pokemon p = new D_Pokemon();
                    p.Init();
                    Database.PokedexData.Add(p);
                }
                if (GUILayout.Button("Load data"))
                {
                    Database.LoadPokedexData();
                }
                if (GUILayout.Button("Save"))
                {
                    SaveData();
                }
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal("box");
            {
                if (Database.PokedexData != null)
                {
                    vecvalue = EditorGUILayout.BeginScrollView(vecvalue);
                    for (int i = 0; i < Database.PokedexData.Count; i++)
                    {
                        EditorGUILayout.BeginVertical("box");
                        #region Row
                        EditorGUILayout.BeginHorizontal();
                        {
                            D_Pokemon pok = Database.PokedexData[i];
                            string txt = string.Format("{0}. {1}: {2}-{3}-{4}-{5}-{6}-{7}", pok.Id,
                                pok.Name,
                                pok.Default.HP,
                                pok.Default.Attack, pok.Default.Defense, pok.Default.SpATK,
                                pok.Default.SpDef, pok.Default.Speed);
                            if (GUILayout.Button(txt, EditorStyles.label))
                            {
                                Index = i;
                            }
                            if (GUILayout.Button("x", GUILayout.Width(24)))
                            {
                                if (Index == i) Index = -1;
                                Database.PokedexData.RemoveAt(i);
                            }
                        }
                        EditorGUILayout.EndHorizontal();
                        #endregion
                        EditorGUILayout.EndVertical();
                    }
                    EditorGUILayout.EndScrollView();
                }
            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();
        #endregion
        EditorGUILayout.EndHorizontal();
    }

}
