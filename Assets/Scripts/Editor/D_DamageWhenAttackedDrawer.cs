﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

//[CustomPropertyDrawer(typeof(D_DamageWhenAttacked))]
//public class D_DamageWhenAttackedDrawer : PropertyDrawer
//{

//    // Draw the property inside the given rect
//    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
//    {
//        // Using BeginProperty / EndProperty on the parent property means that
//        // prefab override logic works on the entire property.
//        EditorGUI.BeginProperty(position, label, property);

//        // Draw label
//        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

//        // Don't make child fields be indented
//        int indent = EditorGUI.indentLevel;
//        EditorGUI.indentLevel = 0;
//        // Calculate rects
//        Rect LvRect = new Rect(position.x - 20, position.y, 80, position.height);
//        Rect SkillRect = new Rect(position.x + 75, position.y, 50, position.height);

//        // Draw fields - passs GUIContent.none to each so they are drawn without labels
//        EditorGUI.PropertyField(LvRect, property.FindPropertyRelative("Class"), GUIContent.none);
//        EditorGUI.PropertyField(SkillRect, property.FindPropertyRelative("Value"), GUIContent.none);

//        // Set indent back to what it was
//        EditorGUI.indentLevel = indent;

//        EditorGUI.EndProperty();
//    }
//}
