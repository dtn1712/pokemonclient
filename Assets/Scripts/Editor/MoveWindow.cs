﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class MoveWindow : EditorWindow
{
    string myString = "Hello World";
    bool groupEnabled;
    bool myBool = true;
    float myFloat = 1.23f;

    Vector2 scrollValue;
    GUIStyle LabelName, LabelType, LabelProperties,LabelEffect;

    string TxtName, TxtEffect, TxtPower;
    int type, cat, power, acc, pp;

    Object sprite;

    List<string> Expands = new List<string>();

    [MenuItem("Database/Moves")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        MoveWindow window = (MoveWindow)EditorWindow.GetWindow(typeof(MoveWindow));
        window.Show();
    }

    public MoveWindow()
    {
        scrollValue = new Vector2();
        LabelName = new GUIStyle();
        LabelType = new GUIStyle();
        LabelEffect = new GUIStyle();
        LabelProperties = new GUIStyle();
        TxtName = "";
        TxtEffect = "";
    }

    void OnGUI()
    {
        //Menu
        #region Tool Menu
        GUILayout.Label("Menu", EditorStyles.boldLabel);
        GUILayout.BeginHorizontal("box");
        {
            GUILayout.Space(100);
            if (GUILayout.Button("Load Data"))
            {
                LoadData();
            }
            GUILayout.Space(100);
            if (GUILayout.Button("Save"))
            {
                SaveData();
            }
            GUILayout.Space(100);
        }
        GUILayout.EndHorizontal();
        #endregion

        #region Label
        GUILayout.BeginHorizontal("box");
        {
            //LabelName.fixedWidth = 256;
            LabelName.normal.textColor = Color.yellow;
            LabelName.padding.left = 40;
            GUILayout.Label("Name", LabelName, GUILayout.MaxWidth(256), GUILayout.Width(256));

            LabelProperties.fixedWidth = 64;
            LabelProperties.normal.textColor = Color.yellow;
            LabelProperties.padding.left = 20;
            GUILayout.Label("Type", LabelProperties, GUILayout.Width(72));

            GUILayout.Label("Cat", LabelProperties);
            GUILayout.Label("Power", LabelProperties);
            GUILayout.Label("Acc", LabelProperties);
            GUILayout.Label("PP", LabelProperties);
            LabelEffect.normal.textColor = Color.yellow;
            LabelEffect.padding.left = 40;
            GUILayout.Label("Effect", LabelEffect);
        }
        GUILayout.EndHorizontal();
        #endregion

        #region ScrollView Data
        //Show Data
        scrollValue = GUILayout.BeginScrollView(scrollValue);
        GUILayout.Label("Database Moves", EditorStyles.boldLabel);
        if(Database.MovesData != null)
        for (int i = 0; i < Database.MovesData.Count; i++)
        {
                SkillData move = Database.MovesData[i];
                GUILayout.BeginHorizontal("box");
                {
                    //LabelName.fixedWidth = 256;
                    LabelName.normal.textColor = Color.yellow;
                    LabelName.padding.left = 40;
                    if (GUILayout.Button(move.Name, LabelName, GUILayout.MaxWidth(256), GUILayout.Width(256)))
                        Show(move);

                    LabelProperties.fixedWidth = 64;
                    LabelProperties.normal.textColor = Color.yellow;
                    LabelProperties.padding.left = 20;
                    GUILayout.Label(move.Type.ToString(), LabelProperties, GUILayout.Width(72));

                    GUILayout.Label(move.Category.ToString(), LabelProperties);
                    GUILayout.Label(move.Power.ToString(), LabelProperties);
                    GUILayout.Label(move.Accuracy.ToString(), LabelProperties);
                    GUILayout.Label(move.PP.ToString(), LabelProperties);
                    LabelEffect.normal.textColor = Color.yellow;
                    LabelEffect.padding.left = 40;

                    if (Expands.Count < i + 1)
                        Expands.Add(" + ");
                    if (Expands[i] == " - ")
                        GUILayout.Label(move.Effect, EditorStyles.textArea);
                    else
                        GUILayout.Label(move.Effect, EditorStyles.label, GUILayout.Width(210));

                    if (GUILayout.Button(Expands[i], GUILayout.Width(32), GUILayout.MaxWidth(32)))
                    {
                        if (Expands[i] == " - ") Expands[i] = " + "; else Expands[i] = " - ";
                    }

                    if (GUILayout.Button(" x ", GUILayout.Width(32), GUILayout.MaxWidth(32)))
                    {
                        Expands.RemoveAt(i);
                        Database.MovesData.RemoveAt(i);
                    }
                }
                GUILayout.EndHorizontal();
            }
        GUILayout.EndScrollView();
        #endregion

        //Add Data
        GUILayout.Label("Input Moves", EditorStyles.boldLabel);

        #region Draw TextField
        GUILayout.BeginHorizontal("box");
        {
            LabelName.fixedWidth = 256;
            LabelName.normal.textColor = Color.yellow;
            TxtName = EditorGUILayout.TextField(TxtName,EditorStyles.textField);

            LabelType = new GUIStyle(EditorStyles.popup);
            LabelType.fixedWidth = 64;

            type = EditorGUILayout.Popup(type, System.Enum.GetNames(typeof(enumType)), LabelType,
                GUILayout.Width(64));

            cat = EditorGUILayout.Popup(cat, System.Enum.GetNames(typeof(enumCategorySkill)), LabelType,
                GUILayout.Width(64));
            power = EditorGUILayout.IntField(power, EditorStyles.numberField, GUILayout.Width(64));
            acc = EditorGUILayout.IntField(acc, EditorStyles.numberField, GUILayout.Width(64));
            pp = EditorGUILayout.IntField(pp, EditorStyles.numberField, GUILayout.Width(64));
            LabelEffect.normal.textColor = Color.yellow;
            TxtEffect = EditorGUILayout.TextArea(TxtEffect, EditorStyles.textArea, GUILayout.MaxWidth(250));
        }
        GUILayout.EndHorizontal();
        #endregion
        
        #region Tool Bottom
        GUILayout.BeginHorizontal("box");
        {
            GUILayout.Space(100);
            if (GUILayout.Button("Add Data"))
            {
                AddData();
            }
            GUILayout.Space(100);
            if (GUILayout.Button("Clear"))
            {
                Clear();
            }
            GUILayout.Space(100);
        }
        GUILayout.EndHorizontal();
        #endregion

        GUILayout.Space(20);
    }

    public void LoadData()
    {
        if (Database.MovesData == null)
        {
            Database.MovesData = (SkillDatabase)FileBinary.ReadTextAssets("Database/Skills");
            if (Database.MovesData == null)
            {
                Database.MovesData = new SkillDatabase();
            }
        }
        Database.Moves = new Dictionary<string, int>();
        for (int i = 0; i < Database.MovesData.Count; i++)
            Database.Moves[Database.MovesData[i].Name.ToLower()] = i;
    }

    void OnEnable()
    {
        if (Database.MovesData == null)
            LoadData();
    }

    void Show(SkillData move)
    {
        TxtName = move.Name;
        type = (int)move.Type;
        cat = (int)move.Category;
        power = move.Power;
        acc = move.Accuracy;
        pp = move.PP;
        TxtEffect = move.Effect;
    }

    public void SaveData()
    {
        string path = System.IO.Path.Combine(FileBinary.Resources, "Database/Skills.bytes");
        FileBinary.Write(path, Database.MovesData);
    }

    public void AddData()
    {
        if(TxtName.Length > 0 && (power + acc + pp) > 0)
        {
            if (Database.Moves.ContainsKey(TxtName.ToLower()))
            {
                int i = Database.Moves[TxtName.ToLower()];
                Database.MovesData[i].Name = TxtName;
                Database.MovesData[i].Type = (enumType)type;
                Database.MovesData[i].Category = (enumCategorySkill)cat;
                Database.MovesData[i].Power = power;
                Database.MovesData[i].Accuracy = acc;
                Database.MovesData[i].PP = pp;
                Database.MovesData[i].Effect = TxtEffect;
            }
            else
            {
                Database.MovesData.AddItem(new SkillData(TxtName, (enumType)type, (enumCategorySkill)cat, power, acc, pp, TxtEffect));
                Clear();
            }
        }
    }

    public void Clear()
    {
        TxtName = "";
        TxtEffect = "";
        power = acc = pp = 0;
    }
}
