﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ItemsWindow : EditorWindow
{
    [MenuItem("Database/Items")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        ItemsWindow window = (ItemsWindow)EditorWindow.GetWindow(typeof(ItemsWindow));
        window.title = "Items Data";
        window.Show();
    }

    Vector2 vec_value;
    D_Item item;
    Sprite sprite;
    Texture2D icon;
    int Index;

    string txtShowDetails, Message;

    GUIStyle ItemSelected;

    void OnEnable()
    {
        vec_value = new Vector2();
        txtShowDetails = "+";
        item = new D_Item();
        Database.LoadItemData();
        Index = -1;
        ItemSelected = new GUIStyle(EditorStyles.label);
        ItemSelected.normal.textColor = Color.yellow;
        Message = "";
    }

    public void SaveData()
    {
        string path = System.IO.Path.Combine(FileBinary.Resources, "Database/Items.bytes");
        FileBinary.Write(path, Database.ItemData);
    }

    void OnGUI()
    {
        #region Show List Data
        //Menu  bar
        EditorGUILayout.BeginHorizontal("box");
        {
            if(GUILayout.Button("New Item",GUILayout.Width(192)))
            {
                Database.ItemData.Add(new D_Item());
                Message = "Add new a item";
            }
            if (GUILayout.Button("Load", GUILayout.Width(192)))
            {
                Database.LoadItemData();
                Message = "Loading Items Data is complete.";
            }
            if (GUILayout.Button("Save", GUILayout.Width(192)))
            {
                SaveData();
                Message = "Saving Items infomation complete.";
            }
        }
        EditorGUILayout.EndHorizontal();

        //List Data
        GUILayout.Label("Items Data", EditorStyles.boldLabel);

        EditorGUILayout.BeginHorizontal("box");
        {
            GUILayout.Label("Icon", GUILayout.Width(96));
            GUILayout.Label("Name", GUILayout.Width(128));
            GUILayout.Label("Category", GUILayout.Width(96));
            GUILayout.Label("Effect");
        }
        EditorGUILayout.EndHorizontal();

        vec_value = EditorGUILayout.BeginScrollView(vec_value);

        if(Database.ItemData != null)
        for (int i = 0; i < Database.ItemData.Count; i++)
        {
            D_Item item = Database.ItemData[i];
            EditorGUILayout.BeginHorizontal("box");
            {
                //GUILayout.Box(new Rect(0, 0, 24, 24), item.Sprite.texture);
                if(item.Sprite != null)
                    GUILayout.Label(item.TextureIcon,GUILayout.Width(96));
                else
                    GUILayout.Label(item.Icon, GUILayout.Width(96));
                if (Index == i)
                {
                    EditorGUILayout.LabelField(item.Name, ItemSelected, GUILayout.Width(128));
                }
                else
                {
                    if (GUILayout.Button(item.Name, EditorStyles.label, GUILayout.Width(128)))
                    {
                        Index = i;
                        sprite = Database.ItemData[Index].Sprite;
                    }
                }
                GUILayout.Label(item.Category.ToString(), GUILayout.Width(96));
                GUILayout.Label(item.Effect);
                if(GUILayout.Button("x",GUILayout.Width(96)))
                {
                    Database.ItemData.RemoveAt(i);
                    i--;
                    Index = -1;
                }
            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndScrollView();
        #endregion

        if (Index >= 0 && Index < Database.ItemData.Count)
            item = Database.ItemData[Index];

        #region Init & Edit
        GUILayout.Label("Item Info", EditorStyles.boldLabel);
        EditorGUILayout.BeginHorizontal("box");
        {
            GUILayout.Label("Icon", GUILayout.Width(96));
            GUILayout.Label("Name", GUILayout.Width(128));
            GUILayout.Label("Category", GUILayout.Width(96));
            GUILayout.Label("Effect");
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal("box");
        {
            sprite = (Sprite)EditorGUILayout.ObjectField(sprite, typeof(Sprite), GUILayout.Width(96));
            if (sprite != null)
            {
                item.Icon = sprite.texture.name;
                item.Rect = sprite.rect;
            }
            item.Name =  EditorGUILayout.TextField(item.Name, EditorStyles.textField ,GUILayout.Width(128));
            item.Category = (enumCategoryItem) EditorGUILayout.Popup((int)item.Category, 
                            Enum.GetNames(typeof(enumCategoryItem)),GUILayout.Width(64));
            if (txtShowDetails == "+")
                item.Effect = EditorGUILayout.TextField(item.Effect, EditorStyles.textField, GUILayout.Width(256));
            else
                item.Effect = EditorGUILayout.TextArea(item.Effect, EditorStyles.textArea, GUILayout.Width(256));

            if (GUILayout.Button(txtShowDetails, GUILayout.Width(48)))
            {
                if (txtShowDetails == "+") txtShowDetails = "-";
                else
                    txtShowDetails = "+";
            }
        }
        EditorGUILayout.EndHorizontal();
        #endregion
    }
}
