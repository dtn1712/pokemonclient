﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class V_ChoosePokemonPanel : MonoBehaviour {

    public Image[] Pokemons;
    public GameObject Btn_Cancel, Btn_Ok, Btn_Choose;

    public bool IsStart;

    Sprite[] Avatars
    {
        get
        {
            return Resources.LoadAll<Sprite>("2D/Pokemon/pokemon");
        }
    }


    int Index;
    int Count;
    

    void Awake()
    {
        Index = 0;
        Count = 1;
        IsStart = true;
        Hide_all();
        cUserBattle.Load_ListPokemon += Load_ListPokemon;
    }

    void Hide_all()
    {
        for (int i = 0; i < Pokemons.Length; i++)
            Pokemons[i].gameObject.SetActive(false);
    }

    Sprite GetSprite(string name)
    {
        for (int i = 0; i < Avatars.Length; i++)
        {

            if (name.ToLower() == Avatars[i].name.ToLower())
                return Avatars[i];
        }
        Debug.Log("GetSprite : " + Avatars.Length);
        return null;
    }

    void OnEnable()
    {
        Hide_all();
        Index = 0;
        if(!IsStart)
        {
            Btn_Cancel.SetActive(true);
            Btn_Ok.SetActive(true);
            Btn_Choose.SetActive(false);
        }
        cUserBattle.Load_PokemonList();
    }

    void OnDestroy()
    {
        cUserBattle.Load_ListPokemon -= Load_ListPokemon;
    }

    void Load_ListPokemon(Dictionary<byte, object> dic)
    {
        Count = dic.Count;
        for(int i = 0; i < Count; i++)
        {
            Pokemons[i].sprite = GetSprite((string)dic[(byte)i]);
        }
        Pokemons[0].gameObject.SetActive(true);
    }

    public void btn_Next()
    {
        Hide_all();
        Index = (Index + 1) % Count;
        Pokemons[Index].gameObject.SetActive(true);
    }

    public void btn_Back()
    {
        Hide_all();
        Index = (Index - 1 + Count) % Count;
        Pokemons[Index].gameObject.SetActive(true);
    }

    public void btn_Ok()
    {
        cUserBattle.ChangePokemon(Index);
    }

    public void btn_Choose()
    {
        cUserBattle.ChoosePokemonFighter(Index);
    }
}
