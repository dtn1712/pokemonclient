﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class V_PokedexListButton : MonoBehaviour {

    public string Id;
    public Text TxtName;

    public void Select()
    {
        if (V_PokedexDialog.eSelectPokedex != null) V_PokedexDialog.eSelectPokedex(gameObject);
    }

    public D_Pokemon GetData
    {
        get
        {
            if (Database.Pokedex.ContainsKey(Id))
                return Database.PokedexData[Database.Pokedex[Id]];
            else
                return null;
        }
    }

    void OnEnable()
    {
        if(GetData != null)
            TxtName.text = GetData.Id + ". " + GetData.Name;
    }
}
