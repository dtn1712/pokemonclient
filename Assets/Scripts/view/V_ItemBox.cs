﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class V_ItemBox : MonoBehaviour {

    public delegate void ActionItemBox(object obj);
    public static ActionItemBox ActionOpenItemBox;
    public static ActionItemBox ActionShowItemsName;
    public static ActionItemBox ActionCurrentSelect;
    public GameObject Items;

    public GameObject ItemModel;

    public V_DetailsItemBox DetailsBox;

    public List<GameObject> childs;

    public void Awake()
    {
        ActionOpenItemBox += OpenBox;
        ActionShowItemsName += ShowItemsName;
        gameObject.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            btnClose();
    }

    void OnEnable()
    {
        Clear();
        AddItems("Antidote", 3);
        AddItems("Awakening", 5);
        AddItems("Berry Juice", 2);
        AddItems("Burn Heal", 6);
        AddItems("Calcium", 6);
        StartCoroutine(IE_ShowBox());
    }

    public void OnDestroy()
    {
        ActionOpenItemBox -= OpenBox;
        ActionShowItemsName -= ShowItemsName;
    }

    public void btnClose()
    {
        gameObject.SetActive(false);
        DetailsBox.gameObject.SetActive(false);
    }

    public void AddItem()
    {

    }

    public void OpenBox(object obj)
    {
        if (!gameObject.active)
            gameObject.SetActive(true);
        else
            btnClose();
    }

    public void ShowItemsName(object item)
    {
        V_Items Item = (V_Items)item;

        DetailsBox.Show(Item);
    }

    public void Clear()
    {
        for (int i = Items.transform.childCount - 1; i >= 0; i--)
            Destroy(Items.transform.GetChild(i).gameObject);
    }

    //AddItem với thông số
    public void AddItems(string name, string effect ,int Count, enumCategoryItem category, Sprite sprite = null)
    {
        GameObject g = (GameObject) Instantiate(ItemModel, Items.transform);
        g.transform.localScale = new Vector3(1, 1, 1);
        V_Items item = g.GetComponent<V_Items>();
        item.Name = name;
        item.Count = Count;
        item.Effect = effect;
        item.ItemType = category;
        if (sprite != null)
            item.Icon.sprite = sprite;
    }

    //Additem với database tĩnh
    public void AddItems(string name, int count)
    {
        GameObject g = (GameObject)Instantiate(ItemModel, Items.transform);
        g.transform.localScale = new Vector3(1, 1, 1);
        V_Items item = g.GetComponent<V_Items>();
        item.Name = name;
        item.Count = count;
        if(Database.Items.ContainsKey(name.ToLower()))
        {
            int id = Database.Items[name.ToLower()];
            item.Effect = Database.ItemData[id].Effect;
            if (Database.ItemData[id].Sprite != null)
                item.Icon.sprite = Database.ItemData[id].Sprite;
            item.ItemType = Database.ItemData[id].Category;
        }
    }

    IEnumerator IE_ShowBox()
    {
        Application.targetFrameRate = 100;
        foreach (GameObject g in childs)
            g.SetActive(false);
        float width = 1173, height = 784, toheight = 82;
        RectTransform rect = GetComponent<RectTransform>();

        int count = 20;
        float delta = (float)(height - toheight) / count;
        for (int i = 0; i < count; i++)
        {
            rect.sizeDelta = new Vector2(width, toheight + (i * delta));
            yield return new WaitForSeconds(0.01f);
        }
        Application.targetFrameRate = 30;
        rect.sizeDelta = new Vector2(width, height);
        foreach (GameObject g in childs)
            g.SetActive(true);
        yield return null;
    }
}
