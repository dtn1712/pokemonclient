﻿using UnityEngine;
using System.Collections;

public class V_PokeBall : MonoBehaviour {

    // Use this for initialization
    public GameObject Pokemon;
    public Transform To_Point;
    public bool KetQua;

    Vector3 Position_Pokemon;
	void Start () {
        Position_Pokemon = (Pokemon.transform.position);
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void Run()
    {
        gameObject.SetActive(true);
        StartCoroutine(IE_NemBong());
    }

    IEnumerator IE_NemBong()
    {
        Vector3 p_pokemon = Pokemon.transform.position +
            new Vector3(0, 160, 0);
        Vector3 unit = p_pokemon - transform.position;
        unit = unit / 15;
        float unit_scale = 0.05f;
        for(int i = 0;i < 15; i++)
        {
            transform.position += unit;
            transform.localScale = transform.localScale - new Vector3(unit_scale, unit_scale, unit_scale);
            transform.Rotate(0, 0, -30);
            yield return new WaitForSeconds(0.02f);
        }
        StartCoroutine(IE_BatPokemon());
    }

    IEnumerator IE_BatPokemon()
    {
        Vector3 unit = To_Point.position - transform.position;
        unit = unit / 15;
        for (int i = 0; i < 15; i++)
        {
            transform.position += unit;
            transform.Rotate(0, 0, 30);
            yield return new WaitForSeconds(0.02f);
        }
        StartCoroutine(IE_ThuPokemon());
    }

    IEnumerator IE_ThuPokemon()
    {
        Vector3 unit = transform.position - Pokemon.transform.position;
        unit = unit / 20;
        for (int i = 0; i < 20; i++)
        {
            Pokemon.transform.position += unit;
            Pokemon.transform.Rotate(0, 0, 30);
            Pokemon.transform.localScale -= new Vector3(0.05f, 0.05f, 0.05f);
            yield return new WaitForSeconds(0.02f);
        }
        StartCoroutine(IE_ThuanPhuc());
    }

    IEnumerator IE_ThuanPhuc()
    {
        float unit_scale = 30;
        for(int i = 0; i < 10; i++)
        {
            if(i % 2 == 0)
            {
                transform.Rotate(0, 0, unit_scale);
            }
            else
            {
                transform.Rotate(0, 0, -unit_scale);
            }
            yield return new WaitForSeconds(0.07f);
        }
        if(KetQua == false)
            StartCoroutine(IE_ThuPhuc_ThatBai());
        else
            StartCoroutine(IE_CompleteAction());
    }

    IEnumerator IE_ThuPhuc_ThatBai()
    {
        Vector3 unit = Position_Pokemon - Pokemon.transform.position;
        unit = unit / 20;
        Pokemon.transform.eulerAngles = new Vector3(0, 0, 0);
        for(int i = 0; i < 20; i++)
        {
            Pokemon.transform.localScale += new Vector3(0.05f, 0.05f, 0.05f);
            Pokemon.transform.position += unit;

            yield return new WaitForSeconds(0.02f);
        }
        StartCoroutine(IE_CompleteAction());
    }

    IEnumerator IE_CompleteAction()
    {
        yield return null;
        cUserBattle.CompleteAction("arrest");
        Object.Destroy(gameObject);
    }
}
