﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class V_Gizmos : MonoBehaviour {

    public float radius;
	void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
