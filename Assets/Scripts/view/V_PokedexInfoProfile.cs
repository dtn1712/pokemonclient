﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class V_PokedexInfoProfile : MonoBehaviour {

    public Text Height, CatchRate, EggGroups, Abilities, Weight, GenderRatio, HatchSteps, EVs;

    public void Load(string height, string catchRate, string eggGroups,
        string abilities, string weight, string genderRatio, string hatchSteps, string eVs)
    {
        Height.text = height;
        CatchRate.text = catchRate;
        EggGroups.text = eggGroups;
        Abilities.text = abilities;
        Weight.text = weight;
        GenderRatio.text = genderRatio;
        HatchSteps.text = hatchSteps;
        EVs.text = eVs;
    }

    public void Load(D_PokemonProfile profile)
    {
        Load(profile.Height, profile.CatchRate, profile.EggGroups, profile.Abilities, 
            profile.Weight, profile.GenderRatio, profile.HatchSteps, profile.Evs);
    }
}
