﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class V_PokedexNaturalMoveItem : MonoBehaviour {

    public Text Lv, SkillName;
    public V_ClassPokemon Class;
    public Text Power, Acc, PP, Details;

    public GameObject Row2, RowDetails;

    public Text TxtButton;

    public void Show()
    {
        if(Row2.active)
        {
            Row2.SetActive(false);
            RowDetails.SetActive(false);
            TxtButton.text = "+";
        }
        else
        {
            Row2.SetActive(true);
            RowDetails.SetActive(true);
            TxtButton.text = "-";
        }
    }

    public void Set(int lv, string skillname, enumType Clss, string power, string acc, string pp, string details)
    {
        Lv.text = lv.ToString();
        SkillName.text = skillname;
        Class.eClass = Clss;
        Class.UpdateValue();
        Power.text = power;
        Acc.text = acc;
        PP.text = pp;
        Details.text = details;
    }
}
