﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class V_PokedexInfoDamage : MonoBehaviour {

    public void Refesh()
    {
        for (int i = 0; i < transform.childCount; i++)
            transform.GetChild(i).gameObject.SetActive(false);
    }
	public void SetDamage(string name, float damage)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform tf = transform.GetChild(i);
            if(name.ToLower() == tf.gameObject.name.ToLower())
            {
                tf.gameObject.SetActive(true);
                tf.GetChild(1).GetComponent<Text>().text = damage + "x";
            }
        }
    }

    public void SetDamage(List<D_DamageWhenAttacked> list)
    {
        Refesh();
        foreach (D_DamageWhenAttacked d in list)
            SetDamage(d.Class.ToString(), d.Value);
    }
}
