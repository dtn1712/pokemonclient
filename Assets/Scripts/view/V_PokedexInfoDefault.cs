﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class V_PokedexInfoDefault : MonoBehaviour {

    public GameObject Class;
    public RectTransform HP, Attack, Defense, Speed, SpAtk, SpDef;
    public Text SeedPokemon;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnEnable()
    {
        for (int i = 0; i < Class.transform.childCount; i++)
            Class.transform.GetChild(i).gameObject.SetActive(false);
    }

    public void SetClass(string classname)
    {
        for (int i = 0; i < Class.transform.childCount; i++)
            if (classname.ToLower() == Class.transform.GetChild(i).gameObject.name.ToLower())
            {
                Class.transform.GetChild(i).gameObject.SetActive(true);
                break;
            }
    }

    public void Set(D_PokemonDefault value)
    {
        for(int i = 0; i < value.Type.Count && i < 2; i++)
        {
            SetClass(value.Type[i].ToString());
        }
        SeedPokemon.text = value.SeedPokemon;
        SetProperties(HP, value.HP);
        SetProperties(Attack, value.Attack);
        SetProperties(Defense, value.Defense);
        SetProperties(Speed, value.Speed);
        SetProperties(SpAtk, value.SpATK);
        SetProperties(SpDef, value.SpDef);
    }

    void SetProperties(RectTransform rect, int value)
    {
        rect.sizeDelta = new Vector2(value * 2, rect.sizeDelta.y);
        rect.GetChild(0).GetComponent<Text>().text = value.ToString();
    }
}
