﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class V_MessageEvent : MonoBehaviour {

    public GameObject TitleLeft, TitleRight;
    public Text Message, NameLeft, NameRight;

    void Awake()
    {
        cEventNPC.eShowMessage += eShowMessage;
        gameObject.SetActive(false);
    }

    private void eShowMessage(GameObject gameObj, string[] parameters)
    {
        ShowMessage(parameters[0], parameters[1], parameters[2]);
    }

    void OnDestroy()
    {
        Log.Write("V_MessageEvent Destroy");
        cEventNPC.eShowMessage -= eShowMessage;
    }

    public void ShowMessage(string message, string title, string name)
    {
        Message.text = message;
        TitleLeft.SetActive(false);
        TitleRight.SetActive(false);
        if (title.ToLower() == "left")
        {
            NameLeft.text = name;
            TitleLeft.SetActive(true);
        }
        else if(title.ToLower() == "right")
        {
            NameRight.text = name;
            TitleRight.SetActive(true);
        }
        gameObject.SetActive(true);
    }

    public void TouchMessage()
    {
        gameObject.SetActive(false);
    }
}
