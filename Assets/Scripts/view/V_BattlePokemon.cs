﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class V_BattlePokemon : MonoBehaviour {

    public Image Img;
    public Texture2D t2DFront, t2DBack;
    public Vector2 SizeFront;
    public int CountFront;
    public Vector2 SizeBack;
    public int CountBack;
    public bool IsFront;

    public Sprite[] SpFront, SpBack;
    int Index = 0;

    public V_Effect effect;

	void Awake()
    {
        ExtraSpriteBack();
        ExtraSpriteFront();
    }

    void Update()
    {
        if (Time.frameCount % 10 == 0)
        {
            if (IsFront)
                Img.sprite = SpFront[Index++ % CountFront];
            else
                Img.sprite = SpBack[Index++ % CountBack];
        }
    }

    void ExtraSpriteFront()
    {
        int Colum = (int)SizeFront.x;
        int Row = (int)SizeFront.y;
        int width = t2DFront.width / Colum;
        int height = t2DFront.height / Row;
        Img.rectTransform.sizeDelta = new Vector2(width * 5, height * 5);
        SpFront = new Sprite[CountFront];
        for(int i = 0; i < CountFront; i++)
        {
            int x = i % Colum;
            int y = Row - (i / Colum) - 1;
            SpFront[i] = Sprite.Create(t2DFront, new Rect(x * width, y * height, width, height), new Vector2(0.5f, 0.5f));
        }
    }

    void ExtraSpriteBack()
    {
        int Colum = (int)SizeBack.x;
        int Row = (int)SizeBack.y;
        int width = t2DBack.width / Colum;
        int height = t2DBack.height / Row;
        Img.rectTransform.sizeDelta = new Vector2(width * 5, height * 5);
        SpBack = new Sprite[CountBack];
        for (int i = 0; i < CountBack; i++)
        {
            int x = i % Colum;
            int y = Row - (i / Colum) - 1;
            SpBack[i] = Sprite.Create(t2DBack, new Rect(x * width, y * height, width, height), new Vector2(0.5f, 0.5f));
        }
    }

    public void ChangePivot(float x, float y)
    {
        RectTransform rect = Img.rectTransform;
        float deltaX = x - rect.pivot.x;
        float deltaY = y - rect.pivot.y;
        Vector3 pos = transform.localPosition;

        rect.pivot = new Vector2(x, y);
        pos += new Vector3(deltaX * rect.sizeDelta.x, deltaY * rect.sizeDelta.y);
        rect.localPosition = new Vector3(pos.x, pos.y, pos.z);
    }

    IEnumerator IE_ShowPokemon()
    {
        Application.targetFrameRate = 100;
        ChangePivot(0.5f, 0);
        Img.rectTransform.localScale = new Vector3(0, 0, 1);
        for(int i = 1; i <= 50; i++)
        {
            float scale = i*2 / 100f;
            Img.rectTransform.localScale = new Vector3(scale, scale, 1);
            yield return new WaitForSeconds(0.02f);
        }
        ChangePivot(0.5f, 0.5f);
        Application.targetFrameRate = 30;
    }

    IEnumerator IE_HidePokemon()
    {
        Application.targetFrameRate = 100;
        Img.rectTransform.localScale = new Vector3(0, 0, 1);
        for (int i = 50; i >= 0; i--)
        {
            float scale = i * 2 / 100f;
            Img.rectTransform.localScale = new Vector3(scale, scale, 1);
            yield return new WaitForSeconds(0.02f);
        }
        Application.targetFrameRate = 30;
    }

    public IEnumerator ExportPokemon()
    {
        gameObject.SetActive(true);
        effect.Point = transform.position;
        StartCoroutine(effect.Run());
        yield return StartCoroutine(IE_ShowPokemon());
    }

    public IEnumerator RetakePokemon()
    {
        gameObject.SetActive(true);
        effect.Point = transform.position;
        StartCoroutine(IE_HidePokemon());
        yield return StartCoroutine(effect.Run());
    }

    public IEnumerator RetakeNonePokemon()
    {
        gameObject.SetActive(true);
        for(int i = 100; i >= 0; i-=2)
        {
            Img.color = new Color(1, 1, 1, i / 100f);
            yield return new WaitForSeconds(0.01f);
        }
    }

}
