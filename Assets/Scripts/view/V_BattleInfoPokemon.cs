﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class V_BattleInfoPokemon : MonoBehaviour
{

    public Text TxtName, TxtHp, TxtMaxHp, TxtLV;
    public Text TxtShadowName, TxtShadowHp, TxtShadowMaxHp, TxtShadowLV;
    public Image ImgHp;

    public V_BattleBallIconInHpBar[] Balls;

    public string NAME
    {
        get { return TxtName.text; }
        set
        {
            TxtName.text = value;
            TxtShadowName.text = value;
        }
    }

    public int HP
    {
        get
        {
            try
            {
                return int.Parse(TxtHp.text);
            }
            catch
            {
                return 0;
            }
        }
        set
        {
            TxtHp.text = value.ToString();
            TxtShadowHp.text = value.ToString();
        }
    }

    public int MAXHP
    {
        get
        {
            try
            {
                return int.Parse(TxtMaxHp.text);
            }
            catch
            {
                return 0;
            }
        }
        set
        {
            TxtMaxHp.text = value.ToString();
            TxtShadowMaxHp.text = value.ToString();
        }
    }

    public int LV
    {
        get
        {
            try
            {
                return int.Parse(TxtLV.text);
            }
            catch
            {
                return 0;
            }
        }
        set
        {
            TxtLV.text = value.ToString();
            TxtShadowLV.text = value.ToString();
        }
    }

    void Awake()
    {
        StartCoroutine(IE_SetHp(1));
    }

    /// <summary>
    /// Max: 1, Min: 0
    /// </summary>
    /// <param name="hp"></param>
    /// <returns></returns>
    IEnumerator IE_SetHp(float hp)
    {
        //1% = 8.85
        if (hp < 0) hp = 0;
        if (hp > 1) hp = 1;
        Application.targetFrameRate = 100;
        float height = ImgHp.rectTransform.sizeDelta.y;
        float HpCurrent = ImgHp.rectTransform.sizeDelta.x / 885f;
        Debug.Log("HpCurrent: " + HpCurrent);
        float delta = (hp - HpCurrent);
        int count = Mathf.Abs(Mathf.RoundToInt(delta * 100));
        Debug.Log("count: " + count);
        int increase = delta >= 0 ? 1 : -1;
        for (int i = 0; i < count; i++)
        {
            ImgHp.rectTransform.sizeDelta += new Vector2(8.85f * increase,0);
            yield return new WaitForSeconds(0.01f);
        }
        ImgHp.rectTransform.sizeDelta = new Vector2(hp * 885f, height);
        Application.targetFrameRate = 30;
    }

    public void SetCountBalls(int count)
    {
        int i = 0;
        for ( ; i < count; i++)
            Balls[i].gameObject.SetActive(true);
        for(; i < 6; i++)
            Balls[i].gameObject.SetActive(false);
    }

    public void SetCountBallsActive(int count)
    {
        int i = 0;
        for (; i < count; i++)
            Balls[i].Active();
        for (; i < 6; i++)
            Balls[i].NoneActive();
    }
    
}