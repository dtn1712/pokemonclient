﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class V_Items : MonoBehaviour {

    public string Name;
    public Image Icon;
    public Text TxtCount;
    public int Count
    {
        get
        {
            return int.Parse(TxtCount.text);
        }
        set
        {
            TxtCount.text = value.ToString();
        }
    }

    public UseItems UseType;
    public enumCategoryItem ItemType;

    public string Effect;

    void Awake()
    {
        V_ItemBox.ActionCurrentSelect += CurrentSelect;
    }

    void OnDestroy()
    {
        V_ItemBox.ActionCurrentSelect -= CurrentSelect;
    }
	// Use this for initialization
	void Start () {
		
	}
	
    public void Select()
    {
        if (V_ItemBox.ActionShowItemsName != null) V_ItemBox.ActionShowItemsName(this);
        if (V_ItemBox.ActionCurrentSelect != null) V_ItemBox.ActionCurrentSelect(this);
    }

    void CurrentSelect(object item)
    {
        if(this == (V_Items)item)
        {
            GetComponent<Image>().color = new Color(189f / 255, 75f / 255, 75f/255, 1);
        }
        else
        {
            GetComponent<Image>().color = new Color(156f/255,206f/255,1,1);
        }
    }
}

public enum UseItems
{
    None,
    Default,
    ForOne,
    ForAll,
}