﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class V_Effect : MonoBehaviour {

    public Vector3 Point;
    public bool AutoRun,GetPosCurrent, RunReverse, Loop;

    void Awake()
    {
        if (GetPosCurrent) Point = transform.position;
        if (AutoRun) StartCoroutine(Run());

    }

    public IEnumerator Run()
    {
        do
        {
            if (RunReverse)
            {
                for (int i = transform.childCount - 1; i >= 0; i--)
                {
                    if (i < transform.childCount - 1) transform.GetChild(i + 1).gameObject.SetActive(false);
                    //if (Point != null) transform.GetChild(i).position = new Vector3(Point.x, Point.y, Point.z);
                    transform.GetChild(i).gameObject.SetActive(true);
                    yield return new WaitForSeconds(0.05f);
                }
                transform.GetChild(0).gameObject.SetActive(false);
            }
            else
            {
                for (int i = 0; i < transform.childCount; i++)
                {
                    if (i > 0) transform.GetChild(i - 1).gameObject.SetActive(false);
                    //if (Point != null) transform.GetChild(i).position = new Vector3(Point.x, Point.y, Point.z);
                    transform.GetChild(i).gameObject.SetActive(true);
                    yield return new WaitForSeconds(0.05f);
                }
                transform.GetChild(transform.childCount - 1).gameObject.SetActive(false);
            }
            yield return null;
        }
        while (Loop);
        Destroy(gameObject);
    }
}
