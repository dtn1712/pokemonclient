﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class V_PokedexInfoEvolutions : MonoBehaviour {
    public GameObject Item;
    public GameObject Details;

    void Awake()
    {
        Refesh();
    }

    public void OnDisable()
    {
        Refesh();
    }

    public void Refesh()
    {
        for (int i = transform.childCount - 1; i >= 1; i--)
            Destroy(transform.GetChild(i).gameObject);
    }

    public void Load(string from, string to, string details)
    {
        GameObject item = Instantiate(Item, transform);
        Vector3 scale = transform.localScale;
        item.transform.localScale = new Vector3(scale.x, scale.y, scale.z);
        Image IMGfrom = item.transform.GetChild(0).GetComponent<Image>();
        Image IMGto = item.transform.GetChild(2).GetComponent<Image>();
        Text dt = Instantiate(Details, transform).GetComponent<Text>();
        dt.transform.localScale = new Vector3(scale.x, scale.y, scale.z);
        dt.text = details;
        Texture2D t2dF = Resources.Load<Texture2D>("2D/pokemon/" + from);
        Texture2D t2dT = Resources.Load<Texture2D>("2D/pokemon/" + to);
        IMGfrom.sprite = Sprite.Create(t2dF, new Rect(0, 0, t2dF.width, t2dT.height), new Vector2(0.5f, 0.5f));
        IMGto.sprite = Sprite.Create(t2dT, new Rect(0, 0, t2dT.width, t2dT.height), new Vector2(0.5f, 0.5f));
    }

    public void Load(List<D_PokemonEvolutions> evolutions)
    {
        Refesh();
        foreach(D_PokemonEvolutions evo in evolutions)
        {
            Load(evo.IdFrom, evo.IdTo, evo.Details);
        }
    }
}
