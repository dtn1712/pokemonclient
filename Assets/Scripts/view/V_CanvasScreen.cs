﻿using UnityEngine;
using System.Collections;

public class V_CanvasScreen : MonoBehaviour {

    public GameObject[] MessageBoxs;
	
    public bool IsShowMessage
    {
        get
        {
            for (int i = 0; i < MessageBoxs.Length; i++)
                if (MessageBoxs[i].active)
                    return true;
            return false;
        }
    }

    void Awake()
    {
        for (int i = 0; i < transform.GetChildCount(); i++)
            transform.GetChild(i).gameObject.SetActive(true);
    }

    void Update()
    {
        Character.IsChatMessage = IsShowMessage;
    }
}
