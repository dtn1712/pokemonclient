﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class V_PokedexNaturalMoves : MonoBehaviour {

    public GameObject Item;
    
    public void OnEnable()
    {
    }
	
    public void OnDisable()
    {
        Clear();
    }

    public void Clear()
    {
        for (int i = transform.childCount - 1; i >= 1; i--)
            Destroy(transform.GetChild(i).gameObject);
    }

    public void Add(int lv, string skillname, enumType Clss, string power, string acc, string pp, string details)
    {
        GameObject g = Instantiate(Item, transform);
        Vector3 scale = transform.localScale;
        g.transform.localScale = new Vector3(scale.x, scale.y, scale.z);
        V_PokedexNaturalMoveItem item = g.GetComponent<V_PokedexNaturalMoveItem>();
        item.Row2.transform.SetParent(transform);
        item.Details.transform.SetParent(transform);
        item.Set(lv,skillname,Clss,power,acc,pp,details);
    }

    public void Set(List<D_PokemonNaturalMove> list)
    {
        Clear();
        foreach (D_PokemonNaturalMove n in list)
        {
            if (Database.Moves.ContainsKey(n.SkillName.ToLower()))
            {
                SkillData m = Database.MovesData[Database.Moves[n.SkillName.ToLower()]];
                Add(n.Lv, n.SkillName, m.Type, m.Power.ToString(), m.Accuracy.ToString(), m.PP.ToString(), m.Effect);
            }
        }
    }
}
