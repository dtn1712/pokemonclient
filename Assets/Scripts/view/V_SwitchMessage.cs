﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class V_SwitchMessage : MonoBehaviour {

    public GameObject TitleLeft, TitleRight;
    public Text Message, NameLeft, NameRight;
    public Text[] TextSwitchs;


    string[] Datas;
    GameObject Sender;

    void Awake()
    {
        cEventNPC.eSwitchMessage += eSwitchMessage;
        gameObject.SetActive(false);
        Datas = new string[4];
    }

    private void eSwitchMessage(GameObject gameObj, string[] parameters)
    {
        Sender = gameObj;
        ShowMessage(parameters);
    }

    void OnDestroy()
    {
        cEventNPC.eSwitchMessage -= eSwitchMessage;
    }

    public void ShowMessage(string[] parameters)
    {
        string message = parameters[0];
        string title = parameters[1];
        string name = parameters[2];

        Message.text = message;
        TitleLeft.SetActive(false);
        TitleRight.SetActive(false);
        if (title.ToLower() == "left")
        {
            NameLeft.text = name;
            TitleLeft.SetActive(true);
        }
        else if (title.ToLower() == "right")
        {
            NameRight.text = name;
            TitleRight.SetActive(true);
        }

        HideAllSwitchText();
        for(int i = 3; i < 7; i++)
        {
            if (parameters[i].Length > 0)
                ShowSwitchText(i - 3, parameters[i]);
        }
        for (int i = 7; i < 11; i++)
        {
            Datas[i - 7] = parameters[i];
        }
        gameObject.SetActive(true);
    }

    public void HideAllSwitchText()
    {
        for(int i = 0; i < TextSwitchs.Length; i++)
        {
            TextSwitchs[i].transform.parent.gameObject.SetActive(false);
        }
    }

    public void ShowSwitchText(int id, string text)
    {
        TextSwitchs[id].transform.parent.gameObject.SetActive(true);
        TextSwitchs[id].text = text;
    }

    public void ChooseSwitch(int id)
    {
        cSwitchFunction.Function(Sender, ToParams(Datas[id]));
        gameObject.SetActive(false);
    }

    public SwitchFunction ToParams(string data)
    {
        SwitchFunction sfObject;
        int IndexOf = -1;
        string func_name = "";
        //Get Function Name
        IndexOf = data.IndexOf(',');
        if (IndexOf >= 0)
        {
            func_name = data.Substring(0, IndexOf);
            data = data.Remove(0, IndexOf + 1);
        }
        else
        {
            func_name = data;
        }
        sfObject = new SwitchFunction(func_name);

        //Get Parameters
        do
        {
            IndexOf = data.IndexOf(',');
            if (IndexOf >= 0)
            {
                string s = data.Substring(0, IndexOf);
                data = data.Remove(0, IndexOf + 1);
                sfObject.Parameters.Add(s);
            }
            else
            {
                if (data.Length > 0)
                    sfObject.Parameters.Add(data);
            }
        } while (IndexOf >= 0);

        return sfObject;
    }
}
