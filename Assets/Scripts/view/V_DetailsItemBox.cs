﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class V_DetailsItemBox : MonoBehaviour {

    public Text NameItem, ShadowName, Effect, Category;
    public V_Items Item;


    void Show(string name, string effect, enumCategoryItem category)
    {
        NameItem.text = name;
        ShadowName.text = name;
        Effect.text = effect;
        Category.text = category.ToString();
        if (!gameObject.active)
            gameObject.SetActive(true);
        StartCoroutine(IE_ShowBox());
    }

    public void Show(V_Items item)
    {
        Show(item.Name, item.Effect, item.ItemType);
        Item = item;
    }

    public void BtnRemove()
    {
        Destroy(Item.gameObject);
    }

    public void BtnUse()
    {
        Item.Count--;
        if (Item.Count == 0)
            BtnRemove();
        //Show Effect at here...
    }

    IEnumerator IE_ShowBox()
    {
        Application.targetFrameRate = 100;
        Vector3 vec = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z);

        transform.localPosition = new Vector3(1786, vec.y);
        int count = 20;
        float delta = (float)(1786f - 1186f) / count;
        Debug.Log("Delta " + delta);
        for(int i = 0; i < count; i++)
        {
            transform.localPosition = new Vector3(1786 - (i * delta), vec.y);
            yield return new WaitForSeconds(0.01f);
        }
        Application.targetFrameRate = 30;
        transform.localPosition = new Vector3(1186, vec.y);
        yield return null;
    }
}
