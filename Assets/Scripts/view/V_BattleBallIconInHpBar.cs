﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class V_BattleBallIconInHpBar : MonoBehaviour {

    public Sprite SpActive, SpNoneActive;

    public void Active()
    {
        GetComponent<Image>().sprite = SpActive;
    }

    public void NoneActive()
    {
        GetComponent<Image>().sprite = SpNoneActive;
    }
}
