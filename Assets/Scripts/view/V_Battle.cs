﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class V_Battle : MonoBehaviour {
    public Image Ball;
    public RectTransform FrameBall, FrameBall2;
    public Transform PointPlayer, PointEnemy, PointAtEnemy;
    public V_BattlePokemon PokemonPlayer, PokemonEnemy;
    void Awake()
    {
        //ExportPokemonEnemy("001");
    }

    #region Throw Pokeball
    public IEnumerator IE_ThrowPokeballOfPlayer()
    {
        float x = 0, y = 0;
        Vector3 p = PointPlayer.localPosition;
        float a = -1, b = 4, c = 4, rate = 100;
        Ball.gameObject.SetActive(true);
        Ball.rectTransform.localScale = new Vector3(3,3,3);
        do
        {

            Ball.rectTransform.localPosition = new Vector3(x* rate + p.x, y* rate + p.y, 0);
            Ball.rectTransform.Rotate(0, 0, -18);
            x += 0.1f;
            float newY = (a * Mathf.Pow(x, 2) + b * x + c);
            float delta = Mathf.Abs(y - newY) / 6.5f;
            y = newY;
            Ball.rectTransform.localScale -= new Vector3(delta, delta, delta);
            yield return new WaitForSeconds(0.02f);
        } while (y >= 2);
        PokemonPlayer.transform.position = Ball.transform.position;
        PokemonPlayer.gameObject.SetActive(false);
        Ball.gameObject.SetActive(false);
        yield return null;
    }

    public IEnumerator IE_ThrowPokeballOfEnemy()
    {
        float x = 0, y = 0;
        Vector3 p = PointEnemy.localPosition;
        float a = -1, b = 4, c = 4, rate = 100;
        Ball.gameObject.SetActive(true);
        Ball.rectTransform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        do
        {

            Ball.rectTransform.localPosition = new Vector3(-x * rate + p.x, y * rate + p.y, 0);
            Ball.rectTransform.Rotate(0, 0, -18);
            x += 0.1f;
            float newY = (a * Mathf.Pow(x, 2) + b * x + c);
            float delta = Mathf.Abs(y - newY) / 20f;
            y = newY;
            Ball.rectTransform.localScale += new Vector3(delta, delta, delta);
            yield return new WaitForSeconds(0.02f);
        } while (y >= 2);
        PokemonEnemy.transform.position = Ball.transform.position;
        PokemonEnemy.gameObject.SetActive(false);
        Ball.gameObject.SetActive(false);
        yield return null;
    }
    #endregion

    #region Export, Retake pokemon player
    //============
    public IEnumerator IE_ExportPokemonPlayer()
    {
        yield return StartCoroutine(IE_ThrowPokeballOfPlayer());
        PokemonPlayer.effect.transform.position = Ball.transform.position;
        yield return StartCoroutine(PokemonPlayer.ExportPokemon());
        yield return null;
    }

    public IEnumerator IE_RetakePokemonPlayer()
    {
        yield return StartCoroutine(PokemonPlayer.RetakePokemon());
        Destroy(PokemonPlayer.gameObject);
        yield return null;
    }
    #endregion
    //############
    #region Export, Retake pokemon Enemy
    public IEnumerator IE_ExportPokemonEnemy()
    {
        yield return StartCoroutine(IE_ThrowPokeballOfEnemy());
        PokemonEnemy.effect.transform.position = Ball.transform.position;
        yield return StartCoroutine(PokemonEnemy.ExportPokemon());
        yield return null;
    }
    public IEnumerator IE_ExportNonePokemonEnemy()
    {
        PokemonEnemy.ChangePivot(0.5f, 0);
        PokemonEnemy.Img.color = new Color(1, 1, 1, 0);
        PokemonEnemy.transform.position = PointAtEnemy.position;
        yield return new WaitForSeconds(0.1f);
        for (int i = 0; i <= 100; i+= 3)
        {
            PokemonEnemy.Img.color = new Color(1, 1, 1, i / 100f);
            yield return new WaitForSeconds(0.01f);
        }
        yield return null;
    }
    public IEnumerator IE_RetakePokemonEnemy(string type)
    {
        if(type != "none")
            yield return StartCoroutine(PokemonEnemy.RetakePokemon());
        else
            yield return StartCoroutine(PokemonEnemy.RetakeNonePokemon());
        Destroy(PokemonEnemy.gameObject);
        yield return null;
    }
    #endregion

    /// <summary>
    /// Animation Bắt pokemon
    /// </summary>
    /// <param name="result">Kết quả bắt pokemon</param>
    /// <returns></returns>
    public IEnumerator IE_Arrested(bool result = false)
    {
        float x, y, scale;
        x = 1;
        scale = 2.5f;
        Ball.gameObject.SetActive(true);
        Ball.transform.parent = FrameBall;
        do
        {
            y = -Mathf.Pow(x, 1.5f) + Mathf.Pow(x, 0.5f);
            Ball.transform.localPosition = new Vector3(x * FrameBall.sizeDelta.x, y * 1000);
            Ball.transform.localScale = new Vector3(scale, scale, scale);
            Ball.transform.Rotate(0, 0, 18);
            scale -= 0.02f * 1.75f;
            x -= 0.02f;
            yield return new WaitForSeconds(0.01f);
        } while (x >= 0);
        x = 1;
        Ball.transform.parent = FrameBall2;
        do
        {
            y = -Mathf.Pow(x, 2) + 1;
            Ball.transform.localPosition = new Vector3(x * FrameBall2.sizeDelta.x, y * FrameBall2.sizeDelta.y);
            x -= 0.1f;
            yield return new WaitForSeconds(0.01f);
        } while (x >= -1);
        
        V_Effect effect = Instantiate(Resources.Load<GameObject>("prefabs/effect/Arrested Pokemon"), transform).GetComponent<V_Effect>();
        effect.transform.position = Ball.transform.position;
        StartCoroutine(effect.Run());
        yield return new WaitForSeconds(0.05f * 15);
        PokemonEnemy.ChangePivot(0.5f, 0.5f);
        Vector3 delta = (PokemonEnemy.transform.position - Ball.transform.position) / 15;
        for (int i = 0; i < 15; i++)
        {
            PokemonEnemy.transform.position -= delta;
            PokemonEnemy.transform.localScale -= new Vector3(1/15f, 1 / 15f, 1 / 15f);
            yield return new WaitForSeconds(0.05f);
        }

        for (int i = 0; i < 5; i++)
        {
            Ball.transform.Rotate(0, 0, 18);
            yield return new WaitForSeconds(0.05f);
            Ball.transform.Rotate(0, 0, -18);
            yield return new WaitForSeconds(0.5f);
        }
        if(result)
        {

        }
        else
        {
            yield return new WaitForSeconds(2f);
            effect = Instantiate(Resources.Load<GameObject>("prefabs/effect/Arrested Pokemon"), transform).GetComponent<V_Effect>();
            effect.transform.position = Ball.transform.position;
            effect.RunReverse = true;
            StartCoroutine(effect.Run());
            yield return new WaitForSeconds(0.05f * 15);
            for (int i = 0; i < 15; i++)
            {
                PokemonEnemy.transform.position += delta;
                PokemonEnemy.transform.localScale += new Vector3(1 / 15f, 1 / 15f, 1 / 15f);
                yield return new WaitForSeconds(0.05f);
            }
            PokemonEnemy.ChangePivot(0.5f, 0f);
            Ball.gameObject.SetActive(false);
        }
        Ball.transform.parent = transform;
        
        yield return null;
    }
    
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            //if (PokemonEnemy == null)
            //    ExportPokemonEnemy("001","player");
            //else
            //    RetakePokemonEnemy("player");
            StartCoroutine(IE_Arrested());
        }
    }

    //-------------------------------------------
    public void ExportPokemonPlayer(string name)
    {
        GameObject gpPlayer = Instantiate(Resources.Load<GameObject>("prefabs/battle/" + name),transform);
        gpPlayer.transform.localScale = new Vector3(1, 1, 1);            
        GameObject geffect = Instantiate(Resources.Load<GameObject>("prefabs/effect/Export Pokemon"),transform);
        geffect.transform.localScale = new Vector3(1, 1, 1);
        PokemonPlayer = gpPlayer.GetComponent<V_BattlePokemon>();
        PokemonPlayer.IsFront = false;
        PokemonPlayer.effect = geffect.GetComponent<V_Effect>();
        StartCoroutine(IE_ExportPokemonPlayer());
    }

    public void RetakePokemonPlayer()
    {
        GameObject geffect = Instantiate(Resources.Load<GameObject>("prefabs/effect/Retake Pokemon"), transform);
        geffect.transform.localScale = new Vector3(1, 1, 1);
        geffect.transform.position = PokemonPlayer.transform.position;
        PokemonPlayer.effect = geffect.GetComponent<V_Effect>();
        StartCoroutine(IE_RetakePokemonPlayer());
    }

    public void ExportPokemonEnemy(string name, string type = "none")
    {
        GameObject gpEnemy = Instantiate(Resources.Load<GameObject>("prefabs/battle/" + name), transform);
        gpEnemy.transform.localScale = new Vector3(1, 1, 1);
        PokemonEnemy = gpEnemy.GetComponent<V_BattlePokemon>();
        PokemonEnemy.IsFront = true;
        if (type != "none")
        {
            GameObject geffect = Instantiate(Resources.Load<GameObject>("prefabs/effect/Export Pokemon"), transform);
            geffect.transform.localScale = new Vector3(1, 1, 1);
            PokemonEnemy.effect = geffect.GetComponent<V_Effect>();
            StartCoroutine(IE_ExportPokemonEnemy());
        }
        else
        {
            StartCoroutine(IE_ExportNonePokemonEnemy());
        }
    }

    public void ExportPokemonEnemy_01(string name)
    {
        ExportPokemonEnemy(name, "none");
    }

    public void ExportPokemonEnemy_02(string name)
    {
        ExportPokemonEnemy(name, "Notnone");
    }

    public void RetakePokemonEnemy(string type = "none")
    {
        GameObject geffect = Instantiate(Resources.Load<GameObject>("prefabs/effect/Retake Pokemon 2"), transform);
        geffect.transform.localScale = new Vector3(1, 1, 1);
        PokemonEnemy.effect = geffect.GetComponent<V_Effect>();
        PokemonEnemy.effect.transform.position = PokemonEnemy.transform.position;
        StartCoroutine(IE_RetakePokemonEnemy(type));
    }
    
    public void ArrestedPokemon(bool result)
    {
        StartCoroutine(IE_Arrested(result));
    }
}
