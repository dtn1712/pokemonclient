﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class V_ClassPokemon : MonoBehaviour {

    public enumType eClass;
    //public Sprite Normal, Grass, Ghost, Bug, Psychic, Ground, Fighting, Poison,
    //              Fire, Water, Dark, Ice, Steel, Flying, Dragon, Rock, Electric;
    public Text Text
    { get { return transform.GetChild(0).GetComponent<Text>(); } }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        UpdateValue();

    }
    
    public void UpdateValue()
    {
        if(eClass.ToString() != Text.text)
        {
            Text.text = eClass.ToString();
            Image img = GetComponent<Image>();
            switch(eClass)
            {
                #region
                case enumType.Bug:
                    img.color = new Color(111f / 255, 159f / 255, 0f / 255); break;
                case enumType.Dark:
                    img.color = new Color(69f / 255, 69f / 255, 69f / 255); break;
                case enumType.Electric:
                    img.color = new Color(246f / 255, 201f / 255, 19f / 255); break;
                case enumType.Fairy:
                    img.color = new Color(232f / 255, 120f / 255, 140f / 255); break;
                case enumType.Fighting:
                    img.color = new Color(152f / 255, 0f / 255, 0f / 255); break;
                case enumType.Fire:
                    img.color = new Color(255f / 255, 111f / 255, 0f / 255); break;
                case enumType.Flying:
                    img.color = new Color(142f / 255, 111f / 255, 235f / 255); break;
                case enumType.Ghost:
                    img.color = new Color(100f / 255, 78f / 255, 136f / 255); break;
                case enumType.Grass:
                    img.color = new Color(0f / 255, 199f / 255, 59f / 255); break;
                case enumType.Ground:
                    img.color = new Color(219f / 255, 181f / 255, 77f / 255); break;
                case enumType.Ice:
                    img.color = new Color(126f / 255, 206f / 255, 206f / 255); break;
                case enumType.Normal:
                    img.color = new Color(137f / 255, 150f / 255, 106f / 255); break;
                case enumType.Poison:
                    img.color = new Color(131f / 255, 7f / 255, 197f / 255); break;
                case enumType.Psychic:
                    img.color = new Color(255f / 255, 0f / 255, 164f / 255); break;
                case enumType.Rock:
                    img.color = new Color(164f / 255, 143f / 255, 50f / 255); break;
                case enumType.Steel:
                    img.color = new Color(148f / 255, 191 / 255, 184f / 255); break;
                case enumType.Dragon:
                    img.color = new Color(72f / 255, 0 / 255, 146f / 255); break;
                    #endregion
            }
        }
    }
}
