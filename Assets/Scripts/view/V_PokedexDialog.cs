﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class V_PokedexDialog : MonoBehaviour {
    public delegate void ActionEvent(object obj);
    public static ActionEvent eSelectPokedex;
    public static ActionEvent eShowDialog;

    public ScrollRect ScreenInfo;

    public RectTransform ListPokemon, InfoDefault, InfoProfile, 
            InfoDamage, InfoEvolutions, InfoNaturalMove;
    public U_Led[] Leds;
    int index;

    public GameObject CurrentPokedex;
    public GameObject[] MachineItems;
    public Image Avatar;

    void Awake()
    {
        index = 0;
        eSelectPokedex += OnSelectPokedex;
        eShowDialog += OnShowDialog;
        Leds[index].TURN_ON();
        gameObject.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
            ButtonClose();
    }

    void OnDestroy()
    {
        eSelectPokedex -= OnSelectPokedex;
        eShowDialog -= OnShowDialog;
    }

    void OnSelectPokedex(object obj)
    {
        CurrentPokedex = (GameObject)obj;
        string id = CurrentPokedex.GetComponent<V_PokedexListButton>().Id;
        if(Database.Pokedex.ContainsKey(id))
        {
            int index = Database.Pokedex[id];
            Avatar.sprite = Database.PokedexData[index].SpriteAvatar;
            Avatar.color = new Color(1, 1, 1, 1);
        }
        
    }

    void OnEnable()
    {
        //TURNOFF_AllLeds();
        Application.targetFrameRate = 60;
        StartCoroutine(IE_OpenPokedex());
        Leds[index].TURN_ON();
    }

    void OnDisable()
    {
        Application.targetFrameRate = 30;
        GetComponent<RectTransform>().sizeDelta = new Vector2(320, 320);
        foreach (GameObject g in MachineItems)
            g.SetActive(false);
    }

    void OnShowDialog(object obj)
    {
        gameObject.SetActive(!gameObject.active);
    }

    void TURNOFF_AllLeds()
    {
        foreach (U_Led l in Leds)
            l.TURN_OFF();
    }

    void UpdateScreen()
    {
        switch(index)
        {
            case 0:
                {
                    ScreenInfo.content.gameObject.SetActive(false);
                    ScreenInfo.content = ListPokemon;
                    ScreenInfo.content.gameObject.SetActive(true);
                } break;
            case 1: {
                    ScreenInfo.content.gameObject.SetActive(false);
                    ScreenInfo.content = InfoDefault;
                    ScreenInfo.content.gameObject.SetActive(true);
                    V_PokedexInfoDefault df = InfoDefault.GetComponent<V_PokedexInfoDefault>();
                    df.Set(CurrentPokedex.GetComponent<V_PokedexListButton>().GetData.Default);
                } break;
            case 2: {
                    ScreenInfo.content.gameObject.SetActive(false);
                    ScreenInfo.content = InfoProfile;
                    ScreenInfo.content.gameObject.SetActive(true);
                    V_PokedexInfoProfile profile = InfoProfile.GetComponent<V_PokedexInfoProfile>();
                    profile.Load(CurrentPokedex.GetComponent<V_PokedexListButton>().GetData.Profile);
                } break;
            case 3: {
                    ScreenInfo.content.gameObject.SetActive(false);
                    ScreenInfo.content = InfoDamage;
                    ScreenInfo.content.gameObject.SetActive(true);
                    V_PokedexInfoDamage damage = InfoDamage.GetComponent<V_PokedexInfoDamage>();
                    damage.SetDamage(CurrentPokedex.GetComponent<V_PokedexListButton>().GetData.Damages);
                } break;
            case 4: {
                    ScreenInfo.content.gameObject.SetActive(false);
                    ScreenInfo.content = InfoEvolutions;
                    ScreenInfo.content.gameObject.SetActive(true);
                    V_PokedexInfoEvolutions evo = InfoEvolutions.GetComponent<V_PokedexInfoEvolutions>();
                    evo.Load(CurrentPokedex.GetComponent<V_PokedexListButton>().GetData.Evolutions);
                } break;
            case 5: {
                    ScreenInfo.content.gameObject.SetActive(false);
                    ScreenInfo.content = InfoNaturalMove;
                    V_PokedexNaturalMoves natural = InfoNaturalMove.GetComponent<V_PokedexNaturalMoves>();
                    natural.Set(CurrentPokedex.GetComponent<V_PokedexListButton>().GetData.NaturalMoves);
                    ScreenInfo.content.gameObject.SetActive(true);
                } break;
        }
    }

    public void ButtonNext()
    {
        if (CurrentPokedex != null)
        {
            Leds[index].TURN_OFF();
            index++;
            if (index > 5) index = 5;
            Leds[index].TURN_ON();
            UpdateScreen();
        }
    }

    public void ButtonBack()
    {
        if (CurrentPokedex != null)
        {
            Leds[index].TURN_OFF();
            index--;
            if (index < 0) index = 0;
            Leds[index].TURN_ON();
            UpdateScreen();
        }
    }

    public void ButtonClose()
    {
        StartCoroutine(IE_ClosePokedex());
    }

    IEnumerator IE_OpenPokedex()
    {
        float width = 1740, height = 900;
        float deltaW = (width - 320) / 50;
        float deltaH = (height - 320) / 50;
        RectTransform rect = GetComponent<RectTransform>();
        yield return new WaitForSeconds(0.2f);
        for (int i = 0; i < 50; i++)
        {
            rect.sizeDelta = new Vector2(320 + (i * deltaW), 320 + (i * deltaH));
            yield return new WaitForSeconds(0.005f);
        }
        rect.sizeDelta = new Vector2(width, height);
        foreach (GameObject g in MachineItems)
            g.SetActive(true);
        yield return null;
    }
    IEnumerator IE_ClosePokedex()
    {
        float width = 1740, height = 900;
        float deltaW = (width - 320) / 50;
        float deltaH = (height - 320) / 50;
        RectTransform rect = GetComponent<RectTransform>();
        foreach (GameObject g in MachineItems)
            g.SetActive(false);
        for (int i = 50; i >= 0; i--)
        {
            rect.sizeDelta = new Vector2(320 + (i * deltaW), 320 + (i * deltaH));
            yield return new WaitForSeconds(0.005f);
        }
        rect.sizeDelta = new Vector2(320, 320);
        yield return new WaitForSeconds(0.2f);
        gameObject.SetActive(false);
        yield return null;
    }
}
