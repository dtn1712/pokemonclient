﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

public delegate void ActionChat (string str);

public delegate void ActionUpdate ();

public class Main : MonoBehaviour
{
	public static Main main;
	public static string mainThreadName;
	public static bool started = false;
	public static bool isIpod = false;
	public static bool isCompactDevice = true;

	// hai cho quan trong can luu y khi build ban client moi -----------------------------
	public static bool isPC;
	public static bool IphoneVersionApp;// neu la ban ip cho appstore>> true: nguoc lai la bang false;
	// hai cho quan trong can luu y khi build ban client moi -----------------------------

	public static string IMEI;
	public static int versionIp = 0;	//cong len moi khi thay doi
	public static int exitValue = 1;

	void Start () {			
		if (!started) {
			Screen.orientation = ScreenOrientation.Landscape; 
			Application.runInBackground = true;
			Application.targetFrameRate = 30;
			useGUILayout = false;			
			if (main == null) {
				main = this;
			}
			started = true;
			if (Thread.CurrentThread.Name != "Main") {
				Thread.CurrentThread.Name = "Main";
			}
			mainThreadName = Thread.CurrentThread.Name;		

			IMEI = SystemInfo.deviceUniqueIdentifier;   
			setupDeviceVariable ();
			SocketManager.getInstance ().connect ();
		}		
	}



	private void setupDeviceVariable() {
		#if UNITY_IOS
			Screen.fullScreen = true;
			isPC = false
			IphoneVersionApp = true;
		#endif

		#if UNITY_STANDALONE_OSX
			Screen.fullScreen = false;
		isPC = true;
			IphoneVersionApp = false;
		#endif

		#if UNITY_STANDALONE_WIN
			Screen.fullScreen = false;
			isPC = true;
			IphoneVersionApp = false;
		#endif
	}

	void OnGUI ()
	{	   
		SocketManager.getInstance ().connect ();
	}
		
	void OnApplicationQuit ()
	{
		SocketManager.getInstance ().close ();
		if (isPC) {
			Application.Quit ();
		}
	}

	public static void exit (){
		if (isPC) {
			main.OnApplicationQuit ();
		} else{
			exitValue = 0;
		}
	}
}
