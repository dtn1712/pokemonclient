﻿using System;

public static class ServerConfig
{
	public const int CONNECTION_TIMEOUT = 20000;
	public const int RECONNECT_INTERVAL = 500;

	public const string DEFAULT_BATTLE_SERVER_HOST = "127.0.0.1";
	public const string DEFAULT_CHAT_SERVER_HOST = "127.0.0.1";
	public const string DEFAULT_ACCOUNT_SERVER_HOST = "127.0.0.1";
	public const string DEFAULT_GAME_SERVER_HOST = "127.0.0.1";

	public const int DEFAULT_BATTLE_SERVER_PORT = 8000;
	public const int DEFAULT_CHAT_SERVER_PORT = 8001;
	public const int DEFAULT_ACCOUNT_SERVER_PORT = 8002;
	public const int DEFAULT_GAME_SERVER_PORT = 8003;

}

