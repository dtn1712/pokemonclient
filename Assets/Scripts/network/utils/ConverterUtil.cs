﻿using System;
using System.Text;

public class ConverterUtil {
	public static string DefaultEncoding = "UTF-8";

	public ConverterUtil() {
	}


	public static String ConvertBytes2String(byte[] data) {
		if (data == null || data.Length == 0) {
			throw new ArgumentException();
		}

		return Encoding.ASCII.GetString(data);
	}


	public static Boolean ConvertBytes2Boolean(byte[] data) {
		if (data == null || data.Length == 0) {
			throw new ArgumentException();
		}

		return Convert.ToBoolean(data[0] != 0);
	}


	public static char ConvertBytes2Character(byte[] data) {
		if (data == null || data.Length == 0) {
			throw new ArgumentException();
		}

		return (char) (((data[0] & 0xFF) << 8) + ((data[1] & 0xFF) << 0));
	}


	public static short ConvertBytes2Short(byte[] data) {
		if (data == null || data.Length == 0) {
			throw new ArgumentException();
		}

		return (short) (((data[0] & 0xFF) << 8) + ((data[1] & 0xFF) << 0));
	}


	public static int ConvertBytes2Integer(byte[] data) {
		if (data == null || data.Length == 0) {
			throw new ArgumentException();
		}

		return (((data[0] & 0xFF) << 24) + ((data[1] & 0xFF) << 16) + ((data[2] & 0xFF) << 8) + ((data[3] & 0xFF) << 0));
	}


	public static long ConvertBytes2Long(byte[] data) {
		if (data == null || data.Length == 0) {
			throw new ArgumentException();
		}

		return (data[0] << 56) + ((data[1] & 0xFF) << 48) + ((data[2] & 0xFF) << 40) + ((data[3] & 0xFF) << 32) + ((data[4] & 0xFF) << 24)
			+ ((data[5] & 0xFF) << 16) + ((data[6] & 0xFF) << 8) + ((data[7] & 0xFF) << 0);
	}


	public static float ConvertBytes2Float(byte[] data) {
		if (data == null || data.Length == 0) {
			throw new ArgumentException();
		}

		return ((data[0] & 0xFF) << 24) + ((data[1] & 0xFF) << 16) + ((data[2] & 0xFF) << 8) + ((data[3] & 0xFF) << 0);
	}


	public static Double ConvertBytes2Double(byte[] data) {
		if (data == null || data.Length == 0) {
			throw new ArgumentException();
		}

		return (data[0] << 56) + ((data[1] & 0xFF) << 48) + ((data[2] & 0xFF) << 40) + ((data[3] & 0xFF) << 32) + ((data[4] & 0xFF) << 24)
			+ ((data[5] & 0xFF) << 16) + ((data[6] & 0xFF) << 8) + ((data[7] & 0xFF) << 0);
	}

	public static byte[] ConvertString2Bytes(String value) {
		if (value == null) {
			return null;
		}

		return Encoding.ASCII.GetBytes(value);
	}

	public static byte[] ConvertShort2Bytes(short value) {
		return new byte[] { (byte) ((int)((uint)value >> 8) & 0xFF), (byte) ((int)((uint)value >> 0) & 0xFF) };
	}

	public static byte[] ConvertInteger2Bytes(int value) {
		return new byte[] { (byte) ((int)((uint)value >> 24) & 0xFF), (byte) ((int)((uint)value >> 16) & 0xFF), (byte) ((int)((uint)value >> 8) & 0xFF), (byte) ((int)((uint)value >> 0) & 0xFF) };
	}

	public static T ConvertBytes2Object<T>(params byte[] data) {
		Type clazz = typeof(T);
		if (data == null || data.Length == 0) {
			throw new ArgumentException();
		}

		try {
			if (clazz == typeof(byte[])) {
				return (T) Convert.ChangeType(data, typeof(T));
			}
			if (clazz == typeof(string)) {
				return (T) Convert.ChangeType(ConvertBytes2String(data), typeof(T));
			}
			if (clazz == typeof(bool)) {
				return (T) Convert.ChangeType(ConvertBytes2Boolean(data), typeof(T));
			}
			if (clazz == typeof(byte)) {
				return (T) Convert.ChangeType(data[0], typeof(T));
			}
			if (clazz == typeof(short)) {
				return (T) Convert.ChangeType(ConvertBytes2Short(data), typeof(T));
			}
			if (clazz == typeof(char)) {
				return (T) Convert.ChangeType(ConvertBytes2Character(data), typeof(T));
			}
			if (clazz == typeof(int)) {
				return (T) Convert.ChangeType(ConvertBytes2Integer(data), typeof(T));
			}
			if (clazz == typeof(long)) {
				return (T) Convert.ChangeType(ConvertBytes2Long(data), typeof(T));
			}
			if (clazz == typeof(float)) {
				return (T) Convert.ChangeType(ConvertBytes2Float(data), typeof(T));
			}
			if (clazz == typeof(double)) {
				return (T) Convert.ChangeType(ConvertBytes2Double(data), typeof(T));
			}
		} catch (Exception e) {
			throw new ArgumentException("Failed to convert the data", e);
		}

		throw new ArgumentException("Unsupport data type:" + clazz.FullName);
	}
}

