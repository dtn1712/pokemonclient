﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;

public class MessageSender {

	private BinaryWriter writer;
	private NetworkStream dataStream;
	private List<Message> sendingMessage;

	public MessageSender(NetworkStream dataStream) {
		this.sendingMessage = new List<Message>();
		this.dataStream = dataStream;
		this.writer = new BinaryWriter(dataStream, new System.Text.UTF8Encoding());
	}

	public void addMessage(Message message) {
		sendingMessage.Add(message);
	}

	public void run(){
		while (dataStream != null) {
			while (sendingMessage.Count > 0)
			{
				Message message = sendingMessage [0];
				if (message != null) {
					try {
						writer.Write (MessageEncoder.Encode (message));
					} catch (Exception e) {
						Debug.LogError ("Failed to write message: " + e.StackTrace);
					}
				}
				sendingMessage.RemoveAt(0);
			}
		}

		if (writer != null) {
			writer.Close ();
			writer = null;
		}
	}
}

