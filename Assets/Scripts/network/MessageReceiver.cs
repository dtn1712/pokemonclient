﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

public class MessageReceiver {

	private BinaryReader reader;
	private NetworkStream dataStream;
	private IMessageHandler messageHandler;

	public MessageReceiver(NetworkStream dataStream, IMessageHandler messageHandler) {
		this.messageHandler = messageHandler;
		this.dataStream = dataStream;
		this.reader = new BinaryReader(dataStream, new System.Text.UTF8Encoding());

	}

	private Message readMessage() {
		// Read Header
		byte[] dataHeader = new byte[Message.HeaderLength];
		reader.Read(dataHeader, 0, dataHeader.Length);
		Message messageWithHeader = MessageDecoder.DecodeHeader(dataHeader);

		// Read body
		byte[] data = new byte[Message.HeaderLength + messageWithHeader.GetBodyLength()];
		Array.Copy(dataHeader, 0, data, 0, Message.HeaderLength);
		reader.Read(data, Message.HeaderLength, messageWithHeader.GetBodyLength());

		return MessageDecoder.Decode(data, messageWithHeader);
	}

	public void run() {
		while (dataStream != null) {
			if (dataStream.DataAvailable) {
				Debug.Log ("In");
				Message message = readMessage ();
				Debug.Log (message);
				if (message != null) {
					messageHandler.onMessage (message);
				} 
			}
		}
			
		if (reader != null) {
			reader.Close ();
			reader = null;
		}
	}
}

