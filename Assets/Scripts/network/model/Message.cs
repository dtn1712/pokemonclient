﻿using System;
using System.Collections.Generic;

public class Message
{
	public static short DataKey = 9999;
	public static int HeaderLength = 7;

	private List<MessageParameter> content = new List<MessageParameter>();
	private Dictionary<short, List<int>> keyIndexMap = new Dictionary<short, List<int>>();

	private short commandId;
	private byte protocolVersion;
	private int bodyLength;

	public short GetCommandId()
	{
		return commandId;
	}

	public void SetCommandId(short commandId)
	{
		this.commandId = commandId;
	}

	public byte GetProtocolVersion()
	{
		return protocolVersion;
	}

	public int GetBodyLength()
	{
		return bodyLength;
	}

	public void SetBodyLength(int bodyLength)
	{
		this.bodyLength = bodyLength;
	}

	public void SetProtocolVersion(byte protocolVersion)
	{
		this.protocolVersion = protocolVersion;
	}

	public byte[] GetBytesAt(int index) {
		if (index < 0 || index >= content.Count) {
			return null;
		}

		MessageParameter keyValueInfo = content[index];
		if (keyValueInfo != null) {
			return keyValueInfo.GetValue();
		}

		return null;
	}

	public List<short> GetKeyList() {
		if (content == null || content.Count == 0) {
			return null;
		}

		int size = content.Count;
		List<short> keys = new List<short>(size);
		for (int i = 0; i < size; i++) {
			keys.Add(content[i].GetKey());
		}

		return keys;
	}


	private MessageParameter GetKeyValueInfo(short key) {
		List<int> keyIndexes = keyIndexMap[key];
		if (keyIndexes == null || keyIndexes.Count == 0) {
			return null;
		}

		return content[keyIndexes[0]];
	}

	public String GetString(short key) {
		MessageParameter keyValueInfo = GetKeyValueInfo(key);
		if (keyValueInfo != null) {
			return ConverterUtil.ConvertBytes2String(keyValueInfo.GetValue());
		}

		return null;
	}


	public void PutBytes(short key, byte[] value) {
		MessageParameter info = new MessageParameter();
		info.SetKey(key);
		info.SetValue(value);
		PutKeyValueInfo(key, info);
	}



	private void PutKeyValueInfo(short key, MessageParameter info) {
		if (info == null || info.GetValue() == null) {
			return;
		}

		content.Add(info);
		int keyIndex = content.Count - 1;

		List<int> keyIndexes = new List<int>();
		if (keyIndexMap.ContainsKey(key)){
			keyIndexes = keyIndexMap[key];
		}
		if (keyIndexes.Count == 0) {
			keyIndexMap.Add(key, keyIndexes);
		}

		keyIndexes.Add(keyIndex);
	}


	public void PutString(short key, string value) {
		var info = new MessageParameter();
		info.SetKey(key);
		info.SetValue(ConverterUtil.ConvertString2Bytes(value));
		PutKeyValueInfo(key, info);
	}
}