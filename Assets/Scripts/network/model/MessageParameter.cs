﻿using System;

public class MessageParameter {
	private short _key;
	private byte[] _value;

	public MessageParameter() {
	}

	public short GetKey() {
		return this._key;
	}

	public byte[] GetValue() {
		return this._value;
	}

	public void SetKey(short key) {
		this._key = key;
	}

	public void SetValue(byte[] value) {
		this._value = value;
	}
}

