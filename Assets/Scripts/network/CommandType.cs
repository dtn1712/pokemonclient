﻿using System;

public class CommandType
{
	public static short COMMAND_REGISTER = 100;
	public static short COMMAND_LOGIN = 101;
	public static short COMMAND_LOGOUT = 102;

	public static short COMMAND_SUBSCRIBE_CHANNEL = 100;
	public static short COMMAND_SEND_MESSAGE = 102;
}

