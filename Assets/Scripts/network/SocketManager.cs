﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class SocketManager
{
	private static SocketManager instance;

	private static INIParser ini;

	private SocketConnection chatConnection;
	private SocketConnection accountConnection;

	private Dictionary<ServerType, SocketConnection> socketMapper = new Dictionary<ServerType, SocketConnection>();

	protected SocketManager (){}

	public static SocketManager getInstance() {
		if (instance == null) {
			init ();
			instance = new SocketManager ();
		}
		return instance;
	}

	private static void init() {
		ini = new INIParser();
		TextAsset configFile = Resources.Load("Config/server") as TextAsset;
		ini.Open (configFile);
	}


	public SocketConnection getServerConnection(ServerType serverType) {
		if (socketMapper.ContainsKey(serverType)) {
			return socketMapper[serverType];
		}
		return null;
	}

	public void connect() {
		if (chatConnection == null) {
			chatConnection = setupSocket (ini, "ChatServer", ServerConfig.DEFAULT_CHAT_SERVER_HOST, ServerConfig.DEFAULT_CHAT_SERVER_PORT);
			chatConnection.connect ();
			socketMapper.Add (ServerType.CHAT, chatConnection);
		}

		if (accountConnection == null) {
			accountConnection = setupSocket (ini, "AccountServer", ServerConfig.DEFAULT_ACCOUNT_SERVER_HOST, ServerConfig.DEFAULT_ACCOUNT_SERVER_PORT);
			accountConnection.connect ();
			socketMapper.Add (ServerType.ACCOUNT, accountConnection);
		}
	}

	public void close() {
		if (chatConnection != null) {
			chatConnection.close ();
		}
		if (accountConnection != null) {
			accountConnection.close ();
		}
	}

	private SocketConnection setupSocket(INIParser ini, string server, string defaultHost, int defaultPort) {
		string host = ini.ReadValue(server,"host", defaultHost);
		int port = ini.ReadValue(server,"port", defaultPort);
		return new SocketConnection (host, port);
	}
}
