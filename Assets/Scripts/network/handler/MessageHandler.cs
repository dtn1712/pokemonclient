﻿using System;
using UnityEngine;

public class MessageHandler : IMessageHandler
{

	public void onMessage(Message message){
		Debug.Log (message.GetString (Message.DataKey));
	}

	public void onConnectionFail() {

	}

	public void onDisconnected() {

	}

	public void onConnectSuccess() {

	}
}

