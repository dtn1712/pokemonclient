﻿using System;

public class MessageDecoder
{

	public static Message DecodeHeader(byte[] dataHeader)
	{
		byte protocolVersion = dataHeader[0];
		short commandId = ConverterUtil.ConvertBytes2Short(new byte[] {dataHeader[1], dataHeader[2]});
		int bodyLength = ConverterUtil.ConvertBytes2Integer(new byte[] {dataHeader[3], dataHeader[4], dataHeader[5], dataHeader[6]});

		Message message = new Message();
		message.SetProtocolVersion(protocolVersion);
		message.SetCommandId(commandId);
		message.SetBodyLength(bodyLength);

		return message;
	}

	public static Message Decode(byte[] data, Message messageWithHeader) {
		Message message = messageWithHeader;
		byte[] bodyData = new byte[message.GetBodyLength()];
		// copy data từ vị trí nào của data copy vào body data vào index từ
		// 0->length
		Array.Copy(data, Message.HeaderLength, bodyData, 0, message.GetBodyLength());
		int i = 0;
		while (i < message.GetBodyLength()) {
			try {
				// get key
				short key = ConverterUtil.ConvertBytes2Short(new byte[] {bodyData[i], bodyData[i + 1]});

				int valueLength = ConverterUtil.ConvertBytes2Integer(new byte[] { bodyData[(i + 2)], bodyData[(i + 3)], bodyData[(i + 4)], bodyData[(i + 5)] });
				if (valueLength > message.GetBodyLength()) {
					throw new SystemException("Invalid data, invalid value length:" + valueLength + " for key " + key);
				}

				byte[] value = new byte[valueLength];
				Array.Copy(bodyData, i + 6, value, 0, valueLength);


				message.PutBytes(key, value);
				i += 6 + valueLength;
			} catch (Exception e) {
				throw new SystemException("Invalid data:" + e.Message);
			}
		}

		return message;
	}

	public static Message Decode(byte[] data) {
		if (data.Length < Message.HeaderLength)
			throw new ArgumentException("Invalid data length");

		byte[] dataHeader = new byte[Message.HeaderLength];
		Array.Copy(data, 0, dataHeader, 0, Message.HeaderLength);

		return Decode (data, DecodeHeader(dataHeader));
	}
}