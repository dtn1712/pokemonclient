﻿using System;
using System.IO;
using System.Collections.Generic;

public class MessageEncoder
{
	public static byte[] Encode(Message message) {
		MemoryStream bos = null;
		BinaryWriter bodyDOS = null;

		try {
			List<short> keyList = message.GetKeyList();
			byte[] body = null;
			if (keyList != null && keyList.Count > 0) {
				bos = new MemoryStream();
				bodyDOS = new BinaryWriter(bos);
				int size = keyList.Count;
				for (int i = 0; i < size; i++) {
					short key = keyList[i];
					byte[] value = message.GetBytesAt(i);
					bodyDOS.Write(ConverterUtil.ConvertShort2Bytes(key));
					bodyDOS.Write(ConverterUtil.ConvertInteger2Bytes(value.Length));
					bodyDOS.Write(value);
				}
				bodyDOS.Flush();
				body = bos.ToArray();
			}

			int bodyLength = body != null ? body.Length : 0;
			MemoryStream msgStream = new MemoryStream(7 + bodyLength);
			BinaryWriter msgBody = new BinaryWriter(msgStream);
			msgBody.Write(message.GetProtocolVersion());
			msgBody.Write(ConverterUtil.ConvertShort2Bytes(message.GetCommandId()));
			msgBody.Write(ConverterUtil.ConvertInteger2Bytes(bodyLength));
			if (bodyLength > 0) {
				// body data
				msgBody.Write(body);
			}

			msgBody.Flush();
			byte[] result = msgStream.ToArray();
			return result;
		} catch (IOException e) {
			throw new SystemException("Invalid messsage");
		} finally {
			if (bodyDOS != null) {
				bodyDOS.Close ();
			}
			if (bos != null) {
				bos.Close();
			}
		}
	}
}