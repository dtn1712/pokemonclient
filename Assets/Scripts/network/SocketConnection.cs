﻿using System;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;
using System.IO;
using System.Collections.Generic;

public class SocketConnection
{

	public SocketConnection(string host, int port) {
		this.host = host;
		this.port = port;
		this.messageHandler = new MessageHandler ();
	}

	private IMessageHandler messageHandler;
	private TcpClient tcpClient;
	private NetworkStream dataStream;
	private bool connected, connecting, isCancel;
	private Thread networkThread;
	private Thread messageReceiverThread;
	private Thread messageSenderThread;

	private MessageSender messageSender;
	private MessageReceiver messageReceiver;
	private string host;
	private int port;

	public void connect() {
		if (!connected && !connecting) {
			tcpClient = null;
			networkThread = new Thread (new ThreadStart (initNetwork));
			networkThread.Start ();
		}
	}

	public void close() {
		cleanNetwork();
	}

	public void update() {
		connect ();
	}

	public void sendMessage(Message msg) {
		messageSender.addMessage (msg);
	}

	private void initNetwork()
	{
		isCancel = false;

		// Check if connection is timeout
		new Thread(() =>
			{
				Thread.Sleep(ServerConfig.CONNECTION_TIMEOUT);
				if (connecting){
					tcpClient.Close();
					isCancel = true;
					connecting = false;
					connected = false;
					messageHandler.onConnectionFail();
				}
			}
		).Start();

		connecting = true;
		try {
			doConnect(host, port);
			messageHandler.onConnectSuccess();
		} catch (Exception) {
			connected = false;
			Thread.Sleep(ServerConfig.RECONNECT_INTERVAL);
			if (isCancel) {
				if (messageHandler != null) {
					close();
					messageHandler.onConnectionFail();
				}
			}
		}
	}

	private void startSendingMessageThread() {
		if (messageSenderThread == null) {
			messageSender = new MessageSender (dataStream);
			messageSenderThread = new Thread (new ThreadStart (messageSender.run));
			messageSenderThread.Start ();
		}
	}

	private void startReceivingMessageThread() {
		if (messageReceiverThread == null) {
			messageReceiver = new MessageReceiver (dataStream, messageHandler);
			messageReceiverThread = new Thread (new ThreadStart (messageReceiver.run));
			messageReceiverThread.Start ();
		}
	}

	private void doConnect(string host, int port)
	{
		tcpClient = new TcpClient();
		tcpClient.Connect(host, port);

		dataStream = tcpClient.GetStream();

		startSendingMessageThread ();
		startReceivingMessageThread ();

		connecting = false;
		connected = true;
	}

	private void cleanNetwork()
	{
		connected = false;
		connecting = false;
		if (tcpClient != null) {
			tcpClient.Close ();
			tcpClient = null;
		}
		if (dataStream != null) {
			dataStream.Close ();
			dataStream = null;
		}
		networkThread = null;
		messageSenderThread = null;
		messageReceiverThread = null;
	}
		
}

