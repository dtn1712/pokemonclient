﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VirtualServer {

    //Static
    public static Dictionary<int, RoomBattleServer> RoomsBattles = new Dictionary<int, RoomBattleServer>();
    public static int CountRoomsBattles;

    //-- Properties
    static string MapName;
    static Vector2 Position;
    static int Id_RoomBattle = -1;
    static List<Pokemon> Pokemons;
    static Dictionary<int, int> Items;
    //--
    
    public static void Init()
    {
        Pokemons = new List<Pokemon>();
        //Pikachu
            Pokemon pikachu = new Pokemon("Pikachu");
            pikachu.SetProperties(6, 4, 5, 5, 7, 5);
            pikachu.SetHP(400, 400);
            pikachu.Skills[0] = new Skill("Default", 5);
            pikachu.Skills[1] = new Skill("Electric", 8);
            pikachu.Skills[2] = new Skill("Thunder", 15);
        Pokemons.Add(pikachu);

        //bulbasaur
        Pokemon bulbasaur = new Pokemon("Bulbasaur");
        bulbasaur.SetProperties(4, 7, 6, 5, 5, 5);
        bulbasaur.SetHP(500, 500);
        bulbasaur.Skills[0] = new Skill("Default", 5);
        Pokemons.Add(bulbasaur);
    }

    public static void ReceiveMessage(MessageNetwork message)
    {
        EnumNetWork eNet = (EnumNetWork)message.Key;
        Dictionary<byte, object> dic = message.Parameters;
        Log.Write("VirtualServer ReceiveMessage eCode: " + eNet);

        switch (eNet)
        {
            #region Login
            case EnumNetWork.Login:
                Login();
                break;
            case EnumNetWork.Regis:
                Regis();
                break;
            case EnumNetWork.BackToGame:
                BackToGame();
                break;
            #endregion

            #region Xử lý map
            case EnumNetWork.MoveOnMap:
                CheckMoveLocation(dic);
                break;
            case EnumNetWork.ChangeMap:
                ChangeMap(dic);
                break;
            case EnumNetWork.Goto_LocalMap:
                Goto_LocalMap(dic);
                break;
            case EnumNetWork.Goto_Battle:
                Goto_Battle_OnMap();
                break;
            #endregion

            #region Request Battle
            case EnumNetWork.Player_Atack:
                RoomsBattles[Id_RoomBattle].Player_Atack((int) message.Parameters[0]);
                break;
            case EnumNetWork.Enemy_Atack:
                RoomsBattles[Id_RoomBattle].Enemy_Atack((int)message.Parameters[0]);
                break;
            case EnumNetWork.Load_Battle:
                RoomsBattles[Id_RoomBattle].Load_Battle();
                break;
            case EnumNetWork.Arrest_Pokemon:
                RoomsBattles[Id_RoomBattle].Arrest_Pokemon();
                break;
            case EnumNetWork.Run_GoOut:
                RoomsBattles[Id_RoomBattle].Run_GoOut();
                break;
            case EnumNetWork.Load_List_Pokemon:
                Load_PokemonList();
                break;
            case EnumNetWork.ChangedPokemon:
                RoomsBattles[Id_RoomBattle].ChangePokemon(message.Parameters);
                break;
            case EnumNetWork.Action_Complete:
                RoomsBattles[Id_RoomBattle].Action_Complete(message.Parameters);
                break;
            case EnumNetWork.BackTo_Map:
                BackTo_Map();
                break;
            #endregion

            #region Function Others
            case EnumNetWork.RecoverHP:
                RecoverHP();
                break;
            #endregion
        }
    }
    
    #region Scene Login
    static void Login()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Login);
        mess.Parameters[0] = true;
        SendClient(mess);
        //---
        MapName = "PALLET TOWN";
        Position = new Vector2(6, -9);
        //MapName = "SAFFRON CITY";
        //      Position = new Vector2(216, 132);
    }

    static void Regis()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Regis);
        mess.Parameters[0] = true;
        SendClient(mess);
    }

    #endregion

    #region Xử lý map

    static void BackToGame()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.ChangeMap);
        mess.Parameters[0] = MapName;
        mess.Parameters[1] = (int)Position.x;
        mess.Parameters[2] = (int)Position.y;
        SendClient(mess);
    }

    static void ChangeMap(Dictionary<byte, object> dic)
    {
        MapName = (string) dic[0];
        Position = new Vector2((int)dic[1], (int)dic[2]);
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.ChangeMap);
        mess.Parameters[0] = MapName;
        mess.Parameters[1] = (int)Position.x;
        mess.Parameters[2] = (int)Position.y;
        mess.Parameters[3] = dic[3];
        SendClient(mess);
    }

    static void CheckMoveLocation(Dictionary<byte, object> dic)
    {
        int x = (int) dic[0];
        int y = (int) dic[1];
        float delta = Mathf.Pow(Mathf.Abs(x - Position.x), 2) + Mathf.Pow(Mathf.Abs(y - Position.y), 2);
        if(delta == 1)
        {
            Position = new Vector2(x, y);
        }
        else
        {
            UpdateLocation();
        }
    }

    static void Goto_LocalMap(Dictionary<byte, object> dic)
    {
        int x = (int)dic[0];
        int y = (int)dic[1];
        Position = new Vector2(x, y);
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Goto_LocalMap);
        mess.Parameters[0] = x;
        mess.Parameters[1] = y;
        SendClient(mess);
    }

    static void UpdateLocation()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.UpdateLocation);
        mess.Parameters[0] = (int) Position.x;
        mess.Parameters[1] = (int) Position.y;
        SendClient(mess);
    }

    #endregion

    #region Xử lý battle

    /// <summary>
    /// Tạo ra một sân battle
    /// Gửi một yêu cầu chuyển sang scene battle về client
    /// </summary>
    static void Goto_Battle_OnMap()
    {
        Id_RoomBattle = -1;
        for(int i = 0; i < CountRoomsBattles; i++)
        {
            if(!RoomsBattles.ContainsKey(i))
            {
                Id_RoomBattle = i;
                break;
            }
        }
        if (Id_RoomBattle == -1)
        {
            Id_RoomBattle = CountRoomsBattles;
            CountRoomsBattles++;
        }
        RoomsBattles[Id_RoomBattle] = new RoomBattleServer();
        RoomsBattles[Id_RoomBattle].IdRoom = Id_RoomBattle;
        RoomsBattles[Id_RoomBattle].Set_Pokemon_User(Pokemons);
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Goto_Battle);
        mess.Parameters[0] = Id_RoomBattle;
        SendClient(mess);
    }
    
    static void Load_PokemonList()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Load_List_Pokemon);
        for (int i = 0; i < Pokemons.Count; i++)
            mess.Parameters[(byte)i] = Pokemons[i].Name;
        SendClient(mess);
    }

    static void BackTo_Map()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.ChangeMap);
        mess.Parameters[0] = MapName;
        mess.Parameters[1] = (int)Position.x;
        mess.Parameters[2] = (int)Position.y;
        SendClient(mess);
    }

    #endregion

    #region Các Function Khác
    static void RecoverHP()
    {
        foreach (Pokemon pokemon in Pokemons)
            pokemon.HP = pokemon.MaxHP;
    }
    #endregion

    static public void SendClient(MessageNetwork mess)
    {
        VirtualClient.ReceiveMessage(mess);
    }
}
