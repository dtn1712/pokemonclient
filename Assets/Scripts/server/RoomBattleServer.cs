﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoomBattleServer {

    List<Pokemon> list_user, list_enemy;
    Pokemon Pokemon_User, Pokemon_Enemy;
    float TurnPlayer, TurnEnemy;
    bool Arrest_Successful;
    string Action_Of;
    bool PlayerIsGo_Out = false;

    public int IdRoom;

    public RoomBattleServer()
    {
        list_user = new List<Pokemon>();
        list_enemy = new List<Pokemon>();
        TurnPlayer = TurnEnemy = 0;
    }

    public void Set_Pokemon_User(List<Pokemon> pokemon)
    {
        list_user = pokemon;
    }

    public void Set_Pokemon_Enemy(List<Pokemon> pokemon)
    {
        list_enemy = pokemon;
    }

    public void Arrest_Pokemon()
    {
        if ((float)Pokemon_Enemy.HP / Pokemon_Enemy.MaxHP <= 0.1f)
        {
            int rand = Random.Range(0, 10);
            if (rand < 7)
                Arrest_Successful = true;
            else
                Arrest_Successful = false;
        }
        else
            Arrest_Successful = false;
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Arrest_Pokemon);
        mess.Parameters[0] = Arrest_Successful;
        VirtualServer.SendClient(mess);
    }

    public void Player_Atack(int skill)
    {
        float damge = Damge(skill, Pokemon_User, Pokemon_Enemy);
        Pokemon_Enemy.HP -= (int)damge;
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Enemy_be_atacked);
        mess.Parameters[0] = damge / Pokemon_Enemy.MaxHP;
        mess.Parameters[1] = Pokemon_User.Skills[skill].Name;
        //Gửi sự kiện enemy bị tấn công
        VirtualServer.SendClient(mess);
    }

    public void Enemy_Atack(int skill)
    {
        if(skill < 0)
        {
            do
            {
                skill = Random.Range(0, 4);
            }
            while (Pokemon_Enemy.Skills[skill] == null) ;
        }
        float damge = Damge(skill, Pokemon_Enemy, Pokemon_User);
        Pokemon_User.HP -= (int)damge;
        Debug.Log("Damge E: " + damge);
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Player_be_actacked);
        mess.Parameters[0] = damge / Pokemon_User.MaxHP;
        mess.Parameters[1] = Pokemon_Enemy.Skills[Random.Range(0, Pokemon_Enemy.Skill_Can_Use)].Name;
        //mess.Parameters[1] = Pokemon_Enemy.Skills[skill];
        //Gửi sự kiện enemy bị tấn công
        VirtualServer.SendClient(mess);
    }

    public void Load_Battle()
    {
        Pokemon_User = list_user[0];

        //Init enemy
        Pokemon pokemon = new Pokemon("Bulbasaur");
        pokemon.SetProperties(5, 6, 5, 5, 7, 6);
        pokemon.SetHP(500, 500);
        pokemon.Skills[0] = new Skill("Default", 5);
        list_enemy.Add(pokemon);

        //Setting
        Pokemon_Enemy = list_enemy[0];

        Action_Of = "player";
        Arrest_Successful = false;
        //---- Load infomation Battle
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Load_Battle);
        mess.Parameters[0] = Pokemon_User.Name;
        mess.Parameters[1] = (float)Pokemon_User.HP / Pokemon_User.MaxHP;
        mess.Parameters[2] = list_user.Count;

        mess.Parameters[3] = Pokemon_Enemy.Name;
        mess.Parameters[4] = (float)Pokemon_Enemy.HP / Pokemon_Enemy.MaxHP;
        mess.Parameters[5] = list_enemy.Count;

        for (int i = 0; i < 4; i++)
        {
            if (Pokemon_User.Skills[i] != null)
                mess.Parameters[(byte)(i + 6)] = Pokemon_User.Skills[i].Name;
            else
                mess.Parameters[(byte)(i + 6)] = "";
        }
        VirtualServer.SendClient(mess);
    }

    public void UpdatePokemon_User()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.UpdatePokemon_User);
        mess.Parameters[0] = Pokemon_User.Name;
        mess.Parameters[1] = (float) Pokemon_User.HP / Pokemon_User.MaxHP;
        for (int i = 0; i < 4; i++)
            if (Pokemon_User.Skills[i] != null)
                mess.Parameters[(byte)(i + 2)] = Pokemon_User.Skills[i].Name;
            else
                mess.Parameters[(byte)(i + 2)] = "";
        VirtualServer.SendClient(mess);
    }

    public void UpdatePokemon_Enemy()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.UpdatePokemon_Enemy);
        mess.Parameters[0] = Pokemon_Enemy.Name;
        mess.Parameters[1] = (float)Pokemon_Enemy.HP / Pokemon_Enemy.MaxHP;
        for (int i = 0; i < 4; i++)
            if (Pokemon_Enemy.Skills[i] != null)
                mess.Parameters[(byte)(i + 2)] = Pokemon_Enemy.Skills[i].Name;
            else
                mess.Parameters[(byte)(i + 2)] = "";
        VirtualServer.SendClient(mess);
    }

    public void Action_Complete(Dictionary<byte, object> dic)
    {
        if(PlayerIsGo_Out)
        {
            Lose_Battle();
            return;
        }

        string code = dic[0].ToString();

        if (code == "attack")
        {
            if (Pokemon_User.HP <= 0)
            {
                if (CheckTeamCanFight())
                {
                    ContinuteBattle();
                }
                else
                {
                    Lose_Battle();
                }
            }
            else if (Pokemon_Enemy.HP <= 0)
                Win_Battle();
            else
            {
                Change_Turn();
            }
        }

        if (code == "arrest")
        {
            if (Arrest_Successful)
                Arrest_Success();
            else
                Change_Turn();
        }

        if(code == "go_out")
        {
            if (Action_Of == "player")
                PlayerIsGo_Out = true;
            Change_Turn();
        }
    }

    public void ChangePokemon(Dictionary<byte, object> dic)
    {
        int Index = (int)dic[0];
        bool success = true;
        if (!(bool)dic[1])
        {
            for (int i = 0; i < list_user.Count; i++)
            {
                if (Pokemon_User == list_user[i] && Index == i)
                {
                    success = false;
                    break;
                }
            }
        }
        if (Index >= list_user.Count) success = false;
        if (list_user[Index].HP <= 0) success = false;

        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.ChangedPokemon);
        mess.Parameters[0] = success;
        if (success)
        {
            Pokemon_User = list_user[Index];
            mess.Parameters[1] = list_user[Index].Name;
            mess.Parameters[2] = (float)list_user[Index].HP / list_user[Index].MaxHP;
            for (int i = 0; i < 4; i++)
                if (list_user[Index].Skills[i] != null)
                    mess.Parameters[(byte)(i + 3)] = list_user[Index].Skills[i].Name;
                else
                    mess.Parameters[(byte)(i + 3)] = "";
        }
        else
        {
            mess.Parameters[1] = "Change Pokemon in battle is not successful. Try Again other pokemon.";
        }
        VirtualServer.SendClient(mess);
    }

    public void ContinuteBattle()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.ContinuteBattle);
        VirtualServer.SendClient(mess);
    }

    public void Win_Battle()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Win_Battle);
        mess.Parameters[0] = 100; //Exp;
        mess.Parameters[1] = 134; //Gold;
        VirtualServer.SendClient(mess);
        Destroy();
    }

    public void Lose_Battle()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Lose_Battle);
        mess.Parameters[0] = 20; //Exp;
        VirtualServer.SendClient(mess);
        Destroy();
    }

    public void Arrest_Success()
    {
        list_user.Add(Pokemon_Enemy);
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Arrest_Success);
        mess.Parameters[0] = Pokemon_Enemy.Name;
        VirtualServer.SendClient(mess);
        Destroy();
    }

    public void Run_GoOut()
    {
        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Run_GoOut);
        VirtualServer.SendClient(mess);
    }

    public void Change_Turn()
    {
        while (TurnEnemy < 100 && TurnPlayer < 100)
        {
            TurnEnemy += Pokemon_Enemy.Agi;
            TurnPlayer += Pokemon_User.Agi;
        }

        if(TurnPlayer >= 100 && TurnEnemy >= 100)
        {
            if(TurnPlayer >= TurnEnemy)
            {
                Action_Of = "player";
                TurnPlayer = TurnPlayer % 100;
            }
            else
            {
                Action_Of = "enemy";
                TurnEnemy = TurnPlayer % 100;
            }
        }
        else
        {
            if(TurnPlayer >= 100)
            {
                Action_Of = "player";
                TurnPlayer = TurnPlayer % 100;
            }
            else
            {
                Action_Of = "enemy";
                TurnEnemy = TurnPlayer % 100;
            }
        }

        MessageNetwork mess = new MessageNetwork((byte)EnumNetWork.Turn_Action);
        if (Action_Of == "player")
        {
            //Gửi về cho client player rằng đến lượt hành động
        }
        else
        {
            //Gửi về cho client enemy rằng đến lượt hành động
        }
        mess.Parameters[0] = Action_Of;
        VirtualServer.SendClient(mess);
    }

    public float Damge(int skill, Pokemon pok_1, Pokemon pok_2)
    {
        Debug.LogWarning("Damge : " + skill);
        float damge = (pok_1.PAtk * (pok_1.MAtk + pok_1.Skills[skill].Power) * pok_1.Ini_Pow);
        damge -= (pok_2.PDef * pok_2.MDef * pok_2.Ini_Pow);
        damge *= (pok_1.Ini_Random + 1);
        if (damge <= 0) damge = 1;
        return damge;
    }

    public bool CheckTeamCanFight()
    {
        for(int i = 0; i < list_user.Count; i++)
            if(list_user[i].HP > 0)
            {
                return true;
            }
        return false;
    }

    public void Destroy()
    {
        VirtualServer.RoomsBattles.Remove(IdRoom);
    }
}
