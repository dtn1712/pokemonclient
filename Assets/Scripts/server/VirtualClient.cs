﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VirtualClient {

    public delegate void ActionEvent(Dictionary<byte, object> dic);
    public static event ActionEvent OnLogin;
    public static event ActionEvent OnRegis;
    public static event ActionEvent OnChangeMap;
    public static event ActionEvent OnUpdateLocation;
    public static event ActionEvent OnGoToBattle;
    public static event ActionEvent OnGotoLocalMap;
    public static void ReceiveMessage(MessageNetwork message)
    {
        EnumNetWork eNet = (EnumNetWork)message.Key;
        Dictionary<byte, object> dic = message.Parameters;
        Debug.Log("VirtualClient ReceiveMessage eCode: " + eNet.ToString());
        switch (eNet)
        {

            #region Login
            case EnumNetWork.Login:
                if (OnLogin != null) OnLogin(dic);
                break;
            case EnumNetWork.Regis:
                if (OnRegis != null) OnRegis(dic);
                break;
            case EnumNetWork.ChangeMap:
                if (OnChangeMap != null) OnChangeMap(dic);
                break;

            #endregion

            #region Scene Map
            case EnumNetWork.UpdateLocation:
                if (OnUpdateLocation != null) OnUpdateLocation(dic);
                break;
            case EnumNetWork.Goto_LocalMap:
                if (OnGotoLocalMap != null) OnGotoLocalMap(dic);
                break;
            case EnumNetWork.Goto_Battle:
                if (OnGoToBattle != null) OnGoToBattle(dic);
                break;
            #endregion

            #region Scene Battle
            case EnumNetWork.Enemy_be_atacked:
                if (cUserBattle.Enemy_Be_Atacked != null) cUserBattle.Enemy_Be_Atacked(dic);
                break;
            case EnumNetWork.Player_be_actacked:
                if (cUserBattle.Player_Be_Atacked != null) cUserBattle.Player_Be_Atacked(dic);
                break;
            case EnumNetWork.Load_Battle:
                if (cUserBattle.Load_Battle_Info != null) cUserBattle.Load_Battle_Info(dic);
                break;
            case EnumNetWork.Turn_Action:
                if (cUserBattle.Turn_Action != null) cUserBattle.Turn_Action(dic);
                break;
            case EnumNetWork.Arrest_Pokemon:
                if (cUserBattle.Arrest_Pokemon != null) cUserBattle.Arrest_Pokemon(dic);
                break;
            case EnumNetWork.Run_GoOut:
                if (cUserBattle.Run_GoOut != null) cUserBattle.Run_GoOut(dic);
                break;
            case EnumNetWork.Load_List_Pokemon:
                if (cUserBattle.Load_ListPokemon != null) cUserBattle.Load_ListPokemon(dic);
                break;
            case EnumNetWork.ChangedPokemon:
                if (cUserBattle.ChangedPokemon != null) cUserBattle.ChangedPokemon(dic);
                break;
            case EnumNetWork.ContinuteBattle:
                if (cUserBattle.ContinuteBattle != null) cUserBattle.ContinuteBattle(dic);
                break;
            case EnumNetWork.Win_Battle:
                if (cUserBattle.Win_Battle != null) cUserBattle.Win_Battle(dic);
                break;
            case EnumNetWork.Lose_Battle:
                if (cUserBattle.Lose_Battle != null) cUserBattle.Lose_Battle(dic);
                break;
            case EnumNetWork.Arrest_Success:
                if (cUserBattle.Arrest_Success != null) cUserBattle.Arrest_Success(dic);
                break;
            #endregion
        }
    }

    public static void SendMessage(MessageNetwork mess)
    {
        VirtualServer.ReceiveMessage(mess);
    }
}
