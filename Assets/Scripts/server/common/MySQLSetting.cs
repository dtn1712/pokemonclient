﻿using UnityEngine;
using System.Collections;

public class MySQLSetting
{
    static string server = "localhost";
    static string database = "pokemon";
    static string uid = "root";
    static string password = "";
    public static string ConnectionString
    {
        get
        {
            return "SERVER=" + server + ";" + "DATABASE=" +
          database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
        }
    }
}
