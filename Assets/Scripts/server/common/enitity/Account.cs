﻿using UnityEngine;
using System.Collections;
using System;

public class Account
{
    public int Id { get; set; }
    public string User { get; set; }
    public string Pass { get; set; }
    public string LoginTime { get; set; }
    public string MapName { get; set; }
    public int PosX { get; set; }
    public int PosY { get; set; }
    public int Direction { get; set; }
    public Account()
    {
        Id = -1;
    }

    public string ToString()
    {
        return string.Format("[{0} - u:{1} - p:{2} - t:{3}]", Id, User, Pass, LoginTime);
    }
}
