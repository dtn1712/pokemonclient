﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class FileBinary {
    public static string Assets
    {
        get
        {
            string path = "";
            try
            {
                path = Path.GetDirectoryName(Application.streamingAssetsPath);
            }
            catch
            {
            }
            return path;
        }
    }

    public static string Resources
    {
        get { return Path.Combine(Assets, "Resources"); }
    }

    public static void Write(string path, object obj)
    {
        Stream stream = File.Open(path, FileMode.Create);
        BinaryFormatter bformatter = new BinaryFormatter();

        Debug.Log(string.Format("<color=yellow>Writing {0} Infomation.</color>",obj.GetType().Name));
        bformatter.Serialize(stream, obj);
        stream.Close();
    }

    public static object Read(string path)
    {
        object obj = null;
        Stream stream = File.Open(path, FileMode.Open);
        BinaryFormatter bformatter = new BinaryFormatter();
        obj = bformatter.Deserialize(stream);
        if(obj != null)
        Debug.Log(string.Format("<color=yellow>Reading {0} Information.",obj.GetType().Name));
        stream.Close();
        return obj;
    }

    public static object ReadTextAssets(string file)
    {
        object obj = null;
        TextAsset textAsset = UnityEngine.Resources.Load<TextAsset>(file);
        if (textAsset != null)
        {
            Stream stream = new MemoryStream(textAsset.bytes);
            BinaryFormatter bformatter = new BinaryFormatter();
            obj = bformatter.Deserialize(stream);
            if (obj != null)
                Debug.Log(string.Format("<color=yellow>Reading {0} Information.</color>", obj.GetType().Name));
            stream.Close();
        }
        else
            Debug.Log("File not Exists.");
        return obj;
    }
}
